import pandas as pd
import xlrd

def read_excel(model_path = r'C:\Users\trinh\OneDrive\Desktop\CODING\web_cps\\web_cps\LuongTool\models.xlsx'):
    xls = xlrd.open_workbook(model_path, on_demand=True)
    workbook = [(pd.read_excel(model_path,sheet_name).fillna(-1),sheet_name) \
                for sheet_name in xls.sheet_names()]
    return workbook

