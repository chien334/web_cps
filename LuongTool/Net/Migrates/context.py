# import pandas as pd
from setting import *

def sheet2Context(sheet_data,
                  sheet_name,
                  RepositoriesDir= 'ADR.CPS.Data.Repositories',
                  modelsDir = 'ADR.CPS.Data.Models'):
#     sheet_data = workbook[0][0]
#     sheet_name = workbook[0][1]
    modelName = formatName(sheet_name)
    
    modelDeclare = f'\2tpublic DbSet<{modelsDir}.{modelName}> {modelName}s {{ get; set;}}\n'
    ModelBuilderConfig = f'\3tmodelBuilder.ApplyConfiguration(new {modelName}Configuration(dbDescription));\n'
    keyColumn = sheet_data['Field'].loc[sheet_data['PK']=='x'].values[0]
    keyColumn = formatName(keyColumn)
    modelConfiguration = \
        f'\tpublic partial class {modelName}Configuration : IEntityTypeConfiguration<{modelsDir}.{modelName}>''\n\t{\n'\
        '\2tprivate readonly IDbDescription _dbDescription;\n'\
        f'\2tpublic {modelName}Configuration(IDbDescription dbDescription)'\
        '\n\2t{\n\3t_dbDescription = dbDescription;\n\2t}\n'\
        f'\2tpublic void Configure(EntityTypeBuilder<{modelsDir}.{modelName}> builder)''\n\2t{\n'\
        f'\3tbuilder.ToTable("{modelName}");\n'\
        f'\3tbuilder.HasKey(e => e.{keyColumn});\n'\
        '\3tbuilder.Property(e => e.Deleted).IsRequired();\n'\
        '\3tbuilder.Property(e => e.LastUpdatedTime);\n'\
        '\3tbuilder.Property(e => e.LastUpdatedBy).HasMaxLength(50);\n'\
        '\3tbuilder.Property(e => e.CreatedTime);\n'\
        '\3tbuilder.Property(e => e.CreatedBy).HasMaxLength(50);\n'
    for index, data in sheet_data.iterrows():
        columnName = formatName(data['Field'])
        columnbuilder = ''
        if data['Data Type'] != 'List Object':
            columnbuilder = f'\3tbuilder.Property(e => e.{columnName})'
            if data['Not Null'] == 'x':
                columnbuilder+='.IsRequired()'
            if data['Length'] > 0:
                length = int(data['Length'])
                columnbuilder+=f'.HasMaxLength({length})'
            columnbuilder+=';\n'
        if (data['Data Type'] == 'List Object') & (data['Reference'] != '-1'):
            reference = formatName(data['Reference'])                                 
            columnbuilder = f'\3tbuilder.HasMany(e => e.{reference}s)'\
                            f'\n\4t.WithOne(e => e.{modelName})'\
                            f'\n\4t.HasForeignKey(e => e.{columnName})'\
                            f'\n\4t.OnDelete(DeleteBehavior.NoAction);\n'

        modelConfiguration += columnbuilder
    modelConfiguration += '\n\2t}\n\t}\n'
    return modelDeclare, ModelBuilderConfig, modelConfiguration

def workbook2Context(workbook,filePath,
                     RepositoriesDir= 'ADR.CPS.Data.Responsitories',
                     modelsDir = 'ADR.CPS.Data.Models',
                     fileName = 'CPSContext',):
    modelDeclares=''
    ModelBuilderConfigs=''
    modelConfigurations=''

    
    for sheet_data, sheet_name in workbook:
        if sheet_name == 'listDataType':
            continue
        modelDeclare, ModelBuilderConfig, modelConfiguration = sheet2Context(sheet_data,sheet_name)
        modelDeclares+=modelDeclare
        ModelBuilderConfigs+=ModelBuilderConfig
        modelConfigurations+=modelConfiguration

    contextStr = \
                'using ADR.CPS.Data.Models;\n'\
                'using ADR.CPS.SDK.IoC;\n'\
                'using ADR.CPS.SDK.Metadata;\n'\
                'using Microsoft.EntityFrameworkCore;\n'\
                'using System;\n'\
                'using System.Collections.Generic;\n'\
                'using System.Text;\n'\
                'using Microsoft.Extensions.DependencyInjection;\n'\
                'using Microsoft.EntityFrameworkCore.Metadata.Builders;\n'\
                f'namespace {RepositoriesDir}\n'\
                '{\n'\
                f'\tpublic partial class {fileName} : DbContext\n'\
                '\t{\n'\
                '\2tprivate readonly IServiceProvider _serviceProvider;\n'\
                f'\2tpublic {fileName}(DbContextOptions<{fileName}> options) : base(options)\n'\
                '\2t{\n'\
                '\3t_serviceProvider = Container.Services;\n'\
                '\3tif (_serviceProvider == null) _serviceProvider = new ServiceCollection().BuildServiceProvider();\n'\
                '\2t}\n'\
                f'{modelDeclares}'\
                '\2tprotected override void OnModelCreating(ModelBuilder modelBuilder)\n'\
                '\2t{\n'\
                '\3tvar dbDescription = _serviceProvider.GetService<IDbDescription>();\n'\
                f'{ModelBuilderConfigs}'\
                '\2t}\n'\
                '\t}\n'\
                f'{modelConfigurations}'\
                '}\n'
#     print(replaceIndent(contextStr))
    contextStr = replaceIndent(contextStr)
    filePath+=f'/{fileName}.g.cs'
    writeFile(filePath,contextStr)



