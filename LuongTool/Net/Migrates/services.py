from setting import *
def workbook2Services(workbook,filePath,
                     namespace = 'ADR.CPS.Data.Services.Services',
                     ClassName = 'Service',
                     ContextFile = 'CPSContext',
                     IServiceName = 'IService'):


    ModelServices = ''
    for model,sheet_name in workbook:
        if sheet_name == 'listDataType':
            continue
        modelName = formatName(sheet_name)
        pk = [formatName(data['Field']) for i,data in model.iterrows() if data['PK'] == 'x']
        pk_query = ''
        DefaultAsync = ''
        first = True
        for key in pk:
            if first:
                DefaultAsync += 'e.' + key
                pk_query += 'e.' + key + ' == entity.' + key
                first = 0
            else:
                DefaultAsync += ',e.' + key
                pk_query += ' && e.' + key + ' == entity.' + key
        ModelServices+=\
            f'\2tpublic Task<bool> ExistsAsync({modelName} entity, CancellationToken cancellationToken = default)\n'\
            '\2t{\n'\
                f'\3treturn this.dbContext.Set<{modelName}>().AnyAsync(e => {pk_query}, cancellationToken);\n'\
            '\2t}\n'\
            f'\2tpublic async Task AddOrUpdateAsync({modelName} entity, CancellationToken cancellationToken = default)\n'\
            '\2t{\n'\
                f'\3tvar existed = await dbContext.Set<{modelName}>().FirstOrDefaultAsync(e => {pk_query});\n'\
                f'\3tif (existed == null)\n'\
                '\3t{\n'\
                    f'\4tdbContext.Set<{modelName}>().Add(entity);\n'\
                '\3t}\n'\
                f'\3telse\n'\
                '\3t{\n'\
                    '\4tdbContext.Entry(existed).CurrentValues.SetValues(entity);\n'\
                '\3t}\n'\
            '\2t}\n'\
            f'\2tpublic async Task UpdateAsync({modelName} entity, CancellationToken cancellationToken = default)\n'\
            '\2t{\n'\
                f'\3tvar existed = await dbContext.Set<{modelName}>().FirstOrDefaultAsync(e => {pk_query});\n'\
                f'\3tif (existed != null)\n'\
                '\3t{\n'\
                    '\4tdbContext.Entry(existed).CurrentValues.SetValues(entity);\n'\
                '\3t}\n'\
            '\2t}\n'\
            f'\2tpublic Task AddOrUpdateAsync(IEnumerable<{modelName}> entities, CancellationToken cancellationToken = default)\n'\
            '\2t{\n'\
                f'\3tExpression<Func<{modelName}, object>> keySelector = (e => new ''{'f'{DefaultAsync}''} );\n'\
                f'\3treturn AddOrUpdateAsync(entities, keySelector, cancellationToken);\n'\
            '\2t}\n'
    Services = \
    'using ADR.CPS.Data.Models;\n'\
    'using ADR.CPS.Data.Responsitories;\n'\
    'using ADR.CPS.Data.Services;\n'\
    'using ADR.CPS.SDK.Data.EFService;\n'\
    'using ADR.CPS.SDK.EFService;\n'\
    'using Microsoft.EntityFrameworkCore;\n'\
    'using System;\n'\
    'using System.Collections.Generic;\n'\
    'using System.Linq.Expressions;\n'\
    'using System.Text;\n'\
    'using System.Threading;\n'\
    'using System.Threading.Tasks;\n'\
    f'namespace {namespace}\n'\
    '{\n'\
        f'\tpublic partial class {ClassName} : EfService<{ContextFile}>, {IServiceName}\n'\
        '\t{\n'\
            f'\2tpublic {ClassName}(IServiceProvider serviceProvider, IBulkOperation bulkOperation) : base(serviceProvider, bulkOperation)\n'\
            '\2t{\n\2t}\n'\
                f'{ModelServices}'\
    '\n\t}\n}'
    Services = replaceIndent(Services)
    writeFile(filePath=f'{filePath}/{ClassName}.g.cs',text=Services)