from setting import *

def workbook2IService(workbook,
                      filePath,
                      namespace = 'ADR.CPS.Data.Services',
                      className = 'IService'):


    modelAsyn = ''
    for sheet_data,sheet_name in workbook:
        if sheet_name == 'listDataType':
            continue
        modelName = formatName(sheet_name)
        modelAsyn += \
        f'\2tTask<bool> ExistsAsync({modelName} entity, CancellationToken cancellationToken = default);\n'\
        f'\2tTask AddOrUpdateAsync({modelName} entity, CancellationToken cancellationToken = default);\n'\
        f'\2tTask UpdateAsync({modelName} entity, CancellationToken cancellationToken = default);\n'\
        f'\2tTask AddOrUpdateAsync(IEnumerable<{modelName}> entities, CancellationToken cancellationToken = default);\n\n'
    Services=\
    'using ADR.CPS.Data.Models;\n'\
    'using ADR.CPS.SDK.Data;\n'\
    'using System;\n'\
    'using System.Collections.Generic;\n'\
    'using System.Text;\n'\
    'using System.Threading;\n'\
    'using System.Threading.Tasks;\n'\
    f'namespace {namespace}\n'\
    '{\n'\
        f'\tpublic partial interface {className} : IAsyncService, IUnitOfWork\n'\
        '\t{\n'\
            f'{modelAsyn}'\
        '\t}\n'\
    '}'
    Services = replaceIndent(Services)
    writeFile(f'{filePath}/{className}.g.cs',Services)
