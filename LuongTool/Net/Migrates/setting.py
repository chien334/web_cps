DATA_TYPE_CONVERTER = {
    'Boolean': 'Boolean',
    'Binary': 'Binary',
    'Auto': 'long',
    'Float': 'Double',
    'Int': 'Int32',
    'Char': 'String',
    'Text': 'String',
    'Email': 'String',
    'DateTime':'DateTime',
    'FK':'long',
    'List Object':'List',
    'Long': 'long'
}

def replaceIndent(text):
    REP  = {
        '\2t':'\t\t',
        '\3t':'\t\t\t',
        '\4t':'\t\t\t\t',
        '\5t':'\t\t\t\t\t',
    }
    for key, value in REP.items():
        text= text.replace(str(key),str(value))
    return text

def formatName(string):
    return string.title().replace(' ','')



def checkPath(filePath):
    # importing os module   
    import os
    path = filePath[:filePath.rfind('/')]
    folder = path.split('/')
    if len(folder)==1:
        pathExist = os.path.exists(path)
        if not pathExist:
            os.mkdir(path)
    else: 
        path = ''
        for f in folder:
            path += f'{f}/'
            pathExist = os.path.exists(path)
            if not pathExist:
                os.mkdir(path)
    if os.path.isfile(filePath):
        writeType = 'updated'
    else:
        writeType = 'created'
    return writeType
                

def writeFile(filePath,text):
    writeType = checkPath(filePath)
    file = open(filePath, 'w') 
    # Writing a string to file 
    file.write(text) 
    file.close()

    print(f'{filePath} \t\t\t\t{writeType}')
    