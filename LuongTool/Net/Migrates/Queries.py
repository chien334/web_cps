# import pandas as pd
from setting import *

def sheet2Query(sheet_data,modelName):
    haveFromTo = ['DateTime','long','int']
    model = ''
    fieldQuery = ''
    for i, field in sheet_data.iterrows():
        if i == -1:
            pass

        dataType = DATA_TYPE_CONVERTER.get(field['Data Type'])
        fieldName = formatName(field['Field'])

        if dataType == 'List':
            continue
        isnull = '' if (field['Not Null'] == 'x' or field['Data Type']) or dataType == 'String' else '?'
        if (field['Data Type'] != 'FK' and field['PK'] != 'x') and dataType in haveFromTo:
            fieldQuery = f'\2tpublic {dataType}{isnull} From{fieldName}'' { get; set; }\n'\
                         f'\2tpublic {dataType}{isnull} To{fieldName}'' { get; set; }\n'

        elif (field['PK'] != 'x' and dataType not in haveFromTo) or field['Data Type'] == 'FK' :
            fieldQuery = f'\2tpublic {dataType}{isnull} {fieldName}'' { get; set; }\n'

        elif (field['Data Type'] == 'FK' or field['PK'] == 'x'):
            fieldQuery = f'\2tpublic {dataType} {fieldName}'' { get; set; }\n'

        model += fieldQuery
        
    Query = \
        'using System;\n'\
        'using System.Collections.Generic;\n'\
        'using ADR.CPS.SDK.Models;\n\n'\
        'namespace ADR.CPS.Data.Models.Queries\n'\
        '{\n'\
            f'\tpublic partial class {modelName}Query\n'\
            '\t{\n'\
            f'{model}\n'\
            '\t}\n'\
        '}'
    return Query


def workbook2Queries(workbook, filePath):
    for sheetData,sheetName in workbook:
        if sheetName == 'listDataType':
            continue
        modelName = formatName(sheetName)
        query = replaceIndent(sheet2Query(sheetData,modelName))
        writeFile(f'{filePath}/{modelName}Query.g.cs',query)
        