# import pandas as pd
from setting import *

def sheet2QueryService(sheet_data,sheet_name):
    modelName = formatName(sheet_name)
    modelStruct = ''
    haveFromTo = ['DateTime','long','int']
    modelKey = ''
    modelStruct = ''
    keyNum = 0
    first = True
    for i, field in sheet_data.iterrows():
        fieldName = formatName(field['Field'])
        data_type = DATA_TYPE_CONVERTER[field['Data Type']]
        if data_type == 'List':
            continue
        if first:
            if field['Data Type'] == 'FK' or field['PK'] == 'x' or data_type == 'Boolean':
                fieldQuery = f'\5t(query.Entity.{fieldName} == default || e.{fieldName} == query.Entity.{fieldName})\n'
            elif data_type not in haveFromTo:
                fieldQuery = f'\5t(query.Entity.{fieldName} == default || e.{fieldName}.Contains(query.Entity.{fieldName}))\n'
            elif data_type in haveFromTo:
                fieldQuery = f'\5t(query.Entity.From{fieldName} == default || e.{fieldName} >= query.Entity.From{fieldName})\n'\
                             f'\5t(query.Entity.To{fieldName} == default || e.{fieldName} <= query.Entity.To{fieldName})\n'
            first = False
        else:
            if field['Data Type'] == 'FK' or field['PK'] == 'x' or data_type == 'Boolean':
                fieldQuery = f'\5t&&(query.Entity.{fieldName} == default || e.{fieldName} == query.Entity.{fieldName})\n'
            elif data_type not in haveFromTo:
                fieldQuery = f'\5t&&(query.Entity.{fieldName} == default || e.{fieldName}.Contains(query.Entity.{fieldName}))\n'
            elif data_type in haveFromTo:
                fieldQuery = f'\5t&&(query.Entity.From{fieldName} == default || e.{fieldName} >= query.Entity.From{fieldName})\n'\
                             f'\5t&&(query.Entity.To{fieldName} == default || e.{fieldName} <= query.Entity.To{fieldName})\n'

        if field['PK'] == 'x' and keyNum ==0:
            modelKey += f'e.{fieldName} == entity.{fieldName}'
            keyNum+=1
        elif field['PK'] == 'x' and keyNum >0:
            modelKey += f'&& e.{fieldName} == entity.{fieldName}'
            keyNum+=1
        modelStruct += fieldQuery
    Service = \
        f'\2tpublic Task<bool> ExistsAsync({modelName} entity, CancellationToken cancellationToken = default)\n'\
        '\2t{\n'\
            f'\3treturn this.dbContext.Set<{modelName}>().AsNoTracking().AnyAsync(e => {modelKey}, cancellationToken);\n'\
        '\2t}\n'\
        f'\2tprotected IQueryable<{modelName}View> Fillter(QueryModel<{modelName}Query> query, CancellationToken cancellationToken = default)\n'\
        '\2t{\n'\
            f'\3treturn dbContext.Set<{modelName}>()\n'\
               f'\4t.AsNoTracking()\n'\
               f'\4t.Where(e => \n'\
                    f'{modelStruct}\n'\
               f'\4t)\n'\
               f'\4t.Select(x => new {modelName}View(x));\n'\
        '\2t}\n'\
        f'\2tpublic async Task<IPagedList<{modelName}View>> Search(QueryModel<{modelName}Query> query, CancellationToken cancellationToken = default)\n'\
        '\2t{\n'\
            f'\3treturn await Fillter(query, cancellationToken)\n'\
                f'\4t.PageResultAsync(query.Page, query.PageSize, cancellationToken);\n'\
        '\2t}\n'\
        f'\2tpublic async Task<List<{modelName}View>> SearchNoPaging(QueryModel<{modelName}Query> query, CancellationToken cancellationToken = default)\n'\
        '\2t{\n'\
            f'\3treturn await Fillter(query, cancellationToken)\n'\
                f'\4t.ToListAsync(cancellationToken);\n'\
        '\2t}\n'
    return Service

def workbook2QueryServices(workbook, filePath,
                           className = 'QueryServices',
                           namespace = 'ADR.CPS.Data.Services.Services'):
    modelQueryServices = ''
    for shetData, sheetName in workbook:
        if sheetName == 'listDataType':
            continue
        modelQueryServices += sheet2QueryService(shetData, sheetName) 
    QueryService = \
    'using ADR.CPS.Data.Models;\n'\
    'using ADR.CPS.Data.Models.Queries;\n'\
    'using ADR.CPS.Data.Models.Views;\n'\
    'using ADR.CPS.Data.Responsitories;\n'\
    'using ADR.CPS.SDK.Data.EFService;\n'\
    'using ADR.CPS.SDK.Models;\n'\
    'using Microsoft.EntityFrameworkCore;\n'\
    'using System;\n'\
    'using System.Collections.Generic;\n'\
    'using System.Linq;\n'\
    'using System.Text;\n'\
    'using System.Threading;\n'\
    'using System.Threading.Tasks;\n'\
    f'namespace {namespace}\n'\
    '{\n'\
        '\tpublic partial class QueryService : EfQueryService<CPSContext>, IQueryService\n'\
        '\t{\n'\
            f'\2tpublic QueryService(IServiceProvider serviceProvider) : base(serviceProvider)\n'\
            '\2t{\n'\
            '\2t}\n'\
            f'{modelQueryServices}\n'\
        '\t}\n'\
    '}\n'
    writeFile(f'{filePath}/{className}.g.cs',replaceIndent(QueryService))
