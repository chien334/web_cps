﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace System.Importation.Definition
{
    public enum ValueTypes
    {
        BIT,
        INTERGER,
        DECIMAL,
        REAL,
        TEXT,
        DATETIME,
        CUSTOM
    }
}
