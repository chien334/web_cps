﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Importation.Validation;
using System.Text;

namespace System.Importation.Definition
{
    public class PropertyMap
    {
        public string Header { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }
        public ValueTypes Type { get; set; }
        public string DateTimeFormat { get; set; }
        public Func<PropertyMap, string, Tuple<object, ValidationResult>> ValueConverter { get; set; }        
    }
}
