﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Importation.Definition
{
    public class DynamicDomain
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public DynamicDomain(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
