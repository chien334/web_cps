﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace System.Importation.Definition
{
    public class ImportItem<T>:IndexModel<T>
        where T : class, new()
    {
        public List<KeyValuePair<object, object>> DynamicValues { get; set; }        
        public bool IsValid { get; set; }
        public bool Skiped { get; set; }        
    }
}
