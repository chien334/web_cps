﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Importation.Validation;
using System.Text;

namespace System.Importation.Definition
{
    public class IndexModel<T>
    {
        public IndexModel()
        {
        }

        public IndexModel(T model, uint index)
        {
            Model = model;
            Index = index;
        }

        public T Model { get; set; }
        public uint Index { get; set; }

        public List<ValidationResult> Results { get; set; } = new List<ValidationResult>();
        public bool IsValid => Results.IsSuccess();
    }
}
