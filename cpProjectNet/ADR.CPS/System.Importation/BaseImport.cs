﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FastMember;
using System;
using System.Collections.Generic;
using System.Importation.Definition;
using System.Importation.Formatter;
using System.Importation.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace System.Importation
{
    public abstract class BaseImport<TItem>
    where TItem : class, new()
    {
        private const int DEFAULT_CHUNKSIZE = 100;

        private List<PropertyMap> listPropertiesMapping;
        private Dictionary<string, PropertyMap> headerMaps;
        private PropertyMap dynamicColumn;
        private PropertyMap dynamicValue;
        public virtual string DateTimeFormat => "dd/MM/yyyy hh:mm:ss.fff";

        protected abstract PropertyMap DynamicColumn { get; }
        protected abstract PropertyMap DynamicValue { get; }

        protected abstract int HeaderNameRowIndex { get; }
        protected abstract int StartDataRowIndex { get; }
        protected abstract int DynamicHeaderRowIndex { get; }
        protected abstract string StartDynamicColumnName { get; }
        protected abstract int EndFixedHeaderRowIndex { get; }
        protected abstract Dictionary<string, PropertyMap> FixedHeaderMaps { get; }
        protected abstract string SheetName { get; }
        protected abstract Task<List<ValidationResult>> ValidateRow(ImportItem<TItem> item, CancellationToken cancellationToken = default);
        protected abstract List<PropertyMap> PropertiesMapping { get; }
        protected abstract Task<List<ValidationResult>> SaveFixedHeader(Dictionary<string, object> fixedHeaderValues, CancellationToken cancellationToken = default);
        protected abstract Task SaveChunk(List<ImportItem<TItem>> items, CancellationToken cancellationToken);
        protected readonly TypeAccessor Accessor = TypeAccessor.Create(typeof(TItem));

        protected virtual int ChunkSize => DEFAULT_CHUNKSIZE;
        protected virtual string ErrorFormat(List<ValidationResult> failures)
        {
            return string.Join(";", failures.Where(x => x != ValidationResult.Success).Select(x => x.ToString()));
        }

        protected int _lastDetailColumnIndex;

        protected void ValidateFormat()
        {
            List<PropertyMap> propertiesMapping = PropertiesMapping;
            if (ObjectRules.IsEmpty(propertiesMapping))
            {
                throw new Exception("Unsupported properties mapping empty");
            }

            if (string.IsNullOrEmpty(SheetName))
            {
                throw new Exception("Please define sheet name");
            }

            if (HeaderNameRowIndex < 1)
            {
                throw new Exception("Invalid number header name");
            }
        }

        protected Tuple<object, ValidationResult> ConvertValue(PropertyMap propertyMapping, string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return new Tuple<object, ValidationResult>(null, ValidationResult.Success);
                }
                object convertValue = null;
                bool success = false;
                ValidationResult validationResult = ValidationResult.Success;
                switch (propertyMapping.Type)
                {
                    case ValueTypes.BIT:
                        success = Int32.TryParse(value, out var tmpValue);
                        if (success)
                        {
                            success = (tmpValue >= 0 && tmpValue <= 1);
                            convertValue = (tmpValue != 0);
                        }
                        if (!success)
                            validationResult = new ValidationResult($"Value {value} is invalid bit format");
                        break;
                    case ValueTypes.INTERGER:
                        success = Int32.TryParse(value, out var tmpInt);
                        if (success)
                            convertValue = tmpInt;
                        if (!success)
                            validationResult = new ValidationResult($"Value {value} is invalid integer format");
                        break;
                    case ValueTypes.REAL:
                        success = Double.TryParse(value, out var tmpDouble);
                        if (success)
                            convertValue = tmpDouble;
                        if (!success)
                            validationResult = new ValidationResult($"Value {value} is invalid number format");
                        break;
                    case ValueTypes.DECIMAL:
                        success = Decimal.TryParse(value, out var tmpDecimal);
                        if (success)
                            convertValue = tmpDecimal;
                        if (!success)
                            validationResult = new ValidationResult($"Value {value} is invalid number format");
                        break;
                    case ValueTypes.DATETIME:
                        if (value.All(char.IsDigit))
                        {
                            convertValue = DateTime.FromOADate(Double.Parse(value));
                        }
                        else
                        {
                            success = DateTime.TryParseExact(value, propertyMapping.DateTimeFormat, null, Globalization.DateTimeStyles.None, out var dateValue);
                            if (success) convertValue = dateValue;
                            else validationResult = new ValidationResult($"Date {value} must be in format {propertyMapping.DateTimeFormat}");
                        }
                        break;
                    default:
                        convertValue = value;
                        break;
                }
                return new Tuple<object, ValidationResult>(convertValue, validationResult);
            }
            catch (Exception ex)
            {
                var memberName = string.IsNullOrEmpty(propertyMapping.Header) ? propertyMapping.Name : propertyMapping.Header;
                return new Tuple<object, ValidationResult>(null, new ValidationResult($"Invalid input format: {ex.Message}", new string[] { memberName }));
            }

        }

        protected Tuple<bool, List<ValidationResult>> MapValueToObject(int columnIndex, Cell cell, IEnumerable<SharedStringItem> sharedStringTable, ImportItem<TItem> item, Dictionary<int, Tuple<object, ValidationResult>> dynamicColumnKeys)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            var empty = false;
            if (!dynamicColumnKeys.ContainsKey(columnIndex))
            {
                PropertyMap propertyMapping = listPropertiesMapping.Where(pro => pro.Position == columnIndex).FirstOrDefault();
                if (propertyMapping != null)
                {
                    var value = GetCellValue(cell, sharedStringTable, propertyMapping.DateTimeFormat);
                    empty = string.IsNullOrEmpty(value);
                    var (convertedValue, valResult) = propertyMapping.ValueConverter(propertyMapping, value);
                    if (valResult.IsError())
                    {
                        errors.Add(valResult);
                    }
                    else
                    {
                        if (convertedValue != null) // Keep default contructor value if input is null
                            Accessor[item.Model, propertyMapping.Name] = convertedValue;
                    }
                }
            }
            else
            {
                var (key, keyValResults) = dynamicColumnKeys[columnIndex];
                var value = GetCellValue(cell, sharedStringTable, dynamicValue.DateTimeFormat);
                empty = string.IsNullOrEmpty(value);
                var (val, valValResults) = dynamicValue.ValueConverter(dynamicValue, value);
                if (keyValResults.IsError())
                {
                    errors.Add(keyValResults);
                }
                if (valValResults.IsError())
                {
                    errors.Add(valValResults);
                }
                if (errors.Count == 0)
                    item.DynamicValues.Add(new KeyValuePair<object, object>(key, val));
            }
            return new Tuple<bool, List<ValidationResult>>(empty, errors);
        }

        protected int ExcelColumnNameToNumber(string cellReference)
        {
            string columnReference = Regex.Replace(cellReference.ToUpper(), @"[\d]", string.Empty);
            int columnNumber = -1;
            int mulitplier = 1;
            foreach (char c in columnReference.ToCharArray().Reverse())
            {
                columnNumber += mulitplier * ((int)c - 64);

                mulitplier = mulitplier * 26;
            }
            return columnNumber + 1;
        }

        protected int GetColumnIndex(string cellReference)
        {
            var address = cellReference.Replace("$", "").Split(":").FirstOrDefault();
            int startIndex = address.IndexOfAny("0123456789".ToCharArray());
            string column = address.Substring(0, startIndex);
            return ColumnNameToIndex(column);
        }

        protected int ColumnNameToIndex(string columnName)
        {
            int columnNumber = -1;
            int mulitplier = 1;
            foreach (char c in columnName.ToCharArray().Reverse())
            {
                columnNumber += mulitplier * ((int)c - 64);

                mulitplier = mulitplier * 26;
            }
            return columnNumber + 1;
        }

        protected async Task<ImportItem<TItem>> ConvertRowToModel(Row row, IEnumerable<SharedStringItem> sharedStringTable, Dictionary<int, Tuple<object, ValidationResult>> dynamicColumnKeys, CancellationToken cancellationToken)
        {
            ImportItem<TItem> item = new ImportItem<TItem> { Index = row.RowIndex, Model = new TItem(), DynamicValues = new List<KeyValuePair<object, object>>() };
            List<ValidationResult> failures = new List<ValidationResult>();
            var redundancyCells = new List<Cell>();
            var allEmpty = true;
            foreach (Cell c in row.Elements<Cell>())
            {
                var columnIndex = GetColumnIndex(c.CellReference);
                var (empty, rawErrors) = MapValueToObject(columnIndex, c, sharedStringTable, item, dynamicColumnKeys);
                failures.AddRange(rawErrors);
                allEmpty &= empty;
                if (columnIndex >= _lastDetailColumnIndex)
                    redundancyCells.Add(c);
            }

            if (allEmpty)
            {
                item.Skiped = true; // Skipp empty rows
            }
            else
            {
                if (failures.IsSuccess())
                {
                    List<ValidationResult> businessErrors = await ValidateRow(item, cancellationToken);
                    businessErrors = businessErrors == null ? new List<ValidationResult>() : businessErrors;
                    failures.AddRange(businessErrors);
                }

                if (!failures.IsSuccess())
                {
                    string error = ErrorFormat(failures);
                    Cell cellError = new Cell()
                    {
                        CellReference = BuildCellReference(row.RowIndex, _lastDetailColumnIndex + 1),
                        DataType = CellValues.String,
                        CellValue = new CellValue(error)
                    };
                    foreach (var c in redundancyCells)
                    {
                        row.RemoveChild(c);
                    }
                    row.Append(cellError);
                }
                else
                {
                    item.IsValid = true;
                }
            }

            return item;
        }

        protected async Task<List<ImportItem<TItem>>> ConvertMutipleRowToModel(List<Row> rows, IEnumerable<SharedStringItem> sharedStringTable, Dictionary<int, Tuple<object, ValidationResult>> dynamicColumnKeys, CancellationToken cancellationToken)
        {
            List<Task<ImportItem<TItem>>> taskList = new List<Task<ImportItem<TItem>>>();
            foreach (Row row in rows)
            {
                Task<ImportItem<TItem>> taskModel = ConvertRowToModel(row, sharedStringTable, dynamicColumnKeys, cancellationToken);
                taskList.Add(taskModel);
            }
            var items = await Task.WhenAll(taskList);
            var filteredItems = items.Where(item => item.Model != null && !item.Skiped).ToList();
            return filteredItems;
        }

        protected void WriteRows(OpenXmlWriter writer, List<Row> rows)
        {
            foreach (Row row in rows)
            {
                writer.WriteElement(row);
            }
        }

        protected string ToColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (dividend - modulo) / 26;
            }

            return columnName;
        }


        protected string BuildCellReference(uint row, int column) => $"{ToColumnName(column)}{row}";

        private async Task<bool> ConvertToModelsAndSaveChunk(List<Row> listRow, IEnumerable<SharedStringItem> sharedStringTable, Dictionary<int, Tuple<object, ValidationResult>> dynamicColumnKeys, CancellationToken cancellationToken)
        {
            List<ImportItem<TItem>> items = await ConvertMutipleRowToModel(listRow, sharedStringTable, dynamicColumnKeys, cancellationToken);
            var validItems = items.Where(i => i.IsValid).ToList();
            await SaveChunk(validItems, cancellationToken);
            var success = items.Count == validItems.Count;            
            return success;
        }

        // protected string GetCellValue(Cell cell, IEnumerable<SharedStringItem> sharedStringItems)
        // {
        //     string cellValue = cell?.InnerText;
        //     if (cell.DataType != null && cell.DataType == CellValues.SharedString)
        //     {
        //         SharedStringItem ssi = sharedStringItems.ElementAt(int.Parse(cellValue));
        //         cellValue = ssi?.InnerText;
        //     }
        //     else if (cell.DataType != null && cell.DataType == CellValues.InlineString)
        //     {
        //         cellValue = cell.InlineString?.InnerText;
        //     }
        //     return cellValue;
        // }

        public string GetCellValue(Cell cell, IEnumerable<SharedStringItem> sharedStringItems, string datetimeFormat)
        {
            string result = string.Empty;
            //start of if  

            if (cell != null)
            {
                if (cell.DataType != null)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            {
                                int id;
                                if (Int32.TryParse(cell.InnerText, out id))
                                {
                                    SharedStringItem item = sharedStringItems.ElementAt(id);
                                    if (item.Text != null)
                                    {
                                        result = item.Text.Text;
                                    }
                                    else if (item.InnerText != null)
                                    {
                                        result = item.InnerText;
                                    }
                                    else if (item.InnerXml != null)
                                    {
                                        result = item.InnerXml;
                                    }

                                } //end of if (Int32.TryParse ( thecurrentcell.InnerText, out id )  
                                break;
                            }
                        // case CellValues.Boolean:                           
                        case CellValues.Date:
                            var textValue = (cell.CellValue == null ? cell.InnerText : cell.CellValue.InnerText);
                            if (double.TryParse(textValue, out var val))
                                result = DateTime.FromOADate(val).ToString(datetimeFormat);
                            else
                                result = textValue;
                            break;
                        // case CellValues.Error:
                        // case CellValues.InlineString:
                        // case CellValues.Number:
                        // case CellValues.String:
                        //     {
                        //         result = cell.InnerText;
                        //         break;
                        //     }
                        default:
                            result = (cell.CellValue == null ? cell.InnerText : cell.CellValue.InnerText);
                            break;

                    } // end of switch(thecurrentcell.DataType.Value)  

                } //end of (thecurrentcell.DataType != null)  
                else if (cell.DataType == null)
                {
                    if (cell.CellFormula != null)
                    {
                        result = cell.CellValue.InnerText;
                    }
                    else
                    {
                        result = cell.InnerText;
                    }
                }
            } //end of if(thecurrentcell!=null)  
            return result;
        }

        private void SetHeaderPosition(Row headerRow, IEnumerable<SharedStringItem> sharedStringTable)
        {
            foreach (Cell c in headerRow.Elements<Cell>())
            {
                string headerName = GetCellValue(c, sharedStringTable, DateTimeFormat);
                PropertyMap propertiesMapping = listPropertiesMapping.Where(pro => pro.Header == headerName).FirstOrDefault();
                int columnIndex = ExcelColumnNameToNumber(c.CellReference);
                if (!ObjectRules.IsEmpty(propertiesMapping))
                {
                    propertiesMapping.Position = columnIndex;
                }
                if (columnIndex > _lastDetailColumnIndex) _lastDetailColumnIndex = columnIndex;
            }
            List<string> headerNotMappings = listPropertiesMapping.Where(property => property.Position == 0).Select(property => property.Header).ToList();
            if (!ObjectRules.IsEmpty(headerNotMappings))
            {
                throw new Exception(string.Format("Invalid headers mapping {0}", string.Join(',', headerNotMappings)));
            }

        }

        private Tuple<bool, Dictionary<string, object>> ReadFixedCells(
            Row row,
            Dictionary<string, PropertyMap> valueMappings,
            IEnumerable<SharedStringItem> sharedStringTable)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            List<ValidationResult> validationResults = new List<ValidationResult>();
            foreach (Cell c in row.Elements<Cell>())
            {
                var key = c.CellReference?.ToString()?.ToLower();
                if (valueMappings.ContainsKey(key))
                {
                    var propertyDesc = valueMappings[key];
                    var cellValue = GetCellValue(c, sharedStringTable, propertyDesc.DateTimeFormat);
                    var (value, valResult) = propertyDesc.ValueConverter(propertyDesc, cellValue);
                    if (valResult.IsSuccess())
                    {
                        result[propertyDesc.Name] = value;
                    }
                    else
                    {
                        validationResults.Add(valResult);
                    }
                }
            }

            if (validationResults.IsError())
            {
                string error = ErrorFormat(validationResults);
                Cell cellError = new Cell()
                {
                    DataType = CellValues.String,
                    CellValue = new CellValue(error)
                };
                row.Append(cellError);
                return new Tuple<bool, Dictionary<string, object>>(false, new Dictionary<string, object>());
            }

            return new Tuple<bool, Dictionary<string, object>>(true, result);
        }

        protected virtual void InitializeMappings()
        {
            listPropertiesMapping = PropertiesMapping;
            listPropertiesMapping.ForEach(m =>
            {
                if (m.ValueConverter == null) m.ValueConverter = ConvertValue;
            });
            headerMaps = new Dictionary<string, PropertyMap>();
            if (FixedHeaderMaps != null)
                foreach (var kv in FixedHeaderMaps)
                {
                    if (kv.Value.ValueConverter == null) kv.Value.ValueConverter = ConvertValue;
                    headerMaps[kv.Key.ToLower()] = kv.Value;
                }
            dynamicColumn = DynamicColumn;
            dynamicValue = DynamicValue;
            if (dynamicColumn != null && dynamicColumn.ValueConverter == null)
                dynamicColumn.ValueConverter = ConvertValue;
            if (dynamicValue != null && dynamicValue.ValueConverter == null)
                dynamicValue.ValueConverter = ConvertValue;
        }

        public virtual async Task<bool> Import(Stream stream, CancellationToken cancellationToken = default)
        {
            int chunk = ChunkSize;
            chunk = chunk > 0 ? chunk : DEFAULT_CHUNKSIZE;
            ValidateFormat();
            InitializeMappings();
            var fixedHeaderValues = new Dictionary<string, object>();

            int fixedColumns = listPropertiesMapping.Count();
            var dynamicColumnName = StartDynamicColumnName;
            int dynamicColumnIndex = string.IsNullOrEmpty(dynamicColumnName) ? 0 : ExcelColumnNameToNumber(dynamicColumnName);
            int row = 0;
            Dictionary<int, Tuple<object, ValidationResult>> dynamicColumnKeys = new Dictionary<int, Tuple<object, ValidationResult>>();
            var success = true;

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, true))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                string originalSheetId = workbookPart.Workbook.Descendants<Sheet>().First(s => SheetName.Equals(s.Name)).Id;
                WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(originalSheetId);

                WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
                string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

                OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
                OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

                List<Row> listRow = new List<Row>();

                IEnumerable<SharedStringItem> sharedStringTable = workbookPart.SharedStringTablePart?.SharedStringTable?.Elements<SharedStringItem>();
                while (reader.Read())
                {
                    if (reader.IsStartElement && reader.ElementType != typeof(SheetData) && reader.ElementType != typeof(Row))
                    {
                        writer.WriteStartElement(reader);
                    }
                    else if (reader.IsEndElement && reader.ElementType != typeof(SheetData) && reader.ElementType != typeof(Row))
                    {
                        writer.WriteEndElement();
                    }
                    if (reader.ElementType == typeof(SheetData))
                    {
                        if (reader.IsStartElement)
                        {
                            writer.WriteStartElement(new SheetData());
                        }
                        if (reader.IsEndElement)
                        {
                            if (listRow.Count() > 0)
                            {
                                success &= await ConvertToModelsAndSaveChunk(listRow, sharedStringTable, dynamicColumnKeys, cancellationToken);
                                WriteRows(writer, listRow);
                                listRow.Clear();
                            }
                            writer.WriteEndElement();
                        }

                    }
                    if (reader.ElementType == typeof(Row))
                    {
                        Row currentRow = (Row)reader.LoadCurrentElement();
                        row = Int32.Parse(currentRow.RowIndex.ToString());
                        if (row >= StartDataRowIndex)
                        {
                            listRow.Add(currentRow);
                        }
                        else
                        {
                            if (row == DynamicHeaderRowIndex)
                            {
                                foreach (Cell c in currentRow.Elements<Cell>())
                                {
                                    int columnIndex = ExcelColumnNameToNumber(c.CellReference);

                                    if (columnIndex >= dynamicColumnIndex)
                                    {
                                        string cellValue = GetCellValue(c, sharedStringTable, dynamicColumn.DateTimeFormat);
                                        var (val, keyValResults) = dynamicColumn.ValueConverter(dynamicColumn, cellValue);
                                        dynamicColumnKeys.Add(columnIndex, new Tuple<object, ValidationResult>(val, keyValResults));
                                    }
                                    if (columnIndex > _lastDetailColumnIndex) _lastDetailColumnIndex = columnIndex;
                                }
                            }
                            if (row == StartDataRowIndex - 1)
                            {
                                Cell cellHeaderError = new Cell()
                                {
                                    CellReference = BuildCellReference(currentRow.RowIndex, _lastDetailColumnIndex + 1),
                                    DataType = CellValues.String,
                                    CellValue = new CellValue("Error")
                                };
                                currentRow.Append(cellHeaderError);
                            }
                            if (row == HeaderNameRowIndex)
                            {
                                SetHeaderPosition(currentRow, sharedStringTable);
                            }
                            if (row <= EndFixedHeaderRowIndex && headerMaps.Count > 0)
                            {
                                var (ok, fixedCells) = ReadFixedCells(currentRow, headerMaps, sharedStringTable);
                                success &= ok;
                                foreach (var kv in fixedCells)
                                {
                                    fixedHeaderValues[kv.Key] = kv.Value;
                                }
                            }
                            if (row == EndFixedHeaderRowIndex)
                            {
                                var valResults = await SaveFixedHeader(fixedHeaderValues, cancellationToken);
                                success &= valResults.IsSuccess();
                                if (valResults.IsError())
                                {
                                    string error = ErrorFormat(valResults);
                                    var maxIndex = currentRow.Elements<Cell>().Select(x => ExcelColumnNameToNumber(x.CellReference)).Max();
                                    Cell cellError = new Cell()
                                    {
                                        CellReference = BuildCellReference(currentRow.RowIndex, maxIndex + 1),
                                        DataType = CellValues.String,
                                        CellValue = new CellValue(error)
                                    };
                                    currentRow.Append(cellError);
                                }
                            }
                            writer.WriteElement(currentRow);
                        }

                        if (listRow.Count() == chunk)
                        {
                            success &= await ConvertToModelsAndSaveChunk(listRow, sharedStringTable, dynamicColumnKeys, cancellationToken);
                            WriteRows(writer, listRow);
                            listRow.Clear();
                        }
                    }
                }
                reader.Close();
                writer.Close();

                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(originalSheetId)).First();
                sheet.Id.Value = replacementPartId;
                workbookPart.DeletePart(worksheetPart);
            }
            return success;
        }
    }
}
