﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Importation.Validation
{
    public static class NumberRules
    {
        public static bool IsNumber(dynamic value)
        {
            return value is sbyte
                || value is byte
                || value is int
                || value is long
                || value is double
                || value is decimal;
        }
    }
}
