﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace System.Importation.Validation
{
    public static class ObjectRules
    {
        public static bool IsError(this ValidationResult validationResult)
        => validationResult != null && validationResult != ValidationResult.Success;
        public static bool IsSuccess(this ValidationResult validationResult)
        => validationResult == null || validationResult == ValidationResult.Success;
        public static bool IsSuccess(this List<ValidationResult> results)
        => results == null || !results.Any(x => x != null && x != ValidationResult.Success);
        public static bool IsError(this List<ValidationResult> results)
        => results != null && results.Any(x => x != null && x != ValidationResult.Success);
        public static bool IsEmpty(object obj)
        {
            if (obj == null)
            {
                return true;
            }

            if (obj is string && string.IsNullOrEmpty((string)obj))
            {
                return true;
            }

            if (obj is ICollection && (obj as ICollection).Count == 0)
            {
                return true;
            }

            return false;
        }
    }
}
