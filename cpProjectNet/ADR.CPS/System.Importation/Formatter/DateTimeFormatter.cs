﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Importation.Formatter
{
    public static class DateTimeFormatter
    {
        private static string DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";

        public static string ToString(DateTime dateTime)
        {
            if (dateTime == null)
            {
                return "";
            }
            return dateTime.ToString(DATE_TIME_FORMAT);
        }

        public static DateTime toDateTime(string dateTime, string format = null)
        {
            format = string.IsNullOrEmpty(format) ? DATE_TIME_FORMAT : format;
            DateTime datetime = DateTime.ParseExact(dateTime, format,
                                       System.Globalization.CultureInfo.InvariantCulture);
            return datetime;
        }
    }
}
