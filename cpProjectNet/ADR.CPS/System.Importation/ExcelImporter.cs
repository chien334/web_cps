﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using FastMember;
using System;
using System.Collections.Generic;
using System.Importation.Definition;
using System.Importation.Formatter;
using System.Importation.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace System.Importation
{
    public abstract class ExcelImporter<TItem>
    where TItem : class, ICloneable, new()
    {
        private const int DEFAULT_CHUNKSIZE = 100;

        private List<PropertyMap> listPropertiesMapping;
        private Dictionary<string, PropertyMap> headerMaps;
        private PropertyMap dynamicColumn;
        private PropertyMap dynamicValue;
        private Func<ImportItem<TItem>, IEnumerable<IndexModel<TItem>>> _mapFunc;

        protected abstract PropertyMap DynamicColumn { get; }
        protected abstract PropertyMap DynamicValue { get; }

        protected abstract int HeaderNameRowIndex { get; }
        protected abstract int StartDataRowIndex { get; }
        protected abstract int DynamicHeaderRowIndex { get; }
        protected abstract string StartDynamicColumnName { get; }
        protected abstract int EndFixedHeaderRowIndex { get; }
        protected abstract Dictionary<string, PropertyMap> FixedHeaderMaps { get; }
        protected abstract Task BindDynamic(TItem item, KeyValuePair<object, object> value, CancellationToken cancellationToken = default);
        protected abstract string SheetName { get; }
        protected virtual Task<List<ValidationResult>> ValidateRow(ImportItem<TItem> item, CancellationToken cancellationToken = default)
        => Task.FromResult(new List<ValidationResult>());
        protected abstract Task ValidateAsync(IndexModel<TItem>[] items, CancellationToken cancellationToken = default);
        protected abstract List<PropertyMap> PropertiesMapping { get; }
        protected abstract Task<List<ValidationResult>> SaveFixedHeader(Dictionary<string, object> fixedHeaderValues, CancellationToken cancellationToken = default);
        protected abstract Task SaveChunk(List<IndexModel<TItem>> items, CancellationToken cancellationToken);
        protected readonly TypeAccessor Accessor = TypeAccessor.Create(typeof(TItem));

        protected virtual int ChunkSize => DEFAULT_CHUNKSIZE;
        protected virtual string ErrorFormat(List<ValidationResult> failures)
        {
            return string.Join(";", failures.Where(x => x != ValidationResult.Success).Select(x => x.ToString()));
        }

        protected int _lastDetailColumnIndex;

        protected void ValidateFormat()
        {
            List<PropertyMap> propertiesMapping = PropertiesMapping;
            if (ObjectRules.IsEmpty(propertiesMapping))
            {
                throw new Exception("Unsupported properties mapping empty");
            }

            if (string.IsNullOrEmpty(SheetName))
            {
                throw new Exception("Please define sheet name");
            }

            if (HeaderNameRowIndex < 1)
            {
                throw new Exception("Invalid number header name");
            }
        }

        protected Tuple<object, ValidationResult> ConvertValue(PropertyMap propertyMapping, string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return new Tuple<object, ValidationResult>(null, ValidationResult.Success);
                }
                object convertValue = null;
                bool success = false;
                switch (propertyMapping.Type)
                {
                    case ValueTypes.BIT:
                        int tmpValue;
                        success = Int32.TryParse(value, out tmpValue);
                        if (success)
                            convertValue = (tmpValue != 0);
                        break;
                    case ValueTypes.INTERGER:
                        int tmpInt;
                        success = Int32.TryParse(value, out tmpInt);
                        if (success)
                            convertValue = tmpInt;
                        break;
                    case ValueTypes.REAL:
                        double tmpDouble;
                        success = Double.TryParse(value, out tmpDouble);
                        if (success)
                            convertValue = tmpDouble;
                        break;
                    case ValueTypes.DECIMAL:
                        Decimal tmpDecimal;
                        success = Decimal.TryParse(value, out tmpDecimal);
                        if (success)
                            convertValue = tmpDecimal;
                        break;
                    case ValueTypes.DATETIME:
                        if (value.All(char.IsDigit))
                        {
                            convertValue = DateTime.FromOADate(Double.Parse(value));
                        }
                        else
                        {
                            convertValue = DateTimeFormatter.toDateTime(value.ToString(), propertyMapping.DateTimeFormat);
                        }
                        break;
                    default:
                        convertValue = value;
                        break;
                }
                return new Tuple<object, ValidationResult>(convertValue, ValidationResult.Success);
            }
            catch (Exception ex)
            {
                var memberName = string.IsNullOrEmpty(propertyMapping.Header) ? propertyMapping.Name : propertyMapping.Header;
                return new Tuple<object, ValidationResult>(null, new ValidationResult($"Invalid input format: {ex.Message}", new string[] { memberName }));
            }

        }

        protected List<ValidationResult> MapValueToObject(int columnIndex, string value, ImportItem<TItem> item, Dictionary<int, string> dynamicColumnKeys)
        {
            string dynamicKey = null;
            List<ValidationResult> errors = new List<ValidationResult>();
            dynamicColumnKeys.TryGetValue(columnIndex, out dynamicKey);
            if (ObjectRules.IsEmpty(dynamicKey))
            {
                PropertyMap propertyMapping = listPropertiesMapping.Where(pro => pro.Position == columnIndex).FirstOrDefault();
                if (propertyMapping != null)
                {
                    var (convertedValue, valResult) = propertyMapping.ValueConverter(propertyMapping, value);
                    if (valResult.IsError())
                    {
                        errors.Add(valResult);
                    }
                    else
                    {
                        if (convertedValue != null) // Keep default contructor value if input is null
                            Accessor[item.Model, propertyMapping.Name] = convertedValue;
                    }
                }
            }
            else
            {
                var (key, keyValResults) = dynamicColumn.ValueConverter(dynamicColumn, dynamicKey);
                var (val, valValResults) = dynamicValue.ValueConverter(dynamicValue, value);
                if (keyValResults.IsError())
                {
                    errors.Add(keyValResults);
                }
                if (valValResults.IsError())
                {
                    errors.Add(valValResults);
                }
                if (errors.Count == 0)
                    item.DynamicValues.Add(new KeyValuePair<object, object>(key, val));
            }
            return errors;
        }

        protected int ExcelColumnNameToNumber(string cellReference)
        {
            string columnReference = Regex.Replace(cellReference.ToUpper(), @"[\d]", string.Empty);
            int columnNumber = -1;
            int mulitplier = 1;
            foreach (char c in columnReference.ToCharArray().Reverse())
            {
                columnNumber += mulitplier * ((int)c - 64);

                mulitplier = mulitplier * 26;
            }
            return columnNumber + 1;
        }

        protected int GetColumnIndex(string cellReference)
        {
            var address = cellReference.Replace("$", "").Split(":").FirstOrDefault();
            int startIndex = address.IndexOfAny("0123456789".ToCharArray());
            string column = address.Substring(0, startIndex);
            return ColumnNameToIndex(column);
        }

        protected int ColumnNameToIndex(string columnName)
        {
            int columnNumber = -1;
            int mulitplier = 1;
            foreach (char c in columnName.ToCharArray().Reverse())
            {
                columnNumber += mulitplier * ((int)c - 64);

                mulitplier = mulitplier * 26;
            }
            return columnNumber + 1;
        }

        protected Task<ImportItem<TItem>> ConvertRowToModel(Row row, IEnumerable<SharedStringItem> sharedStringTable, Dictionary<int, string> dynamicColumnKeys, CancellationToken cancellationToken)
        {
            ImportItem<TItem> item = new ImportItem<TItem> { Index = row.RowIndex, Model = new TItem(), DynamicValues = new List<KeyValuePair<object, object>>() };
            List<ValidationResult> failures = new List<ValidationResult>();
            Dictionary<int, string> cellValues = new Dictionary<int, string>();            
            foreach (Cell c in row.Elements<Cell>())
            {
                var columnIndex = GetColumnIndex(c.CellReference);
                string cellValue = GetCellValue(c, sharedStringTable);
                cellValues[columnIndex] = cellValue;
            }

            if (!cellValues.Values.Any(x => !string.IsNullOrEmpty(x)))
            {
                item.Skiped = true; // Skipp empty rows
            }
            else
            {
                foreach (var (columnIndex, cellValue) in cellValues)
                {
                    var rawErrors = MapValueToObject(columnIndex, cellValue, item, dynamicColumnKeys);
                    failures.AddRange(rawErrors);
                }

                item.IsValid = failures.IsSuccess();
                item.Results = failures;
            }

            return Task.FromResult(item);
        }

        protected virtual IEnumerable<IndexModel<TItem>> MapToItems(ImportItem<TItem> item)
        {
            return item.DynamicValues.Select(x =>
            {
                var newItem = item.Model.Clone() as TItem;
                BindDynamic(newItem, x);
                return new IndexModel<TItem> { Model = newItem, Index = item.Index, Results = new List<ValidationResult>(item.Results.ToArray()) };
            });
        }

        protected virtual IEnumerable<IndexModel<TItem>> MapToItem(ImportItem<TItem> item)
        {
            return new IndexModel<TItem>[] { new IndexModel<TItem> { Model = item.Model, Index = item.Index, Results = new List<ValidationResult>(item.Results.ToArray()) } };
        }
        
        protected void WriteRows(OpenXmlWriter writer, List<Row> rows)
        {
            foreach (Row row in rows)
            {
                writer.WriteElement(row);
            }
        }

        protected string ToColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (dividend - modulo) / 26;
            }

            return columnName;
        }


        protected string BuildCellReference(uint row, int column) => $"{ToColumnName(column)}{row}";

        private async Task<bool> ConvertToModelsAndSaveChunk(List<Row> listRow, IEnumerable<SharedStringItem> sharedStringTable, Dictionary<int, string> dynamicColumnKeys, OpenXmlWriter writer, CancellationToken cancellationToken)
        {
            var rawItems = await Task.WhenAll(listRow.Select(async (row) => await ConvertRowToModel(row, sharedStringTable, dynamicColumnKeys, cancellationToken)));
            var convertItems = rawItems.SelectMany(x => _mapFunc(x)).ToArray();
            await ValidateAsync(convertItems, cancellationToken);

            var (valid, invalid) = convertItems.Aggregate(
                        new Tuple<List<IndexModel<TItem>>, List<IndexModel<TItem>>>(
                                new List<IndexModel<TItem>>(), new List<IndexModel<TItem>>()),
                        (map, item) =>
                        {
                            if (item.IsValid) map.Item1.Add(item);
                            else map.Item2.Add(item);
                            return map;
                        });
            var errors = invalid.GroupBy(e => e.Index).Select(g => new Tuple<uint, List<ValidationResult>>(g.Key, g.SelectMany(x => x.Results).ToList()));
            foreach (var (index, failures) in errors)
            {
                var row = listRow.First(r => r.RowIndex == index);
                string error = ErrorFormat(failures);
                Cell cellError = new Cell()
                {
                    CellReference = BuildCellReference(row.RowIndex, _lastDetailColumnIndex + 1),
                    DataType = CellValues.String,
                    CellValue = new CellValue(error)
                };
                var redundancyCells = row.Elements<Cell>()
                        .Where(c => GetColumnIndex(c.CellReference) > _lastDetailColumnIndex);

                foreach (var c in redundancyCells)
                {
                    row.RemoveChild(c);
                }
                row.Append(cellError);
            }

            await SaveChunk(valid, cancellationToken);
            WriteRows(writer, listRow);
            listRow.Clear();
            return (invalid.Count == 0);
        }

        public string GetCellValue(Cell cell, IEnumerable<SharedStringItem> sharedStringItems)
        {
            string result = string.Empty;
            //start of if  

            if (cell != null)
            {
                if (cell.DataType != null)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            {
                                int id;
                                if (Int32.TryParse(cell.InnerText, out id))
                                {
                                    SharedStringItem item = sharedStringItems.ElementAt(id);
                                    if (item.Text != null)
                                    {
                                        result = item.Text.Text;
                                    }
                                    else if (item.InnerText != null)
                                    {
                                        result = item.InnerText;
                                    }
                                    else if (item.InnerXml != null)
                                    {
                                        result = item.InnerXml;
                                    }

                                } //end of if (Int32.TryParse ( thecurrentcell.InnerText, out id )  
                                break;
                            }
                        // case CellValues.Boolean:                           
                        // case CellValues.Date:
                        // case CellValues.Error:
                        // case CellValues.InlineString:
                        // case CellValues.Number:
                        // case CellValues.String:
                        //     {
                        //         result = cell.InnerText;
                        //         break;
                        //     }
                        default:
                            if (cell.CellValue != null)
                                result = cell.CellValue.InnerText;
                            else
                                result = cell.InnerText;
                            break;

                    } // end of switch(thecurrentcell.DataType.Value)  

                } //end of (thecurrentcell.DataType != null)  
                else if (cell.DataType == null)
                {
                    if (cell.CellFormula != null)
                    {
                        result = cell.CellValue.InnerText;
                    }
                    else
                    {
                        result = cell.InnerText;
                    }
                }
            } //end of if(thecurrentcell!=null)  
            return result;
        }

        private void SetHeaderPosition(Row headerRow, IEnumerable<SharedStringItem> sharedStringTable)
        {
            foreach (Cell c in headerRow.Elements<Cell>())
            {

                string headerName = GetCellValue(c, sharedStringTable);
                PropertyMap propertiesMapping = listPropertiesMapping.Where(pro => pro.Header == headerName).FirstOrDefault();
                int columnIndex = ExcelColumnNameToNumber(c.CellReference);
                if (!ObjectRules.IsEmpty(propertiesMapping))
                {
                    propertiesMapping.Position = columnIndex;
                }
                if (columnIndex > _lastDetailColumnIndex) _lastDetailColumnIndex = columnIndex;
            }
            List<string> headerNotMappings = listPropertiesMapping.Where(property => property.Position == 0).Select(property => property.Header).ToList();
            if (!ObjectRules.IsEmpty(headerNotMappings))
            {
                throw new Exception(string.Format("Invalid headers mapping {0}", string.Join(',', headerNotMappings)));
            }

        }

        private Tuple<bool, Dictionary<string, object>> ReadFixedCells(
            Row row,
            Dictionary<string, PropertyMap> valueMappings,
            IEnumerable<SharedStringItem> sharedStringTable)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            List<ValidationResult> validationResults = new List<ValidationResult>();
            foreach (Cell c in row.Elements<Cell>())
            {
                var key = c.CellReference?.ToString()?.ToLower();
                if (valueMappings.ContainsKey(key))
                {
                    var cellValue = GetCellValue(c, sharedStringTable);
                    var propertyDesc = valueMappings[key];
                    var (value, valResult) = propertyDesc.ValueConverter(propertyDesc, cellValue);
                    if (valResult.IsSuccess())
                    {
                        result[propertyDesc.Name] = value;
                    }
                    else
                    {
                        validationResults.Add(valResult);
                    }
                }
            }

            if (validationResults.IsError())
            {
                string error = ErrorFormat(validationResults);
                Cell cellError = new Cell()
                {
                    DataType = CellValues.String,
                    CellValue = new CellValue(error)
                };
                row.Append(cellError);
                return new Tuple<bool, Dictionary<string, object>>(false, new Dictionary<string, object>());
            }

            return new Tuple<bool, Dictionary<string, object>>(true, result);
        }

        protected virtual void InitializeMappings()
        {
            if (string.IsNullOrEmpty(this.StartDynamicColumnName))
            {
                _mapFunc = MapToItem;
            }
            else
            {
                _mapFunc = MapToItems;
            }
            listPropertiesMapping = PropertiesMapping;
            listPropertiesMapping.ForEach(m =>
            {
                if (m.ValueConverter == null) m.ValueConverter = ConvertValue;
            });
            headerMaps = new Dictionary<string, PropertyMap>();
            if (FixedHeaderMaps != null)
                foreach (var kv in FixedHeaderMaps)
                {
                    if (kv.Value.ValueConverter == null) kv.Value.ValueConverter = ConvertValue;
                    headerMaps[kv.Key.ToLower()] = kv.Value;
                }
            dynamicColumn = DynamicColumn;
            dynamicValue = DynamicValue;
            if (dynamicColumn != null && dynamicColumn.ValueConverter == null)
                dynamicColumn.ValueConverter = ConvertValue;
            if (dynamicValue != null && dynamicValue.ValueConverter == null)
                dynamicValue.ValueConverter = ConvertValue;
        }

        public virtual async Task<bool> Import(Stream stream, CancellationToken cancellationToken = default)
        {
            int chunk = ChunkSize;
            chunk = chunk > 0 ? chunk : DEFAULT_CHUNKSIZE;
            ValidateFormat();
            InitializeMappings();
            var fixedHeaderValues = new Dictionary<string, object>();

            int fixedColumns = listPropertiesMapping.Count();
            var dynamicColumnName = StartDynamicColumnName;
            int dynamicColumnIndex = string.IsNullOrEmpty(dynamicColumnName) ? 0 : ExcelColumnNameToNumber(dynamicColumnName);
            int row = 0;
            Dictionary<int, string> dynamicColumnKeys = new Dictionary<int, string>();
            var success = true;

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, true))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                string originalSheetId = workbookPart.Workbook.Descendants<Sheet>().First(s => SheetName.Equals(s.Name)).Id;
                WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(originalSheetId);

                WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
                string replacementPartId = workbookPart.GetIdOfPart(replacementPart);

                OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
                OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

                Row firstRow = null;
                List<Row> listRow = new List<Row>();

                IEnumerable<SharedStringItem> sharedStringTable = workbookPart.SharedStringTablePart?.SharedStringTable?.Elements<SharedStringItem>();
                while (reader.Read())
                {
                    if (reader.IsStartElement && reader.ElementType != typeof(SheetData) && reader.ElementType != typeof(Row))
                    {
                        writer.WriteStartElement(reader);
                    }
                    else if (reader.IsEndElement && reader.ElementType != typeof(SheetData) && reader.ElementType != typeof(Row))
                    {
                        writer.WriteEndElement();
                    }
                    if (reader.ElementType == typeof(SheetData))
                    {
                        if (reader.IsStartElement)
                        {
                            writer.WriteStartElement(new SheetData());
                        }
                        if (reader.IsEndElement)
                        {
                            if (listRow.Count() > 0)
                            {
                                success &= await ConvertToModelsAndSaveChunk(listRow, sharedStringTable, dynamicColumnKeys, writer, cancellationToken);
                            }
                            writer.WriteEndElement();
                        }

                    }
                    if (reader.ElementType == typeof(Row))
                    {
                        Row currentRow = (Row)reader.LoadCurrentElement();
                        row = Int32.Parse(currentRow.RowIndex.ToString());

                        if (row >= StartDataRowIndex || row == DynamicHeaderRowIndex)
                        {
                            // Write first row
                            Cell cellHeaderError = new Cell()
                            {
                                CellReference = BuildCellReference(firstRow.RowIndex, _lastDetailColumnIndex + 1),
                                DataType = CellValues.String,
                                CellValue = new CellValue("Error")
                            };
                            firstRow.Append(cellHeaderError);
                            writer.WriteElement(firstRow);

                            if (row == DynamicHeaderRowIndex)
                            {
                                foreach (Cell c in currentRow.Elements<Cell>())
                                {
                                    int columnIndex = ExcelColumnNameToNumber(c.CellReference);
                                    string cellValue = GetCellValue(c, sharedStringTable);
                                    if (row == DynamicHeaderRowIndex)
                                    {
                                        if (columnIndex >= dynamicColumnIndex)
                                        {
                                            dynamicColumnKeys.Add(columnIndex, cellValue);
                                        }
                                    }
                                    if (columnIndex > _lastDetailColumnIndex) _lastDetailColumnIndex = columnIndex;
                                }
                                writer.WriteElement(currentRow);
                            }
                            else
                            {
                                listRow.Add(currentRow);
                            }

                            if (listRow.Count() == chunk)
                            {
                                success &= await ConvertToModelsAndSaveChunk(listRow, sharedStringTable, dynamicColumnKeys, writer, cancellationToken);
                            }
                        }
                        else
                        {
                            firstRow = currentRow;
                            if (row == HeaderNameRowIndex)
                            {
                                SetHeaderPosition(currentRow, sharedStringTable);
                            }
                            if (row <= EndFixedHeaderRowIndex && headerMaps.Count > 0)
                            {
                                var (ok, fixedCells) = ReadFixedCells(currentRow, headerMaps, sharedStringTable);
                                success &= ok;
                                foreach (var kv in fixedCells)
                                {
                                    fixedHeaderValues[kv.Key] = kv.Value;
                                }
                            }
                            if (row == EndFixedHeaderRowIndex)
                            {
                                var valResults = await SaveFixedHeader(fixedHeaderValues, cancellationToken);
                                success &= valResults.IsSuccess();
                                if (valResults.IsError())
                                {
                                    string error = ErrorFormat(valResults);
                                    Cell cellError = new Cell()
                                    {
                                        DataType = CellValues.String,
                                        CellValue = new CellValue(error)
                                    };
                                    currentRow.Append(cellError);
                                }
                            }
                            writer.WriteElement(currentRow);
                        }
                    }
                }
                reader.Close();
                writer.Close();

                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Id.Value.Equals(originalSheetId)).First();
                sheet.Id.Value = replacementPartId;
                workbookPart.DeletePart(worksheetPart);
            }
            return success;
        }
    }
}
