﻿using ADR.CPS.SDK.Models;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.EntityFrameworkCore
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> Ordering<T>(this IQueryable<T> query, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc)
        {
            if (orderFunc != default)
            {
                query = orderFunc(query);
            }
            return query;

        }
        public static IQueryable<T> Includes<T>(this IQueryable<T> query, IList<Expression<Func<T, object>>> includeExpressions) where T : class
        {
            if (includeExpressions == default) return query;
            foreach (var inc in includeExpressions)
            {
                query = query.Include(inc);
            }
            return query;
        }

        public static IQueryable<T> Ordering<T>(this IQueryable<T> query, string ordering)
        {
            return string.IsNullOrWhiteSpace(ordering) ? query : query.OrderBy(ordering);
        }

        public static async Task<IPagedList<T>> PageResultAsync<T>(this IQueryable<T> query, int page, int pageSize, CancellationToken cancellationToken = default)
        {
            var result = query.PageResult(page, pageSize);
            var data = await result.Queryable.ToListAsync(cancellationToken);
            return new PagedList<T>(data, result.CurrentPage, result.PageCount, result.PageSize, result.RowCount);
        }
        private static readonly TypeInfo QueryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();

        private static readonly FieldInfo QueryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");

        private static readonly PropertyInfo NodeTypeProviderField = QueryCompilerTypeInfo.DeclaredProperties.Single(x => x.Name == "NodeTypeProvider");

        private static readonly MethodInfo CreateQueryParserMethod = QueryCompilerTypeInfo.DeclaredMethods.First(x => x.Name == "CreateQueryParser");

        private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");

        private static readonly PropertyInfo DatabaseDependenciesField
            = typeof(Database).GetTypeInfo().DeclaredProperties.Single(x => x.Name == "Dependencies");
    }
       
}
