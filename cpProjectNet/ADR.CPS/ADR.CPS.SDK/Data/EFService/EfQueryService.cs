﻿
using ADR.CPS.SDK.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ADR.CPS.SDK.Data;
using Microsoft.Extensions.DependencyInjection;


namespace ADR.CPS.SDK.Data.EFService
{
    public abstract class EfQueryService<TContext> : IAsyncQueryService
         where TContext : DbContext
    {
        protected readonly TContext dbContext;
        protected readonly IServiceProvider serviceProvider;

        public EfQueryService(IServiceProvider serviceProvider)
        {
            this.dbContext = serviceProvider.GetService<TContext>();
            this.serviceProvider = serviceProvider;
        }

        public virtual IQueryable<T> AsQueryable<T>() where T : class => dbContext.Set<T>().AsNoTracking();
        public Task<T> GetAsync<T>(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default) where T : class
            => AsQueryable<T>().FirstOrDefaultAsync<T>(predicate, cancellationToken);
        public Task<bool> ExistsAsync<T>(Expression<Func<T, bool>> predicate, CancellationToken cancellationToken = default) where T : class
        { 
            return AsQueryable<T>().AnyAsync(predicate, cancellationToken); 
        }
        public Task<IPagedList<T>> QueryAsync<T>(Expression<Func<T, bool>> predicate, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).PageResultAsync(page, pageSize, cancellationToken);
        public Task<IPagedList<T>> QueryAsync<T>(Expression<Func<T, bool>> predicate, IList<Expression<Func<T, object>>> includeExpressions, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class
            => AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).PageResultAsync(page, pageSize, cancellationToken);
        public Task<IPagedList<TView>> QueryAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, TView>> selector, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).Select(selector).PageResultAsync(page, pageSize, cancellationToken); 
        public Task<IPagedList<TView>> QueryAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, TView>> selector, IList<Expression<Func<T, object>>> includeExpressions, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).Select(selector).PageResultAsync(page, pageSize, cancellationToken);
        public Task<List<T>> QueryAsync<T>(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).ToListAsync(cancellationToken);
        public Task<List<T>> QueryAsync<T>(Expression<Func<T, bool>> predicate, IList<Expression<Func<T, object>>> includeExpressions, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class 
            => AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).ToListAsync(cancellationToken); 
        public Task<List<TView>> QueryAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, TView>> selector, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).Select(selector).ToListAsync(cancellationToken); 
        public Task<List<TView>> QueryAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, TView>> selector, IList<Expression<Func<T, object>>> includeExpressions, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).Select(selector).ToListAsync(cancellationToken); 
        public Task<IPagedList<TView>> QueryManyAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, IEnumerable<TView>>> selector, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).SelectMany(selector).PageResultAsync(page, pageSize, cancellationToken); 
        public Task<IPagedList<TView>> QueryManyAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, IEnumerable<TView>>> selector, IList<Expression<Func<T, object>>> includeExpressions, int page, int pageSize, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).SelectMany(selector).PageResultAsync(page, pageSize, cancellationToken);
        public Task<List<TView>> QueryManyAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, IEnumerable<TView>>> selector, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class 
            => AsQueryable<T>().Where(predicate).Ordering(orderFunc).SelectMany(selector).ToListAsync(cancellationToken); 
        public Task<List<TView>> QueryManyAsync<T, TView>(Expression<Func<T, bool>> predicate, Expression<Func<T, IEnumerable<TView>>> selector, IList<Expression<Func<T, object>>> includeExpressions, Func<IQueryable<T>, IOrderedQueryable<T>> orderFunc = null, CancellationToken cancellationToken = default) where T : class where TView : class =>
            AsQueryable<T>().Where(predicate).Includes(includeExpressions).Ordering(orderFunc).SelectMany(selector).ToListAsync(cancellationToken);
    }
}
