﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;

namespace qvd.Sdk.Export.Excel.Models
{
    public class CellTemplate
    {
        public string Address { get; set; }
        public bool IsFormula { get; set; }
        public string Formula { get; set; }
        public string Value { get; set; }
        public bool Merged { get; set; }
        public UInt32Value StyleId { get; set; }
        public CellDataTypes DataType { get; set; } = CellDataTypes.Text;

        public Func<object[], object> ValueFactory { get; set; }
        public Func<object[], string> AddressFactory { get; set; }
        public Func<object[], string> FormulaFactory { get; set; }
    }

    public enum CellDataTypes
    {
        Text,
        Numeric,
        DateTime,
        Boolean
    }
}
