﻿using System;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{
    public class MatrixTemplate
    {
        public List<CellTemplate> FixedHeaders { get; set; } // fixed row, column for each matrix
        public List<ColumnTemplate> Columns { get; set; } // parent-child columns in defined order

        public RowTemplate Rows { get; set; } // dynamic rows , columns
        public CellTemplate Value { get; set; }        
    }
}
