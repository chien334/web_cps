using System;
using System.Collections;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{
    public interface ISheetDataSource<THeader, TItem>
    {
        int TotalRows { get; }
        THeader Header { get; }
        IEnumerable<TItem> Columns { get; }
        IEnumerable<TItem> Details { get; }
    }

    public interface ISheetDataSource
    {
        int TotalRows { get; }
        object Header { get; }
        IEnumerable Columns { get; }
        IEnumerable Details { get; }
        Type HeaderType { get; }
        Type DetailType { get; }
    }

    public class DynamicSheetDataSource : ISheetDataSource
    {
        public int TotalRows {get;private set;}

        public object Header {get;private set;}

        public IEnumerable Columns {get;private set;}

        public IEnumerable Details {get;private set;}

        public Type HeaderType {get;private set;}

        public Type DetailType {get;private set;}
        public DynamicSheetDataSource(
            IEnumerable columns,
            IEnumerable details,
            object header,
            int totalRows,
            Type headerType,
            Type detailType)
        {
            Columns = columns;
            Details = details;
            Header = header;
            TotalRows = totalRows;
            HeaderType = headerType;
            DetailType = detailType;
        }
    }

    public class EmptySheetDataSource : ISheetDataSource
    {
        public int TotalRows => 0;
        public object Header => default;
        public IEnumerable Columns => default;
        public IEnumerable Details => default;
        public Type HeaderType => typeof(object);
        public Type DetailType => typeof(object);
    }

    public class EmptySheetDataSource<THeader, TItem> : ISheetDataSource<THeader, TItem>
    {
        public int TotalRows => 0;
        public THeader Header => default(THeader);

        public IEnumerable<TItem> Columns => new List<TItem>();

        public IEnumerable<TItem> Details => new List<TItem>();
    }
}
