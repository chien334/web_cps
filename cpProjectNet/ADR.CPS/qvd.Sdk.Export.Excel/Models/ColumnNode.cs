﻿using System;
using System.Collections.Generic;
using System.Linq;
using qvd.Sdk.Export.Excel.Utils;

namespace qvd.Sdk.Export.Excel.Models
{
    public class ColumnNode<TItem> : IEquatable<ColumnNode<TItem>>
    {
        public ColumnNode() { }
        public ColumnNode(ColumnTemplate column, TItem item, object value)
        {
            Column = column;
            Value = value;
            Item = item;
        }
        public Guid ID { get; set; }
        public ColumnTemplate Column { get; set; }
        public object Value { get; set; }
        public string Address { get; set; }
        public int ColumnIndex => AddressUtils.GetColumn(Address);
        public int Start { get; set; }
        public int Count { get; set; }
        public int End => Start + Count - 1;
        public bool IsLeaf { get; set; }

        public TItem Item { get; set; }

        public object[] Hierarchies { get; set; }

        public bool Equals(ColumnNode<TItem> other)
        => (Value == null && other.Value == null) || other.Value.Equals(Value);

        public override bool Equals(object obj) => Equals(obj as ColumnNode<TItem>);
        public override int GetHashCode() => Value?.GetHashCode() ?? 0;
    }
    public class ColumnAggregator<TItem>
    {
        protected List<List<ColumnNode<TItem>>> _matrix;
        protected readonly int _length;

        public ColumnAggregator(int length)
        {
            _matrix = new List<List<ColumnNode<TItem>>>();
            _length = length;
        }

        public List<ColumnNode<TItem>> CollectNodes()
        {
            return _matrix.SelectMany(r => r)
                .GroupBy(x => x.ID)
                .Select(g => 
                {
                    var node = g.First();
                    node.Count = g.Max(x => x.Count);
                    node.Start = g.Max(x => x.Start);
                    return node;
                })
                .ToList();
        }

        public void Append(List<ColumnNode<TItem>> items)
        {
            if (items.Count != _length) throw new ArgumentException();
            items.ForEach(i =>
            {
                i.Count = 1;
                i.ID = Guid.NewGuid();
            });
            items[_length - 1].IsLeaf = true;
            items[_length - 1].Hierarchies = items.Select(x => x.Value).ToArray();
            if (_matrix.Count == 0)
            {
                _matrix.Add(items);
            }
            else
            {
                var prev = _matrix.Last();
                if (prev.SequenceEqual(items)) return;
                items[_length - 1].Start = prev[_length - 1].Start + prev[_length - 1].Count;
                for (int i = _length - 1; i > 0; i--)
                {
                    if (prev.Take(i).SequenceEqual(items.Take(i)))
                    {
                        items[i - 1].Count += prev[i - 1].Count;
                        items[i - 1].ID = prev[i - 1].ID;
                    }
                    else
                    {
                        items[i - 1].Start = prev[i - 1].Start + prev[i - 1].Count;
                    }
                }
                _matrix.Add(items);
            }
        }
    }

}
