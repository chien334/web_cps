﻿using System;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{
    public class SummaryTemplate : CellTemplate
    {
        public CellTemplate SumValue { get; set; }
    }
}
