﻿using System;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{
    public class ColumnTemplate
    {
        public List<CellTemplate> Titles { get; set; } = new List<CellTemplate>();//Generate by columns in group
        public CellTemplate Value { get; set; } //Generate by columns in group
        //public List<SummaryTemplate> Summaries { get; set; } = new List<SummaryTemplate>();// not yet support summaries        
    }
}
