﻿using System;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{

    ///
    /// Support argument: totalRows, row, column, startColumn, endColumn, startRow, endRows
    ///
    public class SheetTemplate
    {     
        public string SheetName { get; set; }
        public MatrixTemplate Matrix { get; set; }
        public string AutoFilter { get; set; }
    }
}
