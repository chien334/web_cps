﻿using System;
using System.Collections.Generic;

namespace qvd.Sdk.Export.Excel.Models
{
    public class RowTemplate
    {
        public List<CellTemplate> Cells { get; set; }        

        // // Currently not support this field
        // public List<CellTemplate> Summaries { get; set; }        
        // // Currently not support this field
        // public GroupButtonPosition GroupButton { get; set; }
        // // Currently not support this field
        // public RowTemplate NestedRow { get; set; }
    }

    public enum GroupButtonPosition
    {
        None,
        Top,
        Bottom
    }
}
