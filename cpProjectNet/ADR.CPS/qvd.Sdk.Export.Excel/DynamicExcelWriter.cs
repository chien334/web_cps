using qvd.Sdk.Export.Excel.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.VariantTypes;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using ADR.CPS.SDK.Extensions;
using System.Collections;
using qvd.Sdk.Export.Excel.Utils;
using System.Globalization;

namespace qvd.Sdk.Export.Excel
{
    public class DynamicExcelWriter
    {
        #region interpolation
        protected Tuple<string, Type>[] GetInterpolatorArgs(Type dataType) => new Tuple<string, Type>[]{
                new Tuple<string, Type>("totalRows", typeof(int)),
                new Tuple<string, Type>("rowIndex", typeof(int)),
                new Tuple<string, Type>("column", typeof(int)),
                new Tuple<string, Type>("startRow", typeof(int)),
                new Tuple<string, Type>("endRow", typeof(int)),
                new Tuple<string, Type>("startColumn", typeof(int)),
                new Tuple<string, Type>("endColumn", typeof(int)),
                new Tuple<string, Type>("data", dataType),
            };

        protected Cell CreateCell(CellTemplate cell,
                                int totalRows = 0,
                                int rowIndex = 0,
                                int column = 0,
                                int startRow = 0,
                                int endRow = 0,
                                int startColumn = 0,
                                int endColumn = 0,
                                object data = default) =>
                    CreateCell(cell, new object[] { totalRows, rowIndex, column, startRow, endRow, startColumn, endColumn, data });

        protected string Interpolate(Func<object[], string> interpolator, int totalRows = 0,
                        int rowIndex = 0,
                        int column = 0,
                        int startRow = 0,
                        int endRow = 0,
                        int startColumn = 0,
                        int endColumn = 0,
                        object data = default) =>
            interpolator(new object[] { totalRows, rowIndex, column, startRow, endRow, startColumn, endColumn, data });

        protected object InterpolateValue(Func<object[], object> interpolator, int totalRows = 0,
                        int rowIndex = 0,
                        int column = 0,
                        int startRow = 0,
                        int endRow = 0,
                        int startColumn = 0,
                        int endColumn = 0,
                        object data = default) =>
            interpolator(new object[] { totalRows, rowIndex, column, startRow, endRow, startColumn, endColumn, data });

        protected Func<object[], string> BuildAddressFactory(string value, Type dataType)
        {
            var interpolator = value.BuildInterpolator(GetInterpolatorArgs(dataType));
            return (args) =>
            {
                var templateAddress = interpolator(args);
                return AddressUtils.ConvertToExcelAddress(templateAddress);
            };
        }

        protected Func<object[], object> BuildFunc(string value, Type dataType) => value.BuildFunction(GetInterpolatorArgs(dataType));
        #endregion interpolation

        #region Template preparation
        protected SheetTemplate PrepareTemplate(SheetTemplate templateDesc, ISheetDataSource datasource, int totalRows = 0)
        {
            // TODO: Update address factory functions
            templateDesc.Matrix = PrepareTemplate(templateDesc.Matrix, datasource);
            return templateDesc;
        }

        protected MatrixTemplate PrepareTemplate(MatrixTemplate matrix, ISheetDataSource datasource)
        {
            matrix.FixedHeaders.ForEach(cell => PrepareTemplate(cell, datasource.HeaderType));

            if (matrix.Columns != null)
                matrix.Columns.ForEach(column => PrepareTemplate(column, datasource.DetailType));
            PrepareTemplate(matrix.Rows, datasource.DetailType);
            if (matrix.Value != null)
                PrepareTemplate(matrix.Value, datasource.DetailType);
            return matrix;
        }

        protected CellTemplate PrepareTemplate(CellTemplate cell, Type dataSourceType)
        {
            cell.AddressFactory = BuildAddressFactory(cell.Address, dataSourceType);
            Func<object[], object> valueFunc = null;
            if (cell.Value?.StartsWith("{") == true && cell.Value?.EndsWith("}") == true)
                valueFunc = BuildFunc(cell.Value, dataSourceType);
            else
            {
                valueFunc = (_) => cell.Value;
            }
            cell.ValueFactory = valueFunc;
            if (cell.IsFormula)
            {
                var interpolator = cell.Formula.BuildInterpolator(GetInterpolatorArgs(dataSourceType));
                cell.FormulaFactory = (args) => interpolator(args);
            }

            return cell;
        }
        protected Cell CreateCell(CellTemplate c, object[] args)
        {
            var value = c.ValueFactory(args);
            var address = c.AddressFactory(args);
            var formula = c.FormulaFactory == null ? string.Empty : c.FormulaFactory(args);
            return CreateCell(c, address, value, formula);
        }

        protected Cell CreateCell(CellTemplate c, string address, object value, string formula)
        => c.DataType switch
        {
            CellDataTypes.Numeric => CreateNumericCell(value, address, c.IsFormula, formula, c.StyleId),
            CellDataTypes.DateTime => CreateDateTimeCell(value, address, c.IsFormula, formula, c.StyleId),
            CellDataTypes.Boolean => CreateBooleanCell(value, address, c.IsFormula, formula, c.StyleId),
            _ => CreateTextCell(value, address, c.IsFormula, formula, c.StyleId)
        };

        protected RowTemplate PrepareTemplate(RowTemplate row, Type itemType)
        {
            row.Cells.ForEach(c => PrepareTemplate(c, itemType));
            // row.Summaries.ForEach(c => PrepareTemplate(c, styleTemplate, totalRows, dataSource: dataSource));

            // if (row.NestedRow != null)
            //     row.NestedRow = PrepareTemplate(row.NestedRow, styleTemplate, totalRows, dataSource: dataSource);
            return row;
        }

        protected ColumnTemplate PrepareTemplate(ColumnTemplate column, Type itemType)
        {
            if (column.Titles != null)
                column.Titles.ForEach(c => PrepareTemplate(c, itemType));
            if (column.Value != null)
                PrepareTemplate(column.Value, itemType);

            return column;
        }
        #endregion Template preparation

        #region Main Method
        public Dictionary<string, int> Write(string excelFilePath, string styleTemplatePath, Dictionary<string, ISheetDataSource> dataSources)
        {
            using var reader = new TemplateReader();
            var sheetTemplates = reader.ParseTemplate(styleTemplatePath);

            foreach (var template in sheetTemplates)
            {
                ISheetDataSource dataSource;
                if (dataSources.ContainsKey(template.SheetName))
                    dataSource = dataSources[template.SheetName];
                else
                    dataSource = new EmptySheetDataSource();
                PrepareTemplate(template, dataSource);
            }

            var detailCount = new Dictionary<string, int>();

            using var document = SpreadsheetDocument.Create(excelFilePath, SpreadsheetDocumentType.Workbook, false);
            document.AddWorkbookPart();
            WorkbookPart workbookPart = document.WorkbookPart;
            WorkbookStylesPart stylePart = workbookPart.AddNewPart<WorkbookStylesPart>();

            // Write sheet data
            var sheets = WriteSheets(workbookPart, sheetTemplates, dataSources, detailCount);

            var oxw = OpenXmlWriter.Create(document.WorkbookPart);
            oxw.WriteStartElement(new Workbook());
            oxw.WriteStartElement(new Sheets());

            // Write all sheet information here
            foreach (var sheet in sheets)
            {
                oxw.WriteElement(sheet);
            }

            oxw.WriteEndElement();

            // this is for Workbook
            oxw.WriteEndElement();
            oxw.Close();

            oxw = OpenXmlWriter.Create(stylePart);
            oxw.WriteElement(reader.StyleTemplate);
            oxw.Close();
            document.Close();
            return detailCount;
        }

        protected List<Sheet> WriteSheets(WorkbookPart workbook, List<SheetTemplate> templates, Dictionary<string, ISheetDataSource> dataSources, Dictionary<string, int> detailCount)
        {
            var sheets = new List<Sheet>();
            uint sheetId = 1;
            foreach (var template in templates)
            {
                ISheetDataSource source;
                if (dataSources.ContainsKey(template.SheetName))
                    source = dataSources[template.SheetName];
                else
                    source = new EmptySheetDataSource();
                var (sheet, recordCount) = WriteSheet(workbook, template, sheetId, source);
                sheetId++;
                sheets.Add(sheet);
                detailCount[template.SheetName] = recordCount;
            }
            return sheets;
        }

        protected Tuple<Sheet, int> WriteSheet(WorkbookPart workbookPart, SheetTemplate template, uint sheetId, ISheetDataSource datasource)
        {
            WorksheetPart wsp = workbookPart.AddNewPart<WorksheetPart>();
            var oxw = OpenXmlWriter.Create(wsp);
            oxw.WriteStartElement(new Worksheet());
            var properties = GetSheetProperties(sheetId);
            if (properties != null)
                oxw.WriteElement(properties);

            oxw.WriteStartElement(new SheetData());

            var mergedCells = new List<string>();
            var (mergedInBody, recordCount) = WriteMatrix(oxw, template.Matrix, datasource);
            mergedCells.AddRange(mergedInBody);

            // this is for SheetData
            oxw.WriteEndElement();

            WriteMergedCells(oxw, mergedCells);

            // this is for Worksheet
            oxw.WriteEndElement();
            oxw.Close();
            return new Tuple<Sheet, int>(new Sheet()
            {
                Name = template.SheetName,
                SheetId = sheetId,
                Id = workbookPart.GetIdOfPart(wsp)
            }, recordCount);
        }

        protected void WriteMergedCells(OpenXmlWriter oxw, List<string> mergedCells)
        {
            var merged = mergedCells
               .Select(x => new MergeCell { Reference = new StringValue(x) })
               .ToArray();
            if (merged.Length > 0)
            {
                oxw.WriteElement(new MergeCells(merged) { Count = (uint)merged.Length });
            }
        }

        private Tuple<List<string>, int> WriteMatrix(OpenXmlWriter oxw, MatrixTemplate matrix, ISheetDataSource datasource)
        {
            var mergedCells = new List<string>();
            /*
            TODO:
            1. Prepare all Fixed headers
            2. Generate column headers & mapping
            3. Check row for generated column headers to ensure write excel row incremental            
            3. Generate cell values
            */
            var fixedHeaderBuilder = matrix.FixedHeaders.Select(cellTemplate =>
                        new
                        {
                            Merged = cellTemplate.Merged,
                            Cell = CreateCell(cellTemplate, totalRows: datasource.TotalRows, data: datasource.Header)
                        });
            var matrixHeaders = fixedHeaderBuilder.Select(x => x.Cell).ToList();
            matrixHeaders.ForEach(c => c.CellReference = c.CellReference.ToString().Split(':')[0]);
            var mergedFixed = fixedHeaderBuilder.Where(x => x.Merged)
                                    .Select(x => x.Cell.CellReference.ToString());

            // Add fixed merged cell to list
            mergedCells.AddRange(mergedFixed);

            // Build dynamic column header & mapping            
            var (valueMaps, cells, mergedHeaders) = CreateColumnHeaders(matrix.Columns, datasource.Columns, datasource.TotalRows);

            mergedCells.AddRange(mergedHeaders);

            matrixHeaders.AddRange(cells);
            if (matrixHeaders.Count > 0)
                WriteCells(oxw, matrixHeaders);

            var (mergedInBody, recordCount) = WriteMatrixBody(oxw, matrix, valueMaps, datasource.Details, datasource.TotalRows);
            mergedCells.AddRange(mergedInBody);
            return new Tuple<List<string>, int>(mergedCells, recordCount);
        }

        protected Tuple<List<string>, int> WriteMatrixBody(OpenXmlWriter oxw,
            MatrixTemplate matrix,
            Dictionary<object[], int> valueMaps,
            IEnumerable datasource,
            int totalRows)
        {
            var mergedCells = new List<string>();
            int index = 0;
            object[] rowValues = null;
            List<CellMapInfo> rowCells = new List<CellMapInfo>();
            var dynamicIndexes = valueMaps.Values.ToList();
            List<int> valueIndexes = new List<int>();
            int recordCount = 0;
            foreach (var data in datasource)
            {
                // Collect Cells
                var values = matrix.Rows.Cells.Select(c => InterpolateValue(c.ValueFactory, totalRows: totalRows, data: data)).ToArray();
                if (rowValues == null || !rowValues.SequenceEqual(values)) // new rows
                {
                    if (rowCells.Count > 0)
                    {
                        // TODO: Add Summary Cells                
                        WriteValueRow(oxw, matrix.Value, dynamicIndexes, valueIndexes, index, totalRows, rowCells, mergedCells);
                        rowCells.Clear();
                        valueIndexes.Clear();
                    }
                    index++;
                    var rowHeaderCells = matrix.Rows.Cells.Select(cellTemplate =>
                       new CellMapInfo
                       {
                           Merged = cellTemplate.Merged,
                           Cell = CreateCell(cellTemplate, totalRows: totalRows, rowIndex: index, data: data)
                       }).ToList();
                    rowCells.AddRange(rowHeaderCells);
                    rowValues = values;
                }

                var columnKey = BuildColumnIndex(matrix.Columns, data, totalRows);

                if (valueMaps.ContainsKey(columnKey))
                {
                    var column = valueMaps[columnKey];
                    var valueCell = CreateCell(matrix.Value,
                                rowIndex: index,
                                column: column,
                                totalRows: totalRows,
                                data: data);
                    valueIndexes.Add(column);
                    rowCells.Add(new CellMapInfo
                    {
                        Merged = matrix.Value.Merged,
                        Cell = valueCell
                    });
                }
                recordCount++;
            }
            if (rowCells.Count > 0)
            {
                WriteValueRow(oxw, matrix.Value, dynamicIndexes, valueIndexes, index, totalRows, rowCells, mergedCells);
                rowCells.Clear();
            }
            return new Tuple<List<string>, int>(mergedCells, recordCount);
        }

        protected void WriteValueRow(
            OpenXmlWriter oxw,
            CellTemplate value,
            List<int> dynamicIndexes,
            List<int> valueIndexes,
            int rowIndex,
            int totalRows,
            List<CellMapInfo> rowCells,
            List<string> mergedCells)
        {
            foreach (var i in dynamicIndexes)
            {
                if (!valueIndexes.Contains(i))
                {
                    var address = Interpolate(
                        value.AddressFactory,
                        rowIndex: rowIndex,
                        column: i,
                        totalRows: totalRows,
                        data: default
                    );
                    var formula = value.FormulaFactory == null ? ""
                        : Interpolate(
                            value.FormulaFactory,
                            rowIndex: rowIndex,
                            column: i,
                            totalRows: totalRows,
                            data: default
                        );
                    rowCells.Add(new CellMapInfo
                    {
                        Cell = CreateCell(value, address, null, formula),
                        Merged = value.Merged
                    });
                }
            }

            WriteCells(oxw, rowCells);
            AddMergedCell(mergedCells, rowCells);
        }

        protected void WriteCells(OpenXmlWriter oxw, IEnumerable<CellMapInfo> cells)
        {
            var rowInfos = cells.Select(c => new
            {
                Row = AddressUtils.GetRow(c.Cell.CellReference),
                Column = AddressUtils.GetColumn(c.Cell.CellReference),
                Cell = c.Cell
            })
            .GroupBy(c => c.Row)
            .OrderBy(g => g.Key);
            foreach (var info in rowInfos)
            {
                var row = new Row { RowIndex = info.Key };
                foreach (var c in info.OrderBy(i => i.Column))
                {
                    row.Append(c.Cell);
                }
                oxw.WriteElement(row);
            }
        }

        protected void WriteCells(OpenXmlWriter oxw, IEnumerable<Cell> cells)
        {
            var rowInfos = cells.Select(c => new
            {
                Row = AddressUtils.GetRow(c.CellReference),
                Column = AddressUtils.GetColumn(c.CellReference),
                Cell = c
            })
            .GroupBy(c => c.Row)
            .OrderBy(g => g.Key);
            foreach (var info in rowInfos)
            {
                var row = new Row { RowIndex = info.Key };
                foreach (var c in info.OrderBy(i => i.Column))
                {
                    row.Append(c.Cell);
                }
                oxw.WriteElement(row);
            }
        }

        protected void AddMergedCell(List<string> mergedCells, IEnumerable<CellMapInfo> cells)
        => mergedCells.AddRange(cells.Where(c => c.Merged).Select(c => c.Cell.CellReference.ToString()));

        protected Tuple<List<Cell>, List<string>> BuildCells(ColumnNode<object> node, int totalRows, object item)
        {
            List<Cell> cells = new List<Cell>();
            List<string> mergedCells = new List<string>();
            // Create value cells
            string formula = null;
            if (node.Column.Value != null && node.Column.Value.FormulaFactory != null)
                formula = Interpolate(node.Column.Value.FormulaFactory, totalRows: totalRows, startColumn: node.Start, endColumn: node.End, data: node.Item);
            if (node.Column.Value.Merged)
            {
                var cell = CreateCell(node.Column.Value, node.Address.Split(':')[0], node.Value, formula);
                mergedCells.Add(node.Address);
                cells.Add(cell);
            }
            else
            {
                var (start, end) = AddressUtils.ParseExcelAddress(node.Address);
                if (end == null)
                {
                    var cell = CreateCell(node.Column.Value, node.Address.Split(':')[0], node.Value, formula);
                    cells.Add(cell);
                }
                else
                {
                    for (var i = start.Column; i <= end.Column; i++)
                    {
                        var address = $"{AddressUtils.ToColumnName(i)}{start.Row}";
                        var cell = CreateCell(node.Column.Value, address, node.Value, formula);
                        cells.Add(cell);
                    }
                }
            }

            // Create title cells
            var titles = node.Column.Titles.Select(x =>
            new Tuple<Cell, bool>(
                CreateCell(x, totalRows: totalRows, startColumn: node.Start, endColumn: node.End, data: item),
                x.Merged
             ));
            cells.AddRange(titles.Select(x =>
            {
                x.Item1.CellReference = x.Item1.CellReference.ToString().Split(':')[0];
                return x.Item1;
            }));
            mergedCells.AddRange(titles.Where(x => x.Item2).Select(x => x.Item1.CellReference.ToString()));
            return new Tuple<List<Cell>, List<string>>(cells, mergedCells);
        }

        // TODO: build cells from leaf node to get start, end

        protected Tuple<Dictionary<object[], int>, List<Cell>, List<string>> CreateColumnHeaders(List<ColumnTemplate> columnTemplates, IEnumerable datasource, int totalRows)
        {
            var columnCells = new List<Cell>();

            // TODO: create rawvalue from template
            var aggregator = new ColumnAggregator<object>(columnTemplates.Count);
            foreach (var item in datasource)
            {
                var values = CreateColumnNodes(columnTemplates, item, totalRows);
                aggregator.Append(values);
            }
            var nodes = aggregator.CollectNodes();
            nodes.ForEach(node => node.Address = Interpolate(node.Column.Value.AddressFactory, totalRows: totalRows, startColumn: node.Start, endColumn: node.End, data: node.Item));
            var cellBuilder = nodes.Select(n => BuildCells(n, totalRows, n.Item));
            var cells = cellBuilder.SelectMany(x => x.Item1);
            var mergedCells = cellBuilder.SelectMany(x => x.Item2).ToList();
            var valueMaps = nodes.Where(x => x.IsLeaf).ToDictionary(
                x => x.Hierarchies,
                x => x.ColumnIndex,
                new KeyEqualityComparer<object[]>((a, b) => a.SequenceEqual(b))
            );
            columnCells.AddRange(cells);

            return new Tuple<Dictionary<object[], int>, List<Cell>, List<string>>(valueMaps, columnCells, mergedCells);
        }

        protected List<ColumnNode<object>> CreateColumnNodes(List<ColumnTemplate> columnTemplates, object item, int totalRows)
        => columnTemplates.Select(column => new ColumnNode<object>(column, item,
                                    column.Value == null ? null : InterpolateValue(column.Value.ValueFactory,
                                                        totalRows: totalRows,
                                                        data: item)
                                                        )).ToList();

        protected object[] BuildColumnIndex(List<ColumnTemplate> columnTemplates, object item, int totalRows)
        => columnTemplates.Select(column => column.Value == null ? null
                                    : InterpolateValue(column.Value.ValueFactory, totalRows: totalRows, data: item))
                        .ToArray();

        protected class CellMapInfo
        {
            public CellMapInfo()
            {
            }

            public CellMapInfo(Cell cell, bool merged)
            {
                Cell = cell;
                Merged = merged;
            }

            public Cell Cell { get; set; }
            public bool Merged { get; set; }
            public bool Summary { get; set; }
            public object RawValue { get; set; }
            public bool IsLeaf { get; set; }
            public int StartColumn { get; set; }
            public int EndColumn { get; set; }
            public int StartRow { get; set; }
            public int EndRow { get; set; }
            public List<object> ValueMaps { get; set; }
        }

        #endregion Main Method

        #region Common Method
        protected virtual SheetProperties GetSheetProperties(uint sheetId)
        {
            return null;
        }
        protected Cell CreateDateTimeCell(object value, string address, bool isFormula, string formula, UInt32Value styleId)
        {
            var content = string.Empty;
            if (value is DateTime) content = ((DateTime)value).ToOADate().ToString(CultureInfo.InvariantCulture);
            else content = value?.ToString();
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            if (!string.IsNullOrEmpty(content))
            {
                if (isFormula)
                {
                    cell.Append(new CellFormula(formula));
                }
                cell.CellValue = new CellValue(content);
            }
            return cell;
        }

        protected Cell CreateBooleanCell(object value, string address, bool isFormula, string formula, UInt32Value styleId)
        {
            var content = value?.ToString() ?? string.Empty;
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            if (!string.IsNullOrEmpty(content))
            {
                cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "b"));
                if (isFormula)
                {
                    cell.Append(new CellFormula(formula));
                }
                cell.CellValue = new CellValue(content);
            }
            return cell;
        }

        protected Cell CreateNumericCell(object value, string address, bool isFormula, string formula, UInt32Value styleId)
        {
            var content = value?.ToString() ?? string.Empty;
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            if (!string.IsNullOrEmpty(content))
            {
                cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "n"));
                if (isFormula)
                {
                    cell.Append(new CellFormula(formula));
                }
                cell.CellValue = new CellValue(content);
            }
            return cell;
        }

        protected Cell CreateTextCell(object value, string address, bool isFormula, string formula, UInt32Value styleId)
        {
            var content = value?.ToString() ?? string.Empty;
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            if (!string.IsNullOrEmpty(content))
            {
                if (isFormula)
                {
                    cell.Append(new CellFormula(formula));
                    cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "str"));
                    cell.CellValue = new CellValue(content);
                }
                else
                {
                    cell.InlineString = new InlineString() { Text = new Text(content) };
                    cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "inlineStr"));
                }
            }
            return cell;
        }
        #endregion Common Method
    }

}