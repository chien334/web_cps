using qvd.Sdk.Export.Excel.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.VariantTypes;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Collections;
using qvd.Sdk.Export.Excel.Utils;
using System.Globalization;

namespace qvd.Sdk.Export.Excel
{
    public abstract class ExcelExporterBase
    {
        protected abstract string[] SheetNames { get; }
        protected abstract void WriteHeader(OpenXmlWriter oxw, string sheetName, object datasource);
        protected abstract void WriteBody(OpenXmlWriter oxw, string sheetName, object datasource);

        protected abstract List<string> GetMergedCells(string sheetName);

        private readonly Dictionary<string, SheetData> _sheetTemplates;
        private readonly Stylesheet _templateStyleSheet;

        public ExcelExporterBase(string tempateFilePath)
        {
            using var styleTemplate = SpreadsheetDocument.Open(tempateFilePath, false);
            var workbookPart = styleTemplate.WorkbookPart;
            _templateStyleSheet = workbookPart.WorkbookStylesPart.Stylesheet.Clone() as Stylesheet;
            _sheetTemplates = workbookPart.Workbook.Descendants<Sheet>()
                .Select(x => new
                {
                    Name = x.Name.Value,
                    Sheet = (WorksheetPart)workbookPart.GetPartById(x.Id)
                })
                .ToDictionary(
                    x => x.Name,
                    x => x.Sheet.Worksheet.Elements<SheetData>().First()
                );

        }

        protected virtual SheetProperties GetSheetProperties(string sheetName) => null;

        protected virtual WorksheetPart GetWorksheetPart(WorkbookPart workbookPart, string sheetName)
        {
            string relId = workbookPart.Workbook.Descendants<Sheet>().First(s => sheetName.Equals(s.Name)).Id;
            return (WorksheetPart)workbookPart.GetPartById(relId);
        }

        public virtual void Export(object datasource, string exportFilePath)
        {
            using var document = SpreadsheetDocument.Create(exportFilePath, SpreadsheetDocumentType.Workbook, false);
            document.AddWorkbookPart();
            WorkbookPart workbookPart = document.WorkbookPart;
            WorkbookStylesPart stylePart = workbookPart.AddNewPart<WorkbookStylesPart>();
            // Write sheet data
            var sheets = WriteSheets(workbookPart, datasource);

            var oxw = OpenXmlWriter.Create(document.WorkbookPart);
            oxw.WriteStartElement(new Workbook());
            oxw.WriteStartElement(new Sheets());

            // Write all sheet information here
            foreach (var sheet in sheets)
            {
                oxw.WriteElement(sheet);
            }

            oxw.WriteEndElement();

            // this is for Workbook
            oxw.WriteEndElement();
            oxw.Close();

            oxw = OpenXmlWriter.Create(stylePart);
            oxw.WriteElement(_templateStyleSheet);
            oxw.Close();
            document.Close();
        }

        protected virtual List<Sheet> WriteSheets(WorkbookPart workbookPart, object datasource)
        {
            var sheetNames = SheetNames;
            var sheets = new List<Sheet>();
            uint sheetId = 1;
            foreach (var name in sheetNames)
            {
                var sheet = WriteSheet(workbookPart, sheetId, name, datasource);
                sheetId++;
                sheets.Add(sheet);
            }
            return sheets;
        }

        protected virtual Sheet WriteSheet(WorkbookPart workbookPart, uint sheetId, string sheetName, object datasource)
        {
            WorksheetPart wsp = workbookPart.AddNewPart<WorksheetPart>();
            var oxw = OpenXmlWriter.Create(wsp);
            oxw.WriteStartElement(new Worksheet());
            var properties = GetSheetProperties(sheetName);
            if (properties != null)
                oxw.WriteElement(properties);

            oxw.WriteStartElement(new SheetData());

            var mergedCells = new List<string>();
            WriteHeader(oxw, sheetName, datasource);
            WriteBody(oxw, sheetName, datasource);

            // this is for SheetData
            oxw.WriteEndElement();

            WriteMergedCells(oxw, sheetName);

            // this is for Worksheet
            oxw.WriteEndElement();
            oxw.Close();
            return new Sheet()
            {
                Name = sheetName,
                SheetId = sheetId,
                Id = workbookPart.GetIdOfPart(wsp)
            };
        }

        protected void WriteMergedCells(OpenXmlWriter oxw, string sheetName)
        {
            List<string> mergedCells = GetMergedCells(sheetName);
            var merged = mergedCells
               .Select(x => new MergeCell { Reference = new StringValue(x) })
               .ToArray();
            if (merged.Length > 0)
            {
                oxw.WriteElement(new MergeCells(merged) { Count = (uint)merged.Length });
            }
        }

        protected Cell CreateDateTimeCell(object value, string address, string sheetName, string templateAddress)
        => CreateDateTimeCell(value, address, GetStyleIndex(sheetName, templateAddress));
        protected Cell CreateNumericCell(object value, string address, string sheetName, string templateAddress)
        => CreateNumericCell(value, address, GetStyleIndex(sheetName, templateAddress));
        protected Cell CreateTextCell(object value, string address, string sheetName, string templateAddress)
        => CreateTextCell(value, address, GetStyleIndex(sheetName, templateAddress));

        protected Cell CreateDateTimeCell(object value, string address, UInt32Value styleId)
        {
            var content = string.Empty;
            if (value is DateTime) content = ((DateTime)value).ToOADate().ToString(CultureInfo.InvariantCulture);
            else content = value?.ToString();
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            cell.CellValue = new CellValue(content);
            return cell;
        }

        protected Cell CreateNumericCell(object value, string address, UInt32Value styleId)
        {
            var content = value?.ToString() ?? string.Empty;
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "n"));
            cell.CellValue = new CellValue(content);
            return cell;
        }

        protected Cell CreateTextCell(object value, string address, UInt32Value styleId)
        {
            var content = value?.ToString() ?? string.Empty;
            var cell = new Cell();
            cell.CellReference = address;
            cell.StyleIndex = styleId;
            cell.InlineString = new InlineString() { Text = new Text(content) };
            cell.SetAttribute(new OpenXmlAttribute("t", string.Empty, "inlineStr"));
            return cell;
        }

        protected UInt32Value GetStyleIndex(string sheetName, string address)
        {
            return _sheetTemplates[sheetName].Elements<Row>()
            .SelectMany(r => r.Elements<Cell>())
            .FirstOrDefault(c => c.CellReference == address.Replace("$", ""))
            ?.StyleIndex ?? 0;
        }
    }
}