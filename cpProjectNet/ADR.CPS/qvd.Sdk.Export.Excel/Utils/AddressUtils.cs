using System;
using System.Linq;
using System.Linq.Dynamic.Core.CustomTypeProviders;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis.CSharp.Scripting;

namespace qvd.Sdk.Export.Excel.Utils
{
    [DynamicLinqType]
    public static class AddressUtils
    {
        public static string GetColumnName(string baseColumn, int index)
        => ToColumnName((uint)(ToColumnIndex(baseColumn) + index));

        public static int ToColumnIndex(string columnName)
        {
            if (string.IsNullOrEmpty(columnName)) throw new ArgumentNullException("columnName");

            columnName = columnName.ToUpperInvariant();

            int sum = 0;

            for (int i = 0; i < columnName.Length; i++)
            {
                sum *= 26;
                sum += (columnName[i] - 'A' + 1);
            }

            return sum;
        }

        public static string ToColumnName(uint columnNumber)
        {
            uint dividend = columnNumber;
            string columnName = String.Empty;
            uint modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (uint)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public static string GetColumnName(string cellAddress)
        {
            // Create a regular expression to match the column name portion of the cell name.
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellAddress);
            return match.Value;
        }

        public static int GetColumn(string cellAddress)
        {
            // Create a regular expression to match the column name portion of the cell name.
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellAddress);
            return ToColumnIndex(match.Value);
        }

        public static int GetLastColumn(string cellAddress)
        {
            // Create a regular expression to match the column name portion of the cell name.
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellAddress.Split(":").Last());
            return ToColumnIndex(match.Value);
        }

        // Given a cell name, parses the specified cell to get the row index.
        public static UInt32 GetRow(string cellAddress)
        {
            // Create a regular expression to match the row index portion the cell name.
            var regex = new Regex("\\d+");
            var match = regex.Match(cellAddress);
            return UInt32.Parse(match.Value);
        }

        public static bool EqualsAddress(string address1, string address2)
        {
            return (address1.Replace("$", "").ToLower() == address2.Replace("$", "").ToLower());
        }

        public static string ConvertToExcelAddress(string templateAddress)
        {
            var (start, end) = ParseTemplate(templateAddress);
            if (start == null && end == null)
                return "";
            if (end == null)
                return start.ToExcellAddress();
            return $"{start.ToExcellAddress()}:{end.ToExcellAddress()}";
        }

        public static Tuple<CellIndex, CellIndex> ParseTemplate(string templateAddress)
        {
            var cellIndexes = templateAddress.Split(":").Select(x =>
             {
                 var addr = x.Split(",");
                 var row = UInt32.Parse(addr[0].Replace("$", ""));
                 var column = UInt32.Parse(addr[1].Replace("$", ""));
                 var fixedRow = addr[0].Contains("$");
                 var fixedColumn = addr[1].Contains("$");
                 return new CellIndex(row, column, fixedRow, fixedColumn);
             }).ToArray();
            return cellIndexes.Length switch
            {
                1 => new Tuple<CellIndex, CellIndex>(cellIndexes[0], null),
                2 => new Tuple<CellIndex, CellIndex>(cellIndexes[0], cellIndexes[1]),
                _ => new Tuple<CellIndex, CellIndex>(null, null)
            };
        }

        public static string ConvertToTemplateAddress(string excelAddress)
        {
            var (start, end) = ParseExcelAddress(excelAddress);
            string endAddress = "";
            if (end != null)
                endAddress = $":{end.ToTemplateAddress()}";
            return start.ToTemplateAddress() + endAddress;
        }

        public static Tuple<CellIndex, CellIndex> ParseExcelAddress(string excelAddress)
        {
            var cellIndexes = excelAddress.Split(":").Select(x =>
             {
                 var row = AddressUtils.GetRow(x);
                 var column = AddressUtils.GetColumn(x);
                 var fixedRow = x.Trim().TrimStart('$').Contains("$");
                 var fixedColumn = x.Trim().StartsWith("$");
                 return new CellIndex(row, (uint)column, fixedRow, fixedColumn);
             }).ToArray();
            return cellIndexes.Length switch
            {
                1 => new Tuple<CellIndex, CellIndex>(cellIndexes[0], null),
                2 => new Tuple<CellIndex, CellIndex>(cellIndexes[0], cellIndexes[1]),
                _ => new Tuple<CellIndex, CellIndex>(null, null)
            };
        }
    }

    public class CellIndex
    {
        public CellIndex()
        {
        }

        public CellIndex(uint row, uint column, bool fixedRow = false, bool fixedColumn = false)
        {
            Column = column;
            Row = row;
            FixedRow = fixedRow;
            FixedColumn = fixedColumn;
        }

        public string ToTemplateAddress()
        {
            var columnPrefix = FixedColumn ? "$" : "";
            var rowPrefix = FixedRow ? "$" : "";
            return $"{rowPrefix}{Row},{columnPrefix}{Column}";
        }

        public string ToExcellAddress()
        {
            var columnPrefix = FixedColumn ? "$" : "";
            var rowPrefix = FixedRow ? "$" : "";
            return $"{columnPrefix}{AddressUtils.ToColumnName(Column)}{rowPrefix}{Row}";
        }

        public uint Column { get; set; }
        public uint Row { get; set; }
        public bool FixedRow { get; set; }
        public bool FixedColumn { get; set; }
    }
}
