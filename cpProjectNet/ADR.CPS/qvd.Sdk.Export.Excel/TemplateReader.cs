﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using qvd.Sdk.Export.Excel.Models;
using qvd.Sdk.Export.Excel.Utils;

namespace qvd.Sdk.Export.Excel
{
    public class TemplateReader : IDisposable
    {
        private bool disposedValue;

        /* 
Template format:
- Comment:multiple separate by comma
1. value: start value cells
2. merged: merge cells for column
3. title[<value-address>]: title cell for column at value-address. I.e: title[C3]
- All upper-left cells of value cell is fixed headers
- All upper and upper-right cells is column headers
- All left and lower-left cells row of value is rows definitions
- Cells start from value (comment) to lower-right is value & summaries
- Cell values is mark as {<data.field>} for data reference.        
- For summaries cell with formula, use startRow, endRow, startColumn, endColumn for reference     
*/

        public Stylesheet StyleTemplate { get; private set; }

        public List<SheetTemplate> ParseTemplate(string excelTemplateFile)
        {
            using var templateDoc = SpreadsheetDocument.Open(excelTemplateFile, false);
            var wbp = templateDoc.WorkbookPart;
            StyleTemplate = wbp.WorkbookStylesPart.Stylesheet.Clone() as Stylesheet;
            IEnumerable<SharedStringItem> sharedStringItems = wbp.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>();
            var result = new List<SheetTemplate>();
            foreach (WorksheetPart worksheetPart in wbp.WorksheetParts)
            {
                var matrix = new MatrixTemplate
                {
                    FixedHeaders = new List<CellTemplate>(),
                    Rows = new RowTemplate { Cells = new List<CellTemplate>() },
                    Columns = new List<ColumnTemplate>()
                };

                var annotations = ReadTemplateAnotation(worksheetPart);
                // read sheet cell data to build Matrix
                var sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                var titles = new Dictionary<string, List<CellTemplate>>();
                var columnAddressMap = new Dictionary<string, string>();
                foreach (var row in sheetData.Elements<Row>())
                {
                    foreach (var cell in row.Elements<Cell>())
                    {
                        string address = cell.CellReference;
                        var (start, end) = AddressUtils.ParseExcelAddress(address);
                        string startAddress = start.ToExcellAddress();
                        var rIndex = AddressUtils.GetRow(address);
                        var cIndex = AddressUtils.GetColumn(address);
                        var dataType = CellDataTypes.Text;
                        if (annotations.TypeMapping.ContainsKey(address.ToLower()))
                        {
                            dataType = annotations.TypeMapping[address.ToLower()];
                        }
                        var value = GetCellValue(sharedStringItems, cell)?.Trim();
                        var isFormula = annotations.Formulas.ContainsKey(address.ToLower());
                        var formula = string.Empty;
                        if (isFormula){
                            formula = annotations.Formulas[address.ToLower()];
                        }
                        var cellTemplate = new CellTemplate
                        {
                            DataType = dataType,
                            IsFormula = isFormula,
                            Merged = annotations.Merged.Contains(startAddress),
                            StyleId = cell.StyleIndex,
                            Value = value,
                            Formula = formula
                        };
                        if (rIndex > annotations.StartRowValue || cIndex > annotations.StartColumnValue) break; // only support single value row & single column value definition
                        switch (rIndex >= annotations.StartRowValue, cIndex >= annotations.StartColumnValue)
                        {
                            case (false, false): // Fixed headers
                                cellTemplate.Address = AddressUtils.ConvertToTemplateAddress(address);
                                matrix.FixedHeaders.Add(cellTemplate);
                                break;
                            case (true, false): // rows
                                cellTemplate.Address = BuildRowAddress(start);
                                matrix.Rows.Cells.Add(cellTemplate);
                                break;
                            case (false, true): // columns
                                var isTitle = annotations.TitleMaps.ContainsKey(address.ToLower());
                                cellTemplate.Address = BuildColumnAddress(start); // not support summaries
                                if (isTitle)
                                {
                                    var valueAddress = annotations.TitleMaps[address.ToLower()];
                                    if (!titles.ContainsKey(valueAddress))
                                        titles[valueAddress] = new List<CellTemplate>();
                                    titles[valueAddress].Add(cellTemplate);
                                }
                                else
                                {
                                    var columnTemplate = new ColumnTemplate { Titles = new List<CellTemplate>() };
                                    columnAddressMap[cellTemplate.Address] = address;
                                    columnTemplate.Value = cellTemplate;
                                    matrix.Columns.Add(columnTemplate);
                                }
                                break;
                            default: // summaries & value
                                // Not support summaries
                                cellTemplate.Address = BuildValueAddress(address);
                                matrix.Value = cellTemplate;
                                break;
                        }
                    }
                }

                // Map title to value columns     
                foreach (var item in matrix.Columns)
                {
                    var valueAddr = item.Value.Address;
                    if (columnAddressMap.ContainsKey(valueAddr))
                    {
                        var orgAddr = columnAddressMap[valueAddr];
                        if (titles.ContainsKey(orgAddr))
                        {
                            item.Titles.AddRange(titles[orgAddr]);
                        }
                    }
                }

                var partRelationshipId = wbp.GetIdOfPart(worksheetPart);
                result.Add(new SheetTemplate
                {
                    Matrix = matrix,
                    SheetName = wbp.Workbook.Sheets.Cast<Sheet>()
                        .FirstOrDefault(sheet => sheet.Id.HasValue && sheet.Id.Value == partRelationshipId)?.Name
                });
            }
            result.Reverse();
            return result;
        }

        protected string BuildRowAddress(CellIndex address)
        {
            var columnPrefix = address.FixedColumn ? "$" : "";
            var rowPrefix = address.FixedRow ? "$" : "";
            return $"{rowPrefix}{{{address.Row - 1} + rowIndex}},{columnPrefix}{address.Column}";
        }

        protected string BuildColumnAddress(CellIndex address)
        {
            var columnPrefix = address.FixedColumn ? "$" : "";
            var rowPrefix = address.FixedRow ? "$" : "";
            return $"{rowPrefix}{address.Row},{columnPrefix}{{{address.Column} + startColumn}}:{rowPrefix}{address.Row},{columnPrefix}{{{address.Column} + endColumn}}";
        }

        protected string BuildValueAddress(string address)
        {
            var (start, _) = AddressUtils.ParseExcelAddress(address);
            var columnPrefix = start.FixedColumn ? "$" : "";
            var rowPrefix = start.FixedRow ? "$" : "";
            return $"{rowPrefix}{{{start.Row} + rowIndex - 1}},{columnPrefix}{{column}}";
        }

        protected string GetCellValue(IEnumerable<SharedStringItem> sharedStringItems, Cell cell)
        {
            string cellValue = cell?.InnerText;
            if (cell.DataType != null && cell.DataType == CellValues.SharedString)
            {

                SharedStringItem ssi = sharedStringItems.ElementAt(int.Parse(cellValue));
                cellValue = ssi?.InnerText;
            }
            return cellValue;
        }

        protected TemplateAnotation ReadTemplateAnotation(WorksheetPart sheet)
        {
            var anotation = new TemplateAnotation
            {
                Merged = new List<string>(),
                TitleMaps = new Dictionary<string, string>(),
                TypeMapping = new Dictionary<string, CellDataTypes>(),
                Formulas = new Dictionary<string, string>()
            };
            foreach (WorksheetCommentsPart commentsPart in sheet.GetPartsOfType<WorksheetCommentsPart>())
            {
                foreach (Comment comment in commentsPart.Comments.CommentList)
                {
                    string address = comment.Reference;
                    string[] values = comment.InnerText?.Split(';');
                    if (values != null && values.Length > 0)
                    {
                        foreach (var text in values)
                        {
                            switch (text?.ToLower()?.Trim())
                            {
                                case "value":
                                    anotation.StartColumnValue = (uint)AddressUtils.GetColumn(address);
                                    anotation.StartRowValue = AddressUtils.GetRow(address);
                                    break;
                                case "merged":
                                    if (!anotation.Merged.Contains(address))
                                        anotation.Merged.Add(address);
                                    break;
                                default:
                                    if (text?.ToLower()?.StartsWith("title") == true)
                                    {
                                        var valueColumn = text.Substring(6, text.Length - 7);
                                        anotation.TitleMaps[address.ToLower()] = valueColumn;
                                    }
                                    else if (text?.ToLower()?.StartsWith("type=") == true)
                                    {
                                        var type = text.Substring(5);
                                        anotation.TypeMapping[address.ToLower()] = MapCellDataType(type);
                                    }
                                    else if (text?.ToLower()?.StartsWith("formula=") == true)
                                    {
                                        var formula = text.Substring(8);
                                        anotation.Formulas[address.ToLower()] = formula;
                                    }
                                    break;
                            }
                        }
                    }

                }
            }
            return anotation;
        }

        protected CellDataTypes MapCellDataType(string type) => type?.ToLower() switch
        {
            "numeric" => CellDataTypes.Numeric,
            "date" => CellDataTypes.DateTime,
            "time" => CellDataTypes.DateTime,
            "datetime" => CellDataTypes.DateTime,
            "bool" => CellDataTypes.Boolean,
            _ => CellDataTypes.Text
        };

        protected class TemplateAnotation
        {
            public uint StartRowValue { get; set; } = uint.MaxValue;
            public uint StartColumnValue { get; set; } = uint.MaxValue;
            public List<string> Merged { get; set; }
            public Dictionary<string, string> TitleMaps { get; set; }
            public Dictionary<string, CellDataTypes> TypeMapping { get; set; }
            public Dictionary<string, string> Formulas { get; set; }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~TemplateReader()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}