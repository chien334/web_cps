using ADR.CPS.Data.Models;
using ADR.CPS.Data.Models.Queries;
using ADR.CPS.Data.Services;
using ADR.CPS.SDK.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ADR.CPS.Data.Utils;
namespace ADR.CPS.Web.Controllers
{
	[ApiController]
	[Route("api/[controller]/[action]")]
	public partial class ProductController : CPSController
	{
		private readonly ILogger<ProductController> _logger;
		private readonly IQueryService _queryService;
		private readonly IService _service;
		private readonly IServiceProvider _serviceProvider;
		private readonly IValidator<Product> _validator;
		public ProductController(ILogger<ProductController> logger,
				IQueryService essQueryService,
				IService essService,
				IServiceProvider serviceProvider)
		{
			_logger = logger;
			this._queryService = essQueryService;
			this._service = essService;
			this._serviceProvider = serviceProvider;
			this._validator = serviceProvider.GetService<IValidator<Product>>();
		}
		[HttpPost]
		public async Task<JsonResult> Search(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default)
		{
			var value = await _queryService.Search(query, cancellationToken);
			return new JsonResult(new
			{
				Items = value.ToArray(),
				TotalCount = value.TotalCount
			});
		}
		[HttpPost]
		public async Task<JsonResult> Update(UpdateModel<Product> update, CancellationToken cancellationToken = default)
		{
			SetUpdateAttributes(update.Entity);
			var validationResults = new List<ValidationResult>();
			if (this._validator != null)
			{
				validationResults = await this._validator.ValidateAsync(update.Entity, OperationTypes.Update, this.UserName);
			}
			if (validationResults.HasError())
				return new JsonResult(new { Success = false, Entity = update.Entity, Message = validationResults.JoinToString() });
			await _service.UpdateAsync(update.Entity, cancellationToken);
			var count = await _service.SaveAsync(cancellationToken);
			return new JsonResult(new { Success = count > 0, Entity = update.Entity });
		}
		[HttpPost]
		public async Task<JsonResult> Create(CreateModel<Product> update, CancellationToken cancellationToken = default)
		{
			SetAddNewAttributes(update.Entity);
			var validationResults = new List<ValidationResult>();
			if (this._validator != null)
			{
				validationResults = await this._validator.ValidateAsync(update.Entity, OperationTypes.Create, this.UserName);
			}
			if (validationResults.HasError())
				return new JsonResult(new { Success = false, Entity = update.Entity, Message = validationResults.JoinToString() });
			await _service.AddAsync(update.Entity, cancellationToken);
			var count = await _service.SaveAsync(cancellationToken);
			return new JsonResult(new { Success = count > 0, Entity = update.Entity });
		}
		[HttpPost]
		public async Task<JsonResult> Delete(CreateModel<Product> update, CancellationToken cancellationToken = default)
		{
			var validationResults = new List<ValidationResult>();
			if (this._validator != null)
			{
				validationResults = await this._validator.ValidateAsync(update.Entity, OperationTypes.Delete, this.UserName);
			}
			if (validationResults.HasError())
				return new JsonResult(new { Success = false, Entity = update.Entity, Message = validationResults.JoinToString() });
			await _service.DeleteAsync(update.Entity, cancellationToken);
			var count = await _service.SaveAsync(cancellationToken);
			return new JsonResult(new { Success = count > 0, Entity = update.Entity });
		}
		[HttpPost]
		public async Task<JsonResult> MarkDelete(CreateModel<Product> update, CancellationToken cancellationToken = default)
		{
			SetUpdateAttributes(update.Entity);
			var validationResults = new List<ValidationResult>();
			if (this._validator != null)
			{
				validationResults.AddRange(await this._validator.ValidateAsync(update.Entity, OperationTypes.Update, this.UserName));
			}
			if (validationResults.HasError())
				return new JsonResult(new { Success = false, Entity = update.Entity, Message = validationResults.JoinToString() });
			update.Entity.Deleted = true;
			await _service.UpdateAsync(update.Entity, cancellationToken);
			var count = await _service.SaveAsync(cancellationToken);
			return new JsonResult(new { Success = count > 0, Entity = update.Entity });
		}
	}
}
