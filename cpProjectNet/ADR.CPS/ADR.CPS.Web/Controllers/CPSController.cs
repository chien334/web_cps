﻿using ADR.CPS.SDK.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ADR.CPS.Web.Controllers
{
    [ApiController]
    public partial class CPSController : ControllerBase
    {
        protected string UserName => this.User?.Identity?.Name;
        protected Microsoft.Net.Http.Headers.MediaTypeHeaderValue XlsxMediaType
        => new Microsoft.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        protected void SetUpdateAttributes(BaseEntity entity)
        {
            if (this.User != null)
            {
                entity.LastUpdatedBy = this.User.Identity?.Name;
            }
            else
            {
                entity.LastUpdatedBy = string.Empty;
            }
            entity.LastUpdatedTime = DateTime.Now;
        }

        protected void SetAddNewAttributes(BaseEntity entity)
        {
            SetUpdateAttributes(entity);
            if (this.User != null)
            {
                entity.CreatedBy = this.User.Identity?.Name;
            }
            else
            {
                entity.CreatedBy = string.Empty;
            }
            entity.CreatedTime = DateTime.Now;
        }
    }
}
