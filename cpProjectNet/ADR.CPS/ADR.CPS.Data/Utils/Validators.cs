﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace ADR.CPS.Data.Utils
{
    public static class Validators
    {
        public const string DEFAULT_DATEFORMAT = "dd/MM/yyyy";

        public static ValidationResult Required(string name, object value)
        {
            if (value == null
            || (value is string && (string)value == "")
            || (value is IEnumerable && !(value as IEnumerable).GetEnumerator().MoveNext()))
                return new ValidationResult($"{name} is required", new string[] { name });
            return null;
        }

        public static ValidationResult Min(string name, decimal? value, decimal min)
        {
            if (value < min)
            {
                return new ValidationResult($"{name} must greater than {min}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Max(string name, decimal? value, decimal max)
        {
            if (value > max)
            {
                return new ValidationResult($"{name} must less than {max}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Range(string name, decimal? value, int min, int max)
        {
            if (value < min || value > max)
            {
                return new ValidationResult($"{name} must in range {min} - {max}", new string[] { name });
            }
            return null;
        }
        public static ValidationResult HasSpecialChars(string name, string value)
        {
            if (value == null)
                return null;
            string specialChar = @"!@#$%^*`";
            foreach (var item in specialChar)
            {
                if (value.Contains(item)) return new ValidationResult($"{name} cannot contain special character ", new string[] { name });
            }
            return null;
        }
        public static ValidationResult CharactersAreAllowed(string name, string value)
        {
            if (value == null)
                return null;
            var regexItem = new Regex("^[a-zA-Z0-9-_]*$");
            if (regexItem.IsMatch(value))
            {
                return null;
            }
            else
            {
                return new ValidationResult($"{name} cannot contain special character ");
            }

        }
        public static ValidationResult DomainExists(string name, string input)
        {
            var pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            if (Regex.IsMatch(input, pattern))
            {
                return null;
            }
            else
            {
                return new ValidationResult($"{input} is Not a Valid Email address");
            }
        }
        public static ValidationResult Min(string name, int? value, int min)
        {
            if (value < min)
            {
                return new ValidationResult($"{name} must greater than {min}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Max(string name, int? value, int max)
        {
            if (value > max)
            {
                return new ValidationResult($"{name} must less than {max}", new string[] { name });
            }
            return null;
        }
        public static ValidationResult Range(string name, int? value, int min, int max)
        {
            if (value < min || value > max)
            {
                return new ValidationResult($"{name} must in range {min} - {max}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Min(string name, double? value, double min)
        {
            if (value < min)
            {
                return new ValidationResult($"{name} must greater than {min}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Max(string name, double? value, double max)
        {
            if (value > max)
            {
                return new ValidationResult($"{name} must less than {max}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Range(string name, double? value, double min, double max)
        {
            if (value < min || value > max)
            {
                return new ValidationResult($"{name} must in range {min} - {max}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Min(string name, DateTime? value, DateTime min, string dateformat = DEFAULT_DATEFORMAT)
        {
            if (value < min)
            {
                return new ValidationResult($"{name} must greater than or equal {min.ToString(dateformat)}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult MinOrEqual(string name, DateTime? value, DateTime min, string dateformat = DEFAULT_DATEFORMAT)
        {
            if (value <= min)
            {
                return new ValidationResult($"{name} must greater than {min.ToString(dateformat)}", new string[] { name });
            }
            return null;
        }
        public static ValidationResult Max(string name, DateTime? value, DateTime max, string dateformat = DEFAULT_DATEFORMAT)
        {
            if (value > max)
            {
                return new ValidationResult($"{name} must less than {max.ToString(dateformat)}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Range(string name, DateTime? value, DateTime min, DateTime max, string dateformat = DEFAULT_DATEFORMAT)
        {
            if (value < min || value > max)
            {
                return new ValidationResult($"{name} must in range {min.ToString(dateformat)} - {max.ToString(dateformat)}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult MinLength(string name, string value, int min)
        {
            if ((value?.Length ?? 0) < min)
            {
                return new ValidationResult($"{name} length must longer than {min}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult MaxLength(string name, string value, int max)
        {
            if ((value?.Length ?? 0) > max)
            {
                return new ValidationResult($"{name} length must shorter than {max}", new string[] { name });
            }
            return null;
        }

        public static ValidationResult Length(string name, string value, int min, int max)
        {
            var length = (value?.Length ?? 0);
            if (length < min || length > max)
            {
                return new ValidationResult($"{name} length must in range {min} - {max}", new string[] { name });
            }
            return null;
        }
    }
}
