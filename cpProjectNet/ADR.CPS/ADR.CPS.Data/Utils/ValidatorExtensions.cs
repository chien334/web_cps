﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ADR.CPS.Data.Utils
{
    public static class ValidatorExtensions
    {
        public static List<ValidationResult> AddErrors(this List<ValidationResult> source, IEnumerable<ValidationResult> validationResults)
        {
            if (validationResults != null)
                source.AddRange(validationResults.Where(v => v != null && v != ValidationResult.Success));
            return source;
        }
        public static List<ValidationResult> AddError(this List<ValidationResult> source, ValidationResult validationResult)
        {
            if (validationResult != null && validationResult != ValidationResult.Success)
                source.Add(validationResult);
            return source;
        }

        public static bool IsError(this ValidationResult validationResult) => validationResult != null && validationResult != ValidationResult.Success;

        public static bool HasError(this List<ValidationResult> validationResults)
        => validationResults != null && validationResults.Any(x => x.IsError());

        public static string JoinToString(this List<ValidationResult> validationResults, string separator = ";")
        => string.Join(separator, validationResults.Where(x => x != null).Select(x => x.ErrorMessage.ToString()));
    }
}
