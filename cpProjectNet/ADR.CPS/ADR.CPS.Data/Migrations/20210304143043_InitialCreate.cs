﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ADR.CPS.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupTable",
                columns: table => new
                {
                    GroupId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    GroupTbcode = table.Column<string>(maxLength: 20, nullable: true),
                    GroupCode = table.Column<string>(maxLength: 20, nullable: true),
                    GroupName = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupTable", x => x.GroupId);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    ImgId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ImgProperties = table.Column<string>(nullable: true),
                    ImgAbsolutePath = table.Column<string>(maxLength: 500, nullable: true),
                    ImgRelativePath = table.Column<string>(maxLength: 500, nullable: true),
                    ImageType = table.Column<string>(maxLength: 20, nullable: true),
                    ImgIdReferent = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.ImgId);
                });

            migrationBuilder.CreateTable(
                name: "ProductType",
                columns: table => new
                {
                    PtId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    PtName = table.Column<string>(nullable: true),
                    PtDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductType", x => x.PtId);
                });

            migrationBuilder.CreateTable(
                name: "TradeMark",
                columns: table => new
                {
                    TmId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    TmImage = table.Column<string>(maxLength: 500, nullable: true),
                    TmStatus = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeMark", x => x.TmId);
                });

            migrationBuilder.CreateTable(
                name: "AdminAccount",
                columns: table => new
                {
                    AdmId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    AdmUserName = table.Column<string>(maxLength: 30, nullable: true),
                    AdmPassword = table.Column<string>(maxLength: 30, nullable: true),
                    AdmFirstName = table.Column<string>(maxLength: 30, nullable: true),
                    AdmLastName = table.Column<string>(maxLength: 30, nullable: true),
                    AdmIdentityCard = table.Column<string>(maxLength: 20, nullable: true),
                    AdmDob = table.Column<DateTime>(nullable: true),
                    AdmPhone = table.Column<string>(maxLength: 12, nullable: true),
                    AdmEmail = table.Column<string>(maxLength: 100, nullable: true),
                    RoleTypeId = table.Column<long>(nullable: false),
                    GroupTableGroupId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdminAccount", x => x.AdmId);
                    table.ForeignKey(
                        name: "FK_AdminAccount_GroupTable_GroupTableGroupId",
                        column: x => x.GroupTableGroupId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    EmpId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    EmpFirstName = table.Column<string>(maxLength: 30, nullable: true),
                    EmpLastName = table.Column<string>(maxLength: 30, nullable: true),
                    EmpEmail = table.Column<string>(maxLength: 100, nullable: true),
                    EmpPhone = table.Column<string>(maxLength: 12, nullable: true),
                    EmpDescription = table.Column<string>(nullable: true),
                    GroupDepartmentId = table.Column<long>(nullable: false),
                    GroupTableGroupId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.EmpId);
                    table.ForeignKey(
                        name: "FK_Employee_GroupTable_GroupTableGroupId",
                        column: x => x.GroupTableGroupId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Menu",
                columns: table => new
                {
                    MenuId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    MenuTypeId = table.Column<long>(nullable: false),
                    MctId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Menu", x => x.MenuId);
                    table.ForeignKey(
                        name: "FK_Menu_GroupTable_MenuId",
                        column: x => x.MenuId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId");
                });

            migrationBuilder.CreateTable(
                name: "News",
                columns: table => new
                {
                    NewsId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    NewTypeId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.NewsId);
                    table.ForeignKey(
                        name: "FK_News_GroupTable_NewsId",
                        column: x => x.NewsId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId");
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    RoleId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    RoleCode = table.Column<string>(maxLength: 20, nullable: true),
                    RoleName = table.Column<string>(maxLength: 80, nullable: true),
                    RoleTypeId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.RoleId);
                    table.ForeignKey(
                        name: "FK_Role_GroupTable_RoleId",
                        column: x => x.RoleId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId");
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    PId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    PtId = table.Column<long>(nullable: false),
                    TmId = table.Column<long>(nullable: false),
                    PProperties = table.Column<string>(nullable: true),
                    PStatus = table.Column<bool>(nullable: true),
                    PDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.PId);
                    table.ForeignKey(
                        name: "FK_Product_ProductType_PId",
                        column: x => x.PId,
                        principalTable: "ProductType",
                        principalColumn: "PtId");
                    table.ForeignKey(
                        name: "FK_Product_TradeMark_PId",
                        column: x => x.PId,
                        principalTable: "TradeMark",
                        principalColumn: "TmId");
                });

            migrationBuilder.CreateTable(
                name: "MenuContent",
                columns: table => new
                {
                    MctId = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    MenuId = table.Column<long>(nullable: false),
                    MctLanguageId = table.Column<long>(nullable: false),
                    MctTitle = table.Column<string>(maxLength: 256, nullable: true),
                    MctDescription = table.Column<string>(maxLength: 1000, nullable: true),
                    GroupTableGroupId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuContent", x => x.MctId);
                    table.ForeignKey(
                        name: "FK_MenuContent_GroupTable_GroupTableGroupId",
                        column: x => x.GroupTableGroupId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MenuContent_Menu_MenuId",
                        column: x => x.MenuId,
                        principalTable: "Menu",
                        principalColumn: "MenuId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NewsContent",
                columns: table => new
                {
                    NcId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    CreatedTime = table.Column<DateTime>(nullable: true),
                    LastUpdatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    LastUpdatedTime = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    NcNewsId = table.Column<long>(nullable: false),
                    NcLanguageId = table.Column<long>(nullable: false),
                    NcTitle = table.Column<string>(maxLength: 256, nullable: true),
                    NcDescription = table.Column<string>(maxLength: 1000, nullable: true),
                    NcContent = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsContent", x => x.NcId);
                    table.ForeignKey(
                        name: "FK_NewsContent_GroupTable_NcId",
                        column: x => x.NcId,
                        principalTable: "GroupTable",
                        principalColumn: "GroupId");
                    table.ForeignKey(
                        name: "FK_NewsContent_News_NcId",
                        column: x => x.NcId,
                        principalTable: "News",
                        principalColumn: "NewsId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdminAccount_GroupTableGroupId",
                table: "AdminAccount",
                column: "GroupTableGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_GroupTableGroupId",
                table: "Employee",
                column: "GroupTableGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuContent_GroupTableGroupId",
                table: "MenuContent",
                column: "GroupTableGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_MenuContent_MenuId",
                table: "MenuContent",
                column: "MenuId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdminAccount");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropTable(
                name: "MenuContent");

            migrationBuilder.DropTable(
                name: "NewsContent");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "Menu");

            migrationBuilder.DropTable(
                name: "News");

            migrationBuilder.DropTable(
                name: "ProductType");

            migrationBuilder.DropTable(
                name: "TradeMark");

            migrationBuilder.DropTable(
                name: "GroupTable");
        }
    }
}
