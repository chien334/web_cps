﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ADR.CPS.Data.Migrations
{
    public partial class UpdateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_GroupTable_GroupTableGroupId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuContent_GroupTable_GroupTableGroupId",
                table: "MenuContent");

            migrationBuilder.DropIndex(
                name: "IX_MenuContent_GroupTableGroupId",
                table: "MenuContent");

            migrationBuilder.DropIndex(
                name: "IX_Employee_GroupTableGroupId",
                table: "Employee");

            migrationBuilder.DropColumn(
                name: "GroupTableGroupId",
                table: "MenuContent");

            migrationBuilder.DropColumn(
                name: "ImageType",
                table: "Image");

            migrationBuilder.DropColumn(
                name: "GroupTableGroupId",
                table: "Employee");

            migrationBuilder.AlterColumn<long>(
                name: "MctId",
                table: "MenuContent",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AlterColumn<long>(
                name: "ImgId",
                table: "Image",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<long>(
                name: "ImageTypeId",
                table: "Image",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<long>(
                name: "EmpId",
                table: "Employee",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_GroupTable_EmpId",
                table: "Employee",
                column: "EmpId",
                principalTable: "GroupTable",
                principalColumn: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Image_GroupTable_ImgId",
                table: "Image",
                column: "ImgId",
                principalTable: "GroupTable",
                principalColumn: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuContent_GroupTable_MctId",
                table: "MenuContent",
                column: "MctId",
                principalTable: "GroupTable",
                principalColumn: "GroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee_GroupTable_EmpId",
                table: "Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Image_GroupTable_ImgId",
                table: "Image");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuContent_GroupTable_MctId",
                table: "MenuContent");

            migrationBuilder.DropColumn(
                name: "ImageTypeId",
                table: "Image");

            migrationBuilder.AlterColumn<long>(
                name: "MctId",
                table: "MenuContent",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<long>(
                name: "GroupTableGroupId",
                table: "MenuContent",
                type: "bigint",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ImgId",
                table: "Image",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<string>(
                name: "ImageType",
                table: "Image",
                type: "character varying(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "EmpId",
                table: "Employee",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<long>(
                name: "GroupTableGroupId",
                table: "Employee",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MenuContent_GroupTableGroupId",
                table: "MenuContent",
                column: "GroupTableGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Employee_GroupTableGroupId",
                table: "Employee",
                column: "GroupTableGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employee_GroupTable_GroupTableGroupId",
                table: "Employee",
                column: "GroupTableGroupId",
                principalTable: "GroupTable",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuContent_GroupTable_GroupTableGroupId",
                table: "MenuContent",
                column: "GroupTableGroupId",
                principalTable: "GroupTable",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
