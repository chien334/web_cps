using ADR.CPS.Data.Models;
using ADR.CPS.Data.Responsitories;
using ADR.CPS.Data.Services;
using ADR.CPS.SDK.Data.EFService;
using ADR.CPS.SDK.EFService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ADR.CPS.Data.Services.Services
{
	public partial class Service : EfService<CPSContext>, IService
	{
		public Service(IServiceProvider serviceProvider, IBulkOperation bulkOperation) : base(serviceProvider, bulkOperation)
		{
		}
		public Task<bool> ExistsAsync(Image entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Image>().AnyAsync(e => e.ImgId == entity.ImgId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(Image entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Image>().FirstOrDefaultAsync(e => e.ImgId == entity.ImgId);
			if (existed == null)
			{
				dbContext.Set<Image>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(Image entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Image>().FirstOrDefaultAsync(e => e.ImgId == entity.ImgId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<Image> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<Image, object>> keySelector = (e => new {e.ImgId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(AdminAccount entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<AdminAccount>().AnyAsync(e => e.AdmId == entity.AdmId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(AdminAccount entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<AdminAccount>().FirstOrDefaultAsync(e => e.AdmId == entity.AdmId);
			if (existed == null)
			{
				dbContext.Set<AdminAccount>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(AdminAccount entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<AdminAccount>().FirstOrDefaultAsync(e => e.AdmId == entity.AdmId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<AdminAccount> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<AdminAccount, object>> keySelector = (e => new {e.AdmId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(Role entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Role>().AnyAsync(e => e.RoleId == entity.RoleId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(Role entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Role>().FirstOrDefaultAsync(e => e.RoleId == entity.RoleId);
			if (existed == null)
			{
				dbContext.Set<Role>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(Role entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Role>().FirstOrDefaultAsync(e => e.RoleId == entity.RoleId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<Role> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<Role, object>> keySelector = (e => new {e.RoleId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(Employee entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Employee>().AnyAsync(e => e.EmpId == entity.EmpId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(Employee entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Employee>().FirstOrDefaultAsync(e => e.EmpId == entity.EmpId);
			if (existed == null)
			{
				dbContext.Set<Employee>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(Employee entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Employee>().FirstOrDefaultAsync(e => e.EmpId == entity.EmpId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<Employee> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<Employee, object>> keySelector = (e => new {e.EmpId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(MenuContent entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<MenuContent>().AnyAsync(e => e.MctId == entity.MctId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(MenuContent entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<MenuContent>().FirstOrDefaultAsync(e => e.MctId == entity.MctId);
			if (existed == null)
			{
				dbContext.Set<MenuContent>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(MenuContent entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<MenuContent>().FirstOrDefaultAsync(e => e.MctId == entity.MctId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<MenuContent> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<MenuContent, object>> keySelector = (e => new {e.MctId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(Menu entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Menu>().AnyAsync(e => e.MenuId == entity.MenuId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(Menu entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Menu>().FirstOrDefaultAsync(e => e.MenuId == entity.MenuId);
			if (existed == null)
			{
				dbContext.Set<Menu>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(Menu entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Menu>().FirstOrDefaultAsync(e => e.MenuId == entity.MenuId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<Menu> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<Menu, object>> keySelector = (e => new {e.MenuId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(GroupTable entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<GroupTable>().AnyAsync(e => e.GroupId == entity.GroupId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(GroupTable entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<GroupTable>().FirstOrDefaultAsync(e => e.GroupId == entity.GroupId);
			if (existed == null)
			{
				dbContext.Set<GroupTable>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(GroupTable entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<GroupTable>().FirstOrDefaultAsync(e => e.GroupId == entity.GroupId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<GroupTable> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<GroupTable, object>> keySelector = (e => new {e.GroupId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(News entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<News>().AnyAsync(e => e.NewsId == entity.NewsId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(News entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<News>().FirstOrDefaultAsync(e => e.NewsId == entity.NewsId);
			if (existed == null)
			{
				dbContext.Set<News>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(News entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<News>().FirstOrDefaultAsync(e => e.NewsId == entity.NewsId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<News> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<News, object>> keySelector = (e => new {e.NewsId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(NewsContent entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<NewsContent>().AnyAsync(e => e.NcId == entity.NcId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(NewsContent entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<NewsContent>().FirstOrDefaultAsync(e => e.NcId == entity.NcId);
			if (existed == null)
			{
				dbContext.Set<NewsContent>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(NewsContent entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<NewsContent>().FirstOrDefaultAsync(e => e.NcId == entity.NcId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<NewsContent> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<NewsContent, object>> keySelector = (e => new {e.NcId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(TradeMark entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<TradeMark>().AnyAsync(e => e.TmId == entity.TmId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(TradeMark entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<TradeMark>().FirstOrDefaultAsync(e => e.TmId == entity.TmId);
			if (existed == null)
			{
				dbContext.Set<TradeMark>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(TradeMark entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<TradeMark>().FirstOrDefaultAsync(e => e.TmId == entity.TmId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<TradeMark> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<TradeMark, object>> keySelector = (e => new {e.TmId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(Product entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Product>().AnyAsync(e => e.PId == entity.PId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(Product entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Product>().FirstOrDefaultAsync(e => e.PId == entity.PId);
			if (existed == null)
			{
				dbContext.Set<Product>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(Product entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<Product>().FirstOrDefaultAsync(e => e.PId == entity.PId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<Product> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<Product, object>> keySelector = (e => new {e.PId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}
		public Task<bool> ExistsAsync(ProductType entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<ProductType>().AnyAsync(e => e.PtId == entity.PtId, cancellationToken);
		}
		public async Task AddOrUpdateAsync(ProductType entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<ProductType>().FirstOrDefaultAsync(e => e.PtId == entity.PtId);
			if (existed == null)
			{
				dbContext.Set<ProductType>().Add(entity);
			}
			else
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public async Task UpdateAsync(ProductType entity, CancellationToken cancellationToken = default)
		{
			var existed = await dbContext.Set<ProductType>().FirstOrDefaultAsync(e => e.PtId == entity.PtId);
			if (existed != null)
			{
				dbContext.Entry(existed).CurrentValues.SetValues(entity);
			}
		}
		public Task AddOrUpdateAsync(IEnumerable<ProductType> entities, CancellationToken cancellationToken = default)
		{
			Expression<Func<ProductType, object>> keySelector = (e => new {e.PtId} );
			return AddOrUpdateAsync(entities, keySelector, cancellationToken);
		}

	}
}