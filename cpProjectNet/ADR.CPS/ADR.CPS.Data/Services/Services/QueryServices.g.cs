using ADR.CPS.Data.Models;
using ADR.CPS.Data.Models.Queries;
using ADR.CPS.Data.Models.Views;
using ADR.CPS.Data.Responsitories;
using ADR.CPS.SDK.Data.EFService;
using ADR.CPS.SDK.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ADR.CPS.Data.Services.Services
{
	public partial class QueryService : EfQueryService<CPSContext>, IQueryService
	{
		public QueryService(IServiceProvider serviceProvider) : base(serviceProvider)
		{
		}
		public Task<bool> ExistsAsync(Image entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Image>().AsNoTracking().AnyAsync(e => e.ImgId == entity.ImgId, cancellationToken);
		}
		protected IQueryable<ImageView> Fillter(QueryModel<ImageQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<Image>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.ImgId == default || e.ImgId == query.Entity.ImgId)
					&&(query.Entity.ImgProperties == default || e.ImgProperties.Contains(query.Entity.ImgProperties))
					&&(query.Entity.ImgAbsolutePath == default || e.ImgAbsolutePath.Contains(query.Entity.ImgAbsolutePath))
					&&(query.Entity.ImgRelativePath == default || e.ImgRelativePath.Contains(query.Entity.ImgRelativePath))
					&&(query.Entity.ImageTypeId == default || e.ImageTypeId == query.Entity.ImageTypeId)
					&&(query.Entity.FromImgIdReferent == default || e.ImgIdReferent >= query.Entity.FromImgIdReferent)
					&&(query.Entity.ToImgIdReferent == default || e.ImgIdReferent <= query.Entity.ToImgIdReferent)

				)
				.Select(x => new ImageView(x));
		}
		public async Task<IPagedList<ImageView>> Search(QueryModel<ImageQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<ImageView>> SearchNoPaging(QueryModel<ImageQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(AdminAccount entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<AdminAccount>().AsNoTracking().AnyAsync(e => e.AdmId == entity.AdmId, cancellationToken);
		}
		protected IQueryable<AdminAccountView> Fillter(QueryModel<AdminAccountQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<AdminAccount>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.AdmId == default || e.AdmId == query.Entity.AdmId)
					&&(query.Entity.AdmUserName == default || e.AdmUserName.Contains(query.Entity.AdmUserName))
					&&(query.Entity.AdmPassword == default || e.AdmPassword.Contains(query.Entity.AdmPassword))
					&&(query.Entity.AdmFirstName == default || e.AdmFirstName.Contains(query.Entity.AdmFirstName))
					&&(query.Entity.AdmLastName == default || e.AdmLastName.Contains(query.Entity.AdmLastName))
					&&(query.Entity.AdmIdentityCard == default || e.AdmIdentityCard.Contains(query.Entity.AdmIdentityCard))
					&&(query.Entity.FromAdmDob == default || e.AdmDob >= query.Entity.FromAdmDob)
					&&(query.Entity.ToAdmDob == default || e.AdmDob <= query.Entity.ToAdmDob)
					&&(query.Entity.AdmPhone == default || e.AdmPhone.Contains(query.Entity.AdmPhone))
					&&(query.Entity.AdmEmail == default || e.AdmEmail.Contains(query.Entity.AdmEmail))
					&&(query.Entity.RoleTypeId == default || e.RoleTypeId == query.Entity.RoleTypeId)

				)
				.Select(x => new AdminAccountView(x));
		}
		public async Task<IPagedList<AdminAccountView>> Search(QueryModel<AdminAccountQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<AdminAccountView>> SearchNoPaging(QueryModel<AdminAccountQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(Role entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Role>().AsNoTracking().AnyAsync(e => e.RoleId == entity.RoleId, cancellationToken);
		}
		protected IQueryable<RoleView> Fillter(QueryModel<RoleQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<Role>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.RoleId == default || e.RoleId == query.Entity.RoleId)
					&&(query.Entity.RoleCode == default || e.RoleCode.Contains(query.Entity.RoleCode))
					&&(query.Entity.RoleName == default || e.RoleName.Contains(query.Entity.RoleName))
					&&(query.Entity.RoleTypeId == default || e.RoleTypeId == query.Entity.RoleTypeId)

				)
				.Select(x => new RoleView(x));
		}
		public async Task<IPagedList<RoleView>> Search(QueryModel<RoleQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<RoleView>> SearchNoPaging(QueryModel<RoleQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(Employee entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Employee>().AsNoTracking().AnyAsync(e => e.EmpId == entity.EmpId, cancellationToken);
		}
		protected IQueryable<EmployeeView> Fillter(QueryModel<EmployeeQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<Employee>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.EmpId == default || e.EmpId == query.Entity.EmpId)
					&&(query.Entity.EmpFirstName == default || e.EmpFirstName.Contains(query.Entity.EmpFirstName))
					&&(query.Entity.EmpLastName == default || e.EmpLastName.Contains(query.Entity.EmpLastName))
					&&(query.Entity.EmpEmail == default || e.EmpEmail.Contains(query.Entity.EmpEmail))
					&&(query.Entity.EmpPhone == default || e.EmpPhone.Contains(query.Entity.EmpPhone))
					&&(query.Entity.EmpDescription == default || e.EmpDescription.Contains(query.Entity.EmpDescription))
					&&(query.Entity.GroupDepartmentId == default || e.GroupDepartmentId == query.Entity.GroupDepartmentId)

				)
				.Select(x => new EmployeeView(x));
		}
		public async Task<IPagedList<EmployeeView>> Search(QueryModel<EmployeeQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<EmployeeView>> SearchNoPaging(QueryModel<EmployeeQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(MenuContent entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<MenuContent>().AsNoTracking().AnyAsync(e => e.MctId == entity.MctId, cancellationToken);
		}
		protected IQueryable<MenuContentView> Fillter(QueryModel<MenuContentQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<MenuContent>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.MctId == default || e.MctId == query.Entity.MctId)
					&&(query.Entity.MenuId == default || e.MenuId == query.Entity.MenuId)
					&&(query.Entity.MctLanguageId == default || e.MctLanguageId == query.Entity.MctLanguageId)
					&&(query.Entity.MctTitle == default || e.MctTitle.Contains(query.Entity.MctTitle))
					&&(query.Entity.MctDescription == default || e.MctDescription.Contains(query.Entity.MctDescription))

				)
				.Select(x => new MenuContentView(x));
		}
		public async Task<IPagedList<MenuContentView>> Search(QueryModel<MenuContentQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<MenuContentView>> SearchNoPaging(QueryModel<MenuContentQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(Menu entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Menu>().AsNoTracking().AnyAsync(e => e.MenuId == entity.MenuId, cancellationToken);
		}
		protected IQueryable<MenuView> Fillter(QueryModel<MenuQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<Menu>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.MenuId == default || e.MenuId == query.Entity.MenuId)
					&&(query.Entity.MenuTypeId == default || e.MenuTypeId == query.Entity.MenuTypeId)
					&&(query.Entity.MctId == default || e.MctId == query.Entity.MctId)

				)
				.Select(x => new MenuView(x));
		}
		public async Task<IPagedList<MenuView>> Search(QueryModel<MenuQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<MenuView>> SearchNoPaging(QueryModel<MenuQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(GroupTable entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<GroupTable>().AsNoTracking().AnyAsync(e => e.GroupId == entity.GroupId, cancellationToken);
		}
		protected IQueryable<GroupTableView> Fillter(QueryModel<GroupTableQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<GroupTable>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.GroupId == default || e.GroupId == query.Entity.GroupId)
					&&(query.Entity.GroupTbcode == default || e.GroupTbcode.Contains(query.Entity.GroupTbcode))
					&&(query.Entity.GroupCode == default || e.GroupCode.Contains(query.Entity.GroupCode))
					&&(query.Entity.GroupName == default || e.GroupName.Contains(query.Entity.GroupName))

				)
				.Select(x => new GroupTableView(x));
		}
		public async Task<IPagedList<GroupTableView>> Search(QueryModel<GroupTableQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<GroupTableView>> SearchNoPaging(QueryModel<GroupTableQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(News entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<News>().AsNoTracking().AnyAsync(e => e.NewsId == entity.NewsId, cancellationToken);
		}
		protected IQueryable<NewsView> Fillter(QueryModel<NewsQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<News>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.NewsId == default || e.NewsId == query.Entity.NewsId)
					&&(query.Entity.NewTypeId == default || e.NewTypeId == query.Entity.NewTypeId)

				)
				.Select(x => new NewsView(x));
		}
		public async Task<IPagedList<NewsView>> Search(QueryModel<NewsQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<NewsView>> SearchNoPaging(QueryModel<NewsQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(NewsContent entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<NewsContent>().AsNoTracking().AnyAsync(e => e.NcId == entity.NcId, cancellationToken);
		}
		protected IQueryable<NewsContentView> Fillter(QueryModel<NewsContentQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<NewsContent>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.NcId == default || e.NcId == query.Entity.NcId)
					&&(query.Entity.NcNewsId == default || e.NcNewsId == query.Entity.NcNewsId)
					&&(query.Entity.NcLanguageId == default || e.NcLanguageId == query.Entity.NcLanguageId)
					&&(query.Entity.NcTitle == default || e.NcTitle.Contains(query.Entity.NcTitle))
					&&(query.Entity.NcDescription == default || e.NcDescription.Contains(query.Entity.NcDescription))
					&&(query.Entity.NcContent == default || e.NcContent.Contains(query.Entity.NcContent))

				)
				.Select(x => new NewsContentView(x));
		}
		public async Task<IPagedList<NewsContentView>> Search(QueryModel<NewsContentQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<NewsContentView>> SearchNoPaging(QueryModel<NewsContentQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(TradeMark entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<TradeMark>().AsNoTracking().AnyAsync(e => e.TmId == entity.TmId, cancellationToken);
		}
		protected IQueryable<TradeMarkView> Fillter(QueryModel<TradeMarkQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<TradeMark>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.TmId == default || e.TmId == query.Entity.TmId)
					&&(query.Entity.TmImage == default || e.TmImage.Contains(query.Entity.TmImage))
					&&(query.Entity.TmStatus == default || e.TmStatus == query.Entity.TmStatus)

				)
				.Select(x => new TradeMarkView(x));
		}
		public async Task<IPagedList<TradeMarkView>> Search(QueryModel<TradeMarkQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<TradeMarkView>> SearchNoPaging(QueryModel<TradeMarkQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(Product entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<Product>().AsNoTracking().AnyAsync(e => e.PId == entity.PId, cancellationToken);
		}
		protected IQueryable<ProductView> Fillter(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<Product>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.PId == default || e.PId == query.Entity.PId)
					&&(query.Entity.PtId == default || e.PtId == query.Entity.PtId)
					&&(query.Entity.TmId == default || e.TmId == query.Entity.TmId)
					&&(query.Entity.PProperties == default || e.PProperties.Contains(query.Entity.PProperties))
					&&(query.Entity.PStatus == default || e.PStatus == query.Entity.PStatus)
					&&(query.Entity.FromPDate == default || e.PDate >= query.Entity.FromPDate)
					&&(query.Entity.ToPDate == default || e.PDate <= query.Entity.ToPDate)

				)
				.Select(x => new ProductView(x));
		}
		public async Task<IPagedList<ProductView>> Search(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<ProductView>> SearchNoPaging(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}
		public Task<bool> ExistsAsync(ProductType entity, CancellationToken cancellationToken = default)
		{
			return this.dbContext.Set<ProductType>().AsNoTracking().AnyAsync(e => e.PtId == entity.PtId, cancellationToken);
		}
		protected IQueryable<ProductTypeView> Fillter(QueryModel<ProductTypeQuery> query, CancellationToken cancellationToken = default)
		{
			return dbContext.Set<ProductType>()
				.AsNoTracking()
				.Where(e => 
					(query.Entity.PtId == default || e.PtId == query.Entity.PtId)
					&&(query.Entity.PtName == default || e.PtName.Contains(query.Entity.PtName))
					&&(query.Entity.PtDescription == default || e.PtDescription.Contains(query.Entity.PtDescription))

				)
				.Select(x => new ProductTypeView(x));
		}
		public async Task<IPagedList<ProductTypeView>> Search(QueryModel<ProductTypeQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.PageResultAsync(query.Page, query.PageSize, cancellationToken);
		}
		public async Task<List<ProductTypeView>> SearchNoPaging(QueryModel<ProductTypeQuery> query, CancellationToken cancellationToken = default)
		{
			return await Fillter(query, cancellationToken)
				.ToListAsync(cancellationToken);
		}

	}
}
