using ADR.CPS.Data.Models;
using ADR.CPS.SDK.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ADR.CPS.Data.Services
{
	public partial interface IService : IAsyncService, IUnitOfWork
	{
		Task<bool> ExistsAsync(Image entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(Image entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(Image entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<Image> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(AdminAccount entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(AdminAccount entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(AdminAccount entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<AdminAccount> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(Role entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(Role entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(Role entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<Role> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(Employee entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(Employee entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(Employee entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<Employee> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(MenuContent entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(MenuContent entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(MenuContent entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<MenuContent> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(Menu entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(Menu entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(Menu entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<Menu> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(GroupTable entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(GroupTable entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(GroupTable entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<GroupTable> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(News entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(News entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(News entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<News> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(NewsContent entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(NewsContent entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(NewsContent entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<NewsContent> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(TradeMark entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(TradeMark entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(TradeMark entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<TradeMark> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(Product entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(Product entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(Product entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<Product> entities, CancellationToken cancellationToken = default);

		Task<bool> ExistsAsync(ProductType entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(ProductType entity, CancellationToken cancellationToken = default);
		Task UpdateAsync(ProductType entity, CancellationToken cancellationToken = default);
		Task AddOrUpdateAsync(IEnumerable<ProductType> entities, CancellationToken cancellationToken = default);

	}
}