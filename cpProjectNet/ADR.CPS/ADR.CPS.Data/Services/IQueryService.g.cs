using ADR.CPS.Data.Models.Views;
using ADR.CPS.Data.Models.Queries;
using ADR.CPS.Data.Models;
using ADR.CPS.SDK.Data;
using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace ADR.CPS.Data.Services
{
	public partial interface IQueryService : IAsyncQueryService
	{
		Task<IPagedList<ImageView>> Search(QueryModel<ImageQuery> query, CancellationToken cancellationToken = default);
		Task<List<ImageView>> SearchNoPaging(QueryModel<ImageQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(Image entity, CancellationToken cancellationToken = default);

		Task<IPagedList<AdminAccountView>> Search(QueryModel<AdminAccountQuery> query, CancellationToken cancellationToken = default);
		Task<List<AdminAccountView>> SearchNoPaging(QueryModel<AdminAccountQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(AdminAccount entity, CancellationToken cancellationToken = default);

		Task<IPagedList<RoleView>> Search(QueryModel<RoleQuery> query, CancellationToken cancellationToken = default);
		Task<List<RoleView>> SearchNoPaging(QueryModel<RoleQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(Role entity, CancellationToken cancellationToken = default);

		Task<IPagedList<EmployeeView>> Search(QueryModel<EmployeeQuery> query, CancellationToken cancellationToken = default);
		Task<List<EmployeeView>> SearchNoPaging(QueryModel<EmployeeQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(Employee entity, CancellationToken cancellationToken = default);

		Task<IPagedList<MenuContentView>> Search(QueryModel<MenuContentQuery> query, CancellationToken cancellationToken = default);
		Task<List<MenuContentView>> SearchNoPaging(QueryModel<MenuContentQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(MenuContent entity, CancellationToken cancellationToken = default);

		Task<IPagedList<MenuView>> Search(QueryModel<MenuQuery> query, CancellationToken cancellationToken = default);
		Task<List<MenuView>> SearchNoPaging(QueryModel<MenuQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(Menu entity, CancellationToken cancellationToken = default);

		Task<IPagedList<GroupTableView>> Search(QueryModel<GroupTableQuery> query, CancellationToken cancellationToken = default);
		Task<List<GroupTableView>> SearchNoPaging(QueryModel<GroupTableQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(GroupTable entity, CancellationToken cancellationToken = default);

		Task<IPagedList<NewsView>> Search(QueryModel<NewsQuery> query, CancellationToken cancellationToken = default);
		Task<List<NewsView>> SearchNoPaging(QueryModel<NewsQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(News entity, CancellationToken cancellationToken = default);

		Task<IPagedList<NewsContentView>> Search(QueryModel<NewsContentQuery> query, CancellationToken cancellationToken = default);
		Task<List<NewsContentView>> SearchNoPaging(QueryModel<NewsContentQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(NewsContent entity, CancellationToken cancellationToken = default);

		Task<IPagedList<TradeMarkView>> Search(QueryModel<TradeMarkQuery> query, CancellationToken cancellationToken = default);
		Task<List<TradeMarkView>> SearchNoPaging(QueryModel<TradeMarkQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(TradeMark entity, CancellationToken cancellationToken = default);

		Task<IPagedList<ProductView>> Search(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default);
		Task<List<ProductView>> SearchNoPaging(QueryModel<ProductQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(Product entity, CancellationToken cancellationToken = default);

		Task<IPagedList<ProductTypeView>> Search(QueryModel<ProductTypeQuery> query, CancellationToken cancellationToken = default);
		Task<List<ProductTypeView>> SearchNoPaging(QueryModel<ProductTypeQuery> query, CancellationToken cancellationToken = default);
		Task<bool> ExistsAsync(ProductType entity, CancellationToken cancellationToken = default);

	}
}
