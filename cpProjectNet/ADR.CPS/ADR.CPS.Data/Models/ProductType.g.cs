using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class ProductType : BaseEntity, ICloneable
	{
		public long PtId { get; set;}
		public String PtName { get; set;}
		public String PtDescription { get; set;}
		public ICollection<Product> Products { get; set; }
		public ProductType():base()
		{
		}
		public override object Clone(){
			return new ProductType{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				PtId = this.PtId,
				PtName = this.PtName,
				PtDescription = this.PtDescription,
			};
		}
	}
}