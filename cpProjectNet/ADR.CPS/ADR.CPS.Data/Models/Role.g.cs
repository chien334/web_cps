using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Role : BaseEntity, ICloneable
	{
		public long RoleId { get; set;}
		public String RoleCode { get; set;}
		public String RoleName { get; set;}
		public long RoleTypeId { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public Role():base()
		{
		}
		public override object Clone(){
			return new Role{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				RoleId = this.RoleId,
				RoleCode = this.RoleCode,
				RoleName = this.RoleName,
				RoleTypeId = this.RoleTypeId,
			};
		}
	}
}