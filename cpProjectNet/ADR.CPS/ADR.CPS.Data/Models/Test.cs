using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Test : BaseEntity, ICloneable
	{
		public long PId { get; set;}
		public Test():base()
		{
		}
		public override object Clone(){
			return new Test{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				PId = this.PId,
			};
		}
	}
}