using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Menu : BaseEntity, ICloneable
	{
		public long MenuId { get; set;}
		public long MenuTypeId { get; set;}
		public long MctId { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public virtual MenuContent MenuContent { get; set;}
		public Menu():base()
		{
		}
		public override object Clone(){
			return new Menu{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				MenuId = this.MenuId,
				MenuTypeId = this.MenuTypeId,
				MctId = this.MctId,
			};
		}
	}
}