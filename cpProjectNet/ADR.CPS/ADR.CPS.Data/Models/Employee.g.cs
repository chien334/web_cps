using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Employee : BaseEntity, ICloneable
	{
		public long EmpId { get; set;}
		public String EmpFirstName { get; set;}
		public String EmpLastName { get; set;}
		public String EmpEmail { get; set;}
		public String EmpPhone { get; set;}
		public String EmpDescription { get; set;}
		public long GroupDepartmentId { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public Employee():base()
		{
		}
		public override object Clone(){
			return new Employee{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				EmpId = this.EmpId,
				EmpFirstName = this.EmpFirstName,
				EmpLastName = this.EmpLastName,
				EmpEmail = this.EmpEmail,
				EmpPhone = this.EmpPhone,
				EmpDescription = this.EmpDescription,
				GroupDepartmentId = this.GroupDepartmentId,
			};
		}
	}
}