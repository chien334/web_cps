using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Image : BaseEntity, ICloneable
	{
		public long ImgId { get; set;}
		public String ImgProperties { get; set;}
		public String ImgAbsolutePath { get; set;}
		public String ImgRelativePath { get; set;}
		public long ImageTypeId { get; set;}
		public long? ImgIdReferent { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public Image():base()
		{
		}
		public override object Clone(){
			return new Image{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				ImgId = this.ImgId,
				ImgProperties = this.ImgProperties,
				ImgAbsolutePath = this.ImgAbsolutePath,
				ImgRelativePath = this.ImgRelativePath,
				ImageTypeId = this.ImageTypeId,
				ImgIdReferent = this.ImgIdReferent,
			};
		}
	}
}