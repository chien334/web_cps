using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class GroupTable : BaseEntity, ICloneable
	{
		public long GroupId { get; set;}
		public String GroupTbcode { get; set;}
		public String GroupCode { get; set;}
		public String GroupName { get; set;}
		public ICollection<News> Newss { get; set; }
		public ICollection<NewsContent> NewsContents { get; set; }
		public ICollection<Role> Roles { get; set; }
		public ICollection<Menu> Menus { get; set; }
		public ICollection<MenuContent> MenuContents { get; set; }
		public ICollection<Employee> Employees { get; set; }
		public ICollection<Image> Images { get; set; }
		public GroupTable():base()
		{
		}
		public override object Clone(){
			return new GroupTable{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				GroupId = this.GroupId,
				GroupTbcode = this.GroupTbcode,
				GroupCode = this.GroupCode,
				GroupName = this.GroupName,
			};
		}
	}
}