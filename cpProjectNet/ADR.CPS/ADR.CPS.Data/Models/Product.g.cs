using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class Product : BaseEntity, ICloneable
	{
		public long PId { get; set;}
		public long PtId { get; set;}
		public long TmId { get; set;}
		public String PProperties { get; set;}
		public Boolean? PStatus { get; set;}
		public DateTime? PDate { get; set;}
		public virtual ProductType ProductType { get; set;}
		public virtual TradeMark TradeMark { get; set;}
		public Product():base()
		{
		}
		public override object Clone(){
			return new Product{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				PId = this.PId,
				PtId = this.PtId,
				TmId = this.TmId,
				PProperties = this.PProperties,
				PStatus = this.PStatus,
				PDate = this.PDate,
			};
		}
	}
}