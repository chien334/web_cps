using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class AdminAccount : BaseEntity, ICloneable
	{
		public long AdmId { get; set;}
		public String AdmUserName { get; set;}
		public String AdmPassword { get; set;}
		public String AdmFirstName { get; set;}
		public String AdmLastName { get; set;}
		public String AdmIdentityCard { get; set;}
		public DateTime? AdmDob { get; set;}
		public String AdmPhone { get; set;}
		public String AdmEmail { get; set;}
		public long RoleTypeId { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public AdminAccount():base()
		{
		}
		public override object Clone(){
			return new AdminAccount{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				AdmId = this.AdmId,
				AdmUserName = this.AdmUserName,
				AdmPassword = this.AdmPassword,
				AdmFirstName = this.AdmFirstName,
				AdmLastName = this.AdmLastName,
				AdmIdentityCard = this.AdmIdentityCard,
				AdmDob = this.AdmDob,
				AdmPhone = this.AdmPhone,
				AdmEmail = this.AdmEmail,
				RoleTypeId = this.RoleTypeId,
			};
		}
	}
}