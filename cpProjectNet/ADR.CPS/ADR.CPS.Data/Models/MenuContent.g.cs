using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class MenuContent : BaseEntity, ICloneable
	{
		public long MctId { get; set;}
		public long MenuId { get; set;}
		public long MctLanguageId { get; set;}
		public String MctTitle { get; set;}
		public String MctDescription { get; set;}
		public virtual Menu Menu { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public MenuContent():base()
		{
		}
		public override object Clone(){
			return new MenuContent{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				MctId = this.MctId,
				MenuId = this.MenuId,
				MctLanguageId = this.MctLanguageId,
				MctTitle = this.MctTitle,
				MctDescription = this.MctDescription,
			};
		}
	}
}