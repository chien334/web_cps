using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class News : BaseEntity, ICloneable
	{
		public long NewsId { get; set;}
		public long NewTypeId { get; set;}
		public ICollection<NewsContent> NewsContents { get; set; }
		public virtual GroupTable GroupTable { get; set;}
		public News():base()
		{
		}
		public override object Clone(){
			return new News{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				NewsId = this.NewsId,
				NewTypeId = this.NewTypeId,
			};
		}
	}
}