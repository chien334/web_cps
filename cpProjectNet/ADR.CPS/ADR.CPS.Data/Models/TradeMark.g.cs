using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class TradeMark : BaseEntity, ICloneable
	{
		public long TmId { get; set;}
		public String TmImage { get; set;}
		public Boolean? TmStatus { get; set;}
		public ICollection<Product> Products { get; set; }
		public TradeMark():base()
		{
		}
		public override object Clone(){
			return new TradeMark{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				TmId = this.TmId,
				TmImage = this.TmImage,
				TmStatus = this.TmStatus,
			};
		}
	}
}