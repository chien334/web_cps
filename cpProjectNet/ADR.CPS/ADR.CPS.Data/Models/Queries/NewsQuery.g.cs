using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class NewsQuery
	{
		public long NewsId { get; set; }
		public long NewTypeId { get; set; }

	}
}