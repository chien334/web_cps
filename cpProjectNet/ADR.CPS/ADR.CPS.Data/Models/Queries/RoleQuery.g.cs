using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class RoleQuery
	{
		public long RoleId { get; set; }
		public String RoleCode { get; set; }
		public String RoleName { get; set; }
		public long RoleTypeId { get; set; }

	}
}