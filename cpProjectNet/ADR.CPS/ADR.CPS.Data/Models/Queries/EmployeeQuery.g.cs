using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class EmployeeQuery
	{
		public long EmpId { get; set; }
		public String EmpFirstName { get; set; }
		public String EmpLastName { get; set; }
		public String EmpEmail { get; set; }
		public String EmpPhone { get; set; }
		public String EmpDescription { get; set; }
		public long GroupDepartmentId { get; set; }

	}
}