using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class NewsTypeQuery
	{
		public long NewsId { get; set; }
		public String NewsTypeId { get; set; }

	}
}