using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class TradeMarkQuery
	{
		public long TmId { get; set; }
		public String TmImage { get; set; }
		public Boolean TmStatus { get; set; }

	}
}