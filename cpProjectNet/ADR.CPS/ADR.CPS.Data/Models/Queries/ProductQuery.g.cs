using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class ProductQuery
	{
		public long PId { get; set; }
		public long PtId { get; set; }
		public long TmId { get; set; }
		public String PProperties { get; set; }
		public Boolean PStatus { get; set; }
		public DateTime FromPDate { get; set; }
		public DateTime ToPDate { get; set; }

	}
}