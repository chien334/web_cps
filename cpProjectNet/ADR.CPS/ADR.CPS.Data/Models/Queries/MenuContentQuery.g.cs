using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class MenuContentQuery
	{
		public long MctId { get; set; }
		public long MenuId { get; set; }
		public long MctLanguageId { get; set; }
		public String MctTitle { get; set; }
		public String MctDescription { get; set; }

	}
}