using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class ImageQuery
	{
		public long ImgId { get; set; }
		public String ImgProperties { get; set; }
		public String ImgAbsolutePath { get; set; }
		public String ImgRelativePath { get; set; }
		public long ImageTypeId { get; set; }
		public long FromImgIdReferent { get; set; }
		public long ToImgIdReferent { get; set; }

	}
}