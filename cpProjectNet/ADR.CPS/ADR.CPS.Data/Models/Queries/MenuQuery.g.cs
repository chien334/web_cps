using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class MenuQuery
	{
		public long MenuId { get; set; }
		public long MenuTypeId { get; set; }
		public long MctId { get; set; }

	}
}