using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class ProductTypeQuery
	{
		public long PtId { get; set; }
		public String PtName { get; set; }
		public String PtDescription { get; set; }

	}
}