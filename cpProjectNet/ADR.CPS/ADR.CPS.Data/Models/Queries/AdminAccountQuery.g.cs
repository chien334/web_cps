using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class AdminAccountQuery
	{
		public long AdmId { get; set; }
		public String AdmUserName { get; set; }
		public String AdmPassword { get; set; }
		public String AdmFirstName { get; set; }
		public String AdmLastName { get; set; }
		public String AdmIdentityCard { get; set; }
		public DateTime FromAdmDob { get; set; }
		public DateTime ToAdmDob { get; set; }
		public String AdmPhone { get; set; }
		public String AdmEmail { get; set; }
		public long RoleTypeId { get; set; }

	}
}