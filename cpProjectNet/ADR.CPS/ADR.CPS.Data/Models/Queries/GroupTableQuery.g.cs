using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class GroupTableQuery
	{
		public long GroupId { get; set; }
		public String GroupTbcode { get; set; }
		public String GroupCode { get; set; }
		public String GroupName { get; set; }

	}
}