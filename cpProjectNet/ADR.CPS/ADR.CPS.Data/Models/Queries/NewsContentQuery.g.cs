using System;
using System.Collections.Generic;
using ADR.CPS.SDK.Models;

namespace ADR.CPS.Data.Models.Queries
{
	public partial class NewsContentQuery
	{
		public long NcId { get; set; }
		public long NcNewsId { get; set; }
		public long NcLanguageId { get; set; }
		public String NcTitle { get; set; }
		public String NcDescription { get; set; }
		public String NcContent { get; set; }

	}
}