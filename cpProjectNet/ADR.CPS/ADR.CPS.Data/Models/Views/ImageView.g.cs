using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class ImageView{
		public System.Boolean Deleted { get; set;}
		public long ImgId { get; set;}
		public String ImgProperties { get; set;}
		public String ImgAbsolutePath { get; set;}
		public String ImgRelativePath { get; set;}
		public long ImageTypeId { get; set;}
		public long? ImgIdReferent { get; set;}
		public ImageView(){}
		public ImageView(Image model){
			if (model != null){
				Deleted = this.Deleted;
				ImgId = model.ImgId;
				ImgProperties = model.ImgProperties;
				ImgAbsolutePath = model.ImgAbsolutePath;
				ImgRelativePath = model.ImgRelativePath;
				ImageTypeId = model.ImageTypeId;
				ImgIdReferent = model.ImgIdReferent;
			}
		}
	}
}