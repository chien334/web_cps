using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class ProductView{
		public System.Boolean Deleted { get; set;}
		public long PId { get; set;}
		public long PtId { get; set;}
		public long TmId { get; set;}
		public String PProperties { get; set;}
		public Boolean? PStatus { get; set;}
		public DateTime? PDate { get; set;}
		public ProductView(){}
		public ProductView(Product model){
			if (model != null){
				Deleted = this.Deleted;
				PId = model.PId;
				PtId = model.PtId;
				TmId = model.TmId;
				PProperties = model.PProperties;
				PStatus = model.PStatus;
				PDate = model.PDate;
			}
		}
	}
}