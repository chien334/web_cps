using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class AdminAccountView{
		public System.Boolean Deleted { get; set;}
		public long AdmId { get; set;}
		public String AdmUserName { get; set;}
		public String AdmPassword { get; set;}
		public String AdmFirstName { get; set;}
		public String AdmLastName { get; set;}
		public String AdmIdentityCard { get; set;}
		public DateTime? AdmDob { get; set;}
		public String AdmPhone { get; set;}
		public String AdmEmail { get; set;}
		public long RoleTypeId { get; set;}
		public AdminAccountView(){}
		public AdminAccountView(AdminAccount model){
			if (model != null){
				Deleted = this.Deleted;
				AdmId = model.AdmId;
				AdmUserName = model.AdmUserName;
				AdmPassword = model.AdmPassword;
				AdmFirstName = model.AdmFirstName;
				AdmLastName = model.AdmLastName;
				AdmIdentityCard = model.AdmIdentityCard;
				AdmDob = model.AdmDob;
				AdmPhone = model.AdmPhone;
				AdmEmail = model.AdmEmail;
				RoleTypeId = model.RoleTypeId;
			}
		}
	}
}