using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class RoleView{
		public System.Boolean Deleted { get; set;}
		public long RoleId { get; set;}
		public String RoleCode { get; set;}
		public String RoleName { get; set;}
		public long RoleTypeId { get; set;}
		public RoleView(){}
		public RoleView(Role model){
			if (model != null){
				Deleted = this.Deleted;
				RoleId = model.RoleId;
				RoleCode = model.RoleCode;
				RoleName = model.RoleName;
				RoleTypeId = model.RoleTypeId;
			}
		}
	}
}