using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class TradeMarkView{
		public System.Boolean Deleted { get; set;}
		public long TmId { get; set;}
		public String TmImage { get; set;}
		public Boolean? TmStatus { get; set;}
		public TradeMarkView(){}
		public TradeMarkView(TradeMark model){
			if (model != null){
				Deleted = this.Deleted;
				TmId = model.TmId;
				TmImage = model.TmImage;
				TmStatus = model.TmStatus;
			}
		}
	}
}