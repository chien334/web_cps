using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class ProductTypeView{
		public System.Boolean Deleted { get; set;}
		public long PtId { get; set;}
		public String PtName { get; set;}
		public String PtDescription { get; set;}
		public ProductTypeView(){}
		public ProductTypeView(ProductType model){
			if (model != null){
				Deleted = this.Deleted;
				PtId = model.PtId;
				PtName = model.PtName;
				PtDescription = model.PtDescription;
			}
		}
	}
}