using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class NewsContentView{
		public System.Boolean Deleted { get; set;}
		public long NcId { get; set;}
		public long NcNewsId { get; set;}
		public long NcLanguageId { get; set;}
		public String NcTitle { get; set;}
		public String NcDescription { get; set;}
		public String NcContent { get; set;}
		public NewsContentView(){}
		public NewsContentView(NewsContent model){
			if (model != null){
				Deleted = this.Deleted;
				NcId = model.NcId;
				NcNewsId = model.NcNewsId;
				NcLanguageId = model.NcLanguageId;
				NcTitle = model.NcTitle;
				NcDescription = model.NcDescription;
				NcContent = model.NcContent;
			}
		}
	}
}