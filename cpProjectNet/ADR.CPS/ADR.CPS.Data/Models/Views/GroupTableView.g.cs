using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class GroupTableView{
		public System.Boolean Deleted { get; set;}
		public long GroupId { get; set;}
		public String GroupTbcode { get; set;}
		public String GroupCode { get; set;}
		public String GroupName { get; set;}
		public GroupTableView(){}
		public GroupTableView(GroupTable model){
			if (model != null){
				Deleted = this.Deleted;
				GroupId = model.GroupId;
				GroupTbcode = model.GroupTbcode;
				GroupCode = model.GroupCode;
				GroupName = model.GroupName;
			}
		}
	}
}