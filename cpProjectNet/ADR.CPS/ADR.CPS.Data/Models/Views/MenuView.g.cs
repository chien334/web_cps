using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class MenuView{
		public System.Boolean Deleted { get; set;}
		public long MenuId { get; set;}
		public long MenuTypeId { get; set;}
		public long MctId { get; set;}
		public MenuView(){}
		public MenuView(Menu model){
			if (model != null){
				Deleted = this.Deleted;
				MenuId = model.MenuId;
				MenuTypeId = model.MenuTypeId;
				MctId = model.MctId;
			}
		}
	}
}