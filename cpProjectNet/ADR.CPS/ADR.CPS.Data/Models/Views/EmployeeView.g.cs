using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class EmployeeView{
		public System.Boolean Deleted { get; set;}
		public long EmpId { get; set;}
		public String EmpFirstName { get; set;}
		public String EmpLastName { get; set;}
		public String EmpEmail { get; set;}
		public String EmpPhone { get; set;}
		public String EmpDescription { get; set;}
		public long GroupDepartmentId { get; set;}
		public EmployeeView(){}
		public EmployeeView(Employee model){
			if (model != null){
				Deleted = this.Deleted;
				EmpId = model.EmpId;
				EmpFirstName = model.EmpFirstName;
				EmpLastName = model.EmpLastName;
				EmpEmail = model.EmpEmail;
				EmpPhone = model.EmpPhone;
				EmpDescription = model.EmpDescription;
				GroupDepartmentId = model.GroupDepartmentId;
			}
		}
	}
}