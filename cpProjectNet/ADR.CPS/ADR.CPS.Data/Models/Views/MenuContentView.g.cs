using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class MenuContentView{
		public System.Boolean Deleted { get; set;}
		public long MctId { get; set;}
		public long MenuId { get; set;}
		public long MctLanguageId { get; set;}
		public String MctTitle { get; set;}
		public String MctDescription { get; set;}
		public MenuContentView(){}
		public MenuContentView(MenuContent model){
			if (model != null){
				Deleted = this.Deleted;
				MctId = model.MctId;
				MenuId = model.MenuId;
				MctLanguageId = model.MctLanguageId;
				MctTitle = model.MctTitle;
				MctDescription = model.MctDescription;
			}
		}
	}
}