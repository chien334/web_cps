using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models.Views
{
	public partial class NewsView{
		public System.Boolean Deleted { get; set;}
		public long NewsId { get; set;}
		public long NewTypeId { get; set;}
		public NewsView(){}
		public NewsView(News model){
			if (model != null){
				Deleted = this.Deleted;
				NewsId = model.NewsId;
				NewTypeId = model.NewTypeId;
			}
		}
	}
}