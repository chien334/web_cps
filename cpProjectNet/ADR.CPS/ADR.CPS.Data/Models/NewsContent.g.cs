using ADR.CPS.SDK.Models;
using System;
using System.Collections.Generic;
using System.Text;
namespace ADR.CPS.Data.Models
{
	public partial class NewsContent : BaseEntity, ICloneable
	{
		public long NcId { get; set;}
		public long NcNewsId { get; set;}
		public long NcLanguageId { get; set;}
		public String NcTitle { get; set;}
		public String NcDescription { get; set;}
		public String NcContent { get; set;}
		public virtual News News { get; set;}
		public virtual GroupTable GroupTable { get; set;}
		public NewsContent():base()
		{
		}
		public override object Clone(){
			return new NewsContent{
				Deleted = this.Deleted,
				LastUpdatedTime = this.LastUpdatedTime,
				LastUpdatedBy = this.LastUpdatedBy,
				CreatedTime = this.CreatedTime,
				CreatedBy = this.CreatedBy,
				NcId = this.NcId,
				NcNewsId = this.NcNewsId,
				NcLanguageId = this.NcLanguageId,
				NcTitle = this.NcTitle,
				NcDescription = this.NcDescription,
				NcContent = this.NcContent,
			};
		}
	}
}