﻿using ADR.CPS.Data.Responsitories;
using ADR.CPS.Data.Services;
using ADR.CPS.Data.Services.Services;
using ADR.CPS.SDK.Data.EFService;
using ADR.CPS.SDK.Metadata;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ADR.CPS.Data.StartupExtensions
{
    public static class AddServiceExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IQueryService, QueryService>();
            services.AddTransient<IService, Service>();
            services.AddDbContext<CPSContext>(options =>
            {
                options.UseNpgsql(
               configuration.GetConnectionString("CPSContext"),
               x => x.MigrationsAssembly("ADR.CPS.Data"));
            });
            
            services.AddTransient<IBulkOperation, SqlServerBulkOperation>();
            return services;
        }
    }
}
