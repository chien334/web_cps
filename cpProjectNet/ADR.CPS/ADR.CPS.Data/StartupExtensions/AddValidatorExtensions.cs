﻿using ADR.CPS.Data.Models;
using ADR.CPS.Data.Services;
using ADR.CPS.Data.Validations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ADR.CPS.Data.StartupExtensions
{
    public static class AddValidatorExtensions
    {
        public static IServiceCollection AddValidators(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IValidator<Test>, TestValidator>();
            return services;
        }
    }
}
