using ADR.CPS.Data.Models;
using ADR.CPS.SDK.IoC;
using ADR.CPS.SDK.Metadata;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace ADR.CPS.Data.Responsitories
{
	public partial class CPSContext : DbContext
	{
		private readonly IServiceProvider _serviceProvider;
		public CPSContext(DbContextOptions<CPSContext> options) : base(options)
		{
			_serviceProvider = Container.Services;
			if (_serviceProvider == null) _serviceProvider = new ServiceCollection().BuildServiceProvider();
		}
		public DbSet<ADR.CPS.Data.Models.Image> Images { get; set;}
		public DbSet<ADR.CPS.Data.Models.AdminAccount> AdminAccounts { get; set;}
		public DbSet<ADR.CPS.Data.Models.Role> Roles { get; set;}
		public DbSet<ADR.CPS.Data.Models.Employee> Employees { get; set;}
		public DbSet<ADR.CPS.Data.Models.MenuContent> MenuContents { get; set;}
		public DbSet<ADR.CPS.Data.Models.Menu> Menus { get; set;}
		public DbSet<ADR.CPS.Data.Models.GroupTable> GroupTables { get; set;}
		public DbSet<ADR.CPS.Data.Models.News> Newss { get; set;}
		public DbSet<ADR.CPS.Data.Models.NewsContent> NewsContents { get; set;}
		public DbSet<ADR.CPS.Data.Models.TradeMark> TradeMarks { get; set;}
		public DbSet<ADR.CPS.Data.Models.Product> Products { get; set;}
		public DbSet<ADR.CPS.Data.Models.ProductType> ProductTypes { get; set;}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			var dbDescription = _serviceProvider.GetService<IDbDescription>();
			modelBuilder.ApplyConfiguration(new ImageConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new AdminAccountConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new RoleConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new EmployeeConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new MenuContentConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new MenuConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new GroupTableConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new NewsConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new NewsContentConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new TradeMarkConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new ProductConfiguration(dbDescription));
			modelBuilder.ApplyConfiguration(new ProductTypeConfiguration(dbDescription));
		}
	}
	public partial class ImageConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.Image>
	{
		private readonly IDbDescription _dbDescription;
		public ImageConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.Image> builder)
		{
			builder.ToTable("Image");
			builder.HasKey(e => e.ImgId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.ImgId).IsRequired();
			builder.Property(e => e.ImgProperties);
			builder.Property(e => e.ImgAbsolutePath).HasMaxLength(500);
			builder.Property(e => e.ImgRelativePath).HasMaxLength(500);
			builder.Property(e => e.ImageTypeId);
			builder.Property(e => e.ImgIdReferent);

		}
	}
	public partial class AdminAccountConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.AdminAccount>
	{
		private readonly IDbDescription _dbDescription;
		public AdminAccountConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.AdminAccount> builder)
		{
			builder.ToTable("AdminAccount");
			builder.HasKey(e => e.AdmId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.AdmId).IsRequired();
			builder.Property(e => e.AdmUserName).HasMaxLength(30);
			builder.Property(e => e.AdmPassword).HasMaxLength(30);
			builder.Property(e => e.AdmFirstName).HasMaxLength(30);
			builder.Property(e => e.AdmLastName).HasMaxLength(30);
			builder.Property(e => e.AdmIdentityCard).HasMaxLength(20);
			builder.Property(e => e.AdmDob);
			builder.Property(e => e.AdmPhone).HasMaxLength(12);
			builder.Property(e => e.AdmEmail).HasMaxLength(100);
			builder.Property(e => e.RoleTypeId);

		}
	}
	public partial class RoleConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.Role>
	{
		private readonly IDbDescription _dbDescription;
		public RoleConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.Role> builder)
		{
			builder.ToTable("Role");
			builder.HasKey(e => e.RoleId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.RoleId).IsRequired();
			builder.Property(e => e.RoleCode).HasMaxLength(20);
			builder.Property(e => e.RoleName).HasMaxLength(80);
			builder.Property(e => e.RoleTypeId);

		}
	}
	public partial class EmployeeConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.Employee>
	{
		private readonly IDbDescription _dbDescription;
		public EmployeeConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.Employee> builder)
		{
			builder.ToTable("Employee");
			builder.HasKey(e => e.EmpId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.EmpId).IsRequired();
			builder.Property(e => e.EmpFirstName).HasMaxLength(30);
			builder.Property(e => e.EmpLastName).HasMaxLength(30);
			builder.Property(e => e.EmpEmail).HasMaxLength(100);
			builder.Property(e => e.EmpPhone).HasMaxLength(12);
			builder.Property(e => e.EmpDescription);
			builder.Property(e => e.GroupDepartmentId);

		}
	}
	public partial class MenuContentConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.MenuContent>
	{
		private readonly IDbDescription _dbDescription;
		public MenuContentConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.MenuContent> builder)
		{
			builder.ToTable("MenuContent");
			builder.HasKey(e => e.MctId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.MctId).IsRequired();
			builder.Property(e => e.MenuId);
			builder.Property(e => e.MctLanguageId);
			builder.Property(e => e.MctTitle).HasMaxLength(256);
			builder.Property(e => e.MctDescription).HasMaxLength(1000);

		}
	}
	public partial class MenuConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.Menu>
	{
		private readonly IDbDescription _dbDescription;
		public MenuConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.Menu> builder)
		{
			builder.ToTable("Menu");
			builder.HasKey(e => e.MenuId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.MenuId).IsRequired();
			builder.Property(e => e.MenuTypeId);
			builder.Property(e => e.MctId);

		}
	}
	public partial class GroupTableConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.GroupTable>
	{
		private readonly IDbDescription _dbDescription;
		public GroupTableConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.GroupTable> builder)
		{
			builder.ToTable("GroupTable");
			builder.HasKey(e => e.GroupId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.GroupId).IsRequired();
			builder.Property(e => e.GroupTbcode).HasMaxLength(20);
			builder.Property(e => e.GroupCode).HasMaxLength(20);
			builder.Property(e => e.GroupName).HasMaxLength(100);
			builder.HasMany(e => e.Newss)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.NewsId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.NewsContents)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.NcId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.Roles)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.RoleId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.Menus)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.MenuId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.MenuContents)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.MctId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.Employees)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.EmpId)
				.OnDelete(DeleteBehavior.NoAction);
			builder.HasMany(e => e.Images)
				.WithOne(e => e.GroupTable)
				.HasForeignKey(e => e.ImgId)
				.OnDelete(DeleteBehavior.NoAction);

		}
	}
	public partial class NewsConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.News>
	{
		private readonly IDbDescription _dbDescription;
		public NewsConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.News> builder)
		{
			builder.ToTable("News");
			builder.HasKey(e => e.NewsId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.NewsId).IsRequired();
			builder.Property(e => e.NewTypeId);
			builder.HasMany(e => e.NewsContents)
				.WithOne(e => e.News)
				.HasForeignKey(e => e.NcId)
				.OnDelete(DeleteBehavior.NoAction);

		}
	}
	public partial class NewsContentConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.NewsContent>
	{
		private readonly IDbDescription _dbDescription;
		public NewsContentConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.NewsContent> builder)
		{
			builder.ToTable("NewsContent");
			builder.HasKey(e => e.NcId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.NcId).IsRequired();
			builder.Property(e => e.NcNewsId);
			builder.Property(e => e.NcLanguageId);
			builder.Property(e => e.NcTitle).HasMaxLength(256);
			builder.Property(e => e.NcDescription).HasMaxLength(1000);
			builder.Property(e => e.NcContent);

		}
	}
	public partial class TradeMarkConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.TradeMark>
	{
		private readonly IDbDescription _dbDescription;
		public TradeMarkConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.TradeMark> builder)
		{
			builder.ToTable("TradeMark");
			builder.HasKey(e => e.TmId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.TmId).IsRequired();
			builder.Property(e => e.TmImage).HasMaxLength(500);
			builder.Property(e => e.TmStatus);
			builder.HasMany(e => e.Products)
				.WithOne(e => e.TradeMark)
				.HasForeignKey(e => e.PId)
				.OnDelete(DeleteBehavior.NoAction);

		}
	}
	public partial class ProductConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.Product>
	{
		private readonly IDbDescription _dbDescription;
		public ProductConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.Product> builder)
		{
			builder.ToTable("Product");
			builder.HasKey(e => e.PId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.PId).IsRequired();
			builder.Property(e => e.PtId);
			builder.Property(e => e.TmId);
			builder.Property(e => e.PProperties);
			builder.Property(e => e.PStatus);
			builder.Property(e => e.PDate);

		}
	}
	public partial class ProductTypeConfiguration : IEntityTypeConfiguration<ADR.CPS.Data.Models.ProductType>
	{
		private readonly IDbDescription _dbDescription;
		public ProductTypeConfiguration(IDbDescription dbDescription)
		{
			_dbDescription = dbDescription;
		}
		public void Configure(EntityTypeBuilder<ADR.CPS.Data.Models.ProductType> builder)
		{
			builder.ToTable("ProductType");
			builder.HasKey(e => e.PtId);
			builder.Property(e => e.Deleted).IsRequired();
			builder.Property(e => e.LastUpdatedTime);
			builder.Property(e => e.LastUpdatedBy).HasMaxLength(50);
			builder.Property(e => e.CreatedTime);
			builder.Property(e => e.CreatedBy).HasMaxLength(50);
			builder.Property(e => e.PtId).IsRequired();
			builder.Property(e => e.PtName);
			builder.Property(e => e.PtDescription);
			builder.HasMany(e => e.Products)
				.WithOne(e => e.ProductType)
				.HasForeignKey(e => e.PId)
				.OnDelete(DeleteBehavior.NoAction);

		}
	}
}
