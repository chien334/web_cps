﻿using ADR.CPS.Data.Models;
using ADR.CPS.Data.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace ADR.CPS.Data.Validations
{
    public class TestValidator : IValidator<Test>
    {
        public async Task<List<ValidationResult>> ValidateAsync(Test entity, OperationTypes operation, string userName)
        {
            var result = new List<ValidationResult>();

            return await Task.FromResult(result);
        }
    }
}
