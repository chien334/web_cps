import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { AuthService } from '../authencation/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
})
export class AutoLoginComponent implements OnInit {

    isLoggedIn: any;
    constructor(public authService: AuthService, private router: Router, private oidcSecurityService: OidcSecurityService) { }

    ngOnInit() {
        this.oidcSecurityService.checkAuth().subscribe(() => this.oidcSecurityService.authorize());
    }
}
