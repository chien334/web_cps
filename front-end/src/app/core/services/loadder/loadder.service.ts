
import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LoaderService {
    renderer: Renderer2;
    private loadStates: boolean[] = [];
    loadingElement: HTMLDivElement;

    /** constructor */
    constructor(rendererFactory: RendererFactory2,
        public http: HttpClient) {
        this.http = http;
        this.loadingElement = document.getElementById('app-loader') as HTMLDivElement;
        this.renderer = rendererFactory.createRenderer(null, null);
    }

    /** Hide loader when done call api or init app. */
    hide() {
        this.loadStates.shift();
        this.checkState();
    }
    /** Show loader when done call api or init app. */
    show() {
        this.loadStates.push(true);
        this.checkState();
    }
    /** Check state for loader css . */
    checkState() {
        if (this.loadingElement) {
            // render class when loading or done loading.
            this.renderer[this.loadStates.length ? 'removeClass' : 'addClass'](this.loadingElement, 'hidden');
            this.renderer[!this.loadStates.length ? 'removeClass' : 'addClass'](document.body, 'lock-scroll');
        }
    }
}

