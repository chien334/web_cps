import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError, finalize } from 'rxjs/operators';
import { LoaderService } from '../loadder/loadder.service';



@Injectable()
export class HttpService {
    // contructor
    constructor(private http: HttpClient, protected loaderService: LoaderService) {
    }

    /** handle Error */
    static handleError(errors: any): any {
        return throwError(errors);
    }

    /** Throw format Error */
    private formatErrors(error: any): any {
        return throwError(error.error);
    }

    /** Check loader request */
    checkLoader(options: ExtraRequestOptions = {}, show = true): any {
        if (options.withoutLoader) {
            return;
        }
        if (show) {
            this.loaderService.show();
        } else {
            this.loaderService.hide();
        }
    }

    /** Get request */
    get(requestUrl: string, options?: ExtraRequestOptions): Observable<any> {
        return this.request(this.http.get(requestUrl, options), options)
            .pipe(catchError(this.formatErrors));
    }

    /** Post request */
    post(requestUrl: string, body: any | null, options?: ExtraRequestOptions): any {
        return this.request(this.http.post(requestUrl, body, options), options);
    }

    /** Delete request */
    delete(path, option?: ExtraRequestOptions): Observable<any> {
        return this.http.delete(path, option).pipe(catchError(this.formatErrors));
    }

    /** Get request */
    request(req: Observable<any>, options?: ExtraRequestOptions): any {
        this.checkLoader(options);
        return req.pipe(
            finalize(() => this.checkLoader(options, false)),
            catchError(HttpService.handleError)
        );
    }
}

/** Base request */
export interface BaseRequestOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe?: any;
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: any;
    withCredentials?: boolean;
}

export interface ExtraRequestOptions extends BaseRequestOptions {
    withoutLoader?: boolean;
}
