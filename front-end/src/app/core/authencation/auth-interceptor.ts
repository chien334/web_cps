import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpErrorResponse, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpSentEvent, HttpUserEvent
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserService } from 'src/app/utils/user-service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService, public userService: UserService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse
        | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {
        let requestToForward = request;

        const token = this.authService.token;

        if (token !== '') {
            const tokenValue = 'Bearer ' + token;
            requestToForward = request.clone({ setHeaders: { Authorization: tokenValue } });
        }

        return next.handle(requestToForward).pipe(catchError(err => {
            if (err instanceof HttpErrorResponse) {
                switch ((<HttpErrorResponse>err).status) {
                    case 401:
                        this.clean();
                        return throwError(err);
                    case 403:
                        this.clean();
                        return throwError(err);
                    default:
                        return throwError(err);
                }
            } else {
                return throwError(err);
            }
        }));
    }

    private clean() {
        this.userService.clear('permision');
        this.userService.clear('roleList');
        this.userService.clear('userName');
        this.authService.signOut();
    }
}
