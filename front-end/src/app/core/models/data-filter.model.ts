
export interface DataFilter {
    effectiveFrom?: any;
    effectiveTo?: any;
    channelCodes?: any;
    countryCodes?: any;
    regionCodes?: any;
    businessTypes?: any;
    provinceCodes?: any;
    keyAccounts?: any;
    customerCodes?: any;
    storeCodes?: any;
    categoryType?: any;
}
