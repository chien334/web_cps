export interface User {
    name?: string;
    fullName?: string;
    role?: any;
    permision?: any[];
    roleList?: any[];
    picture?: any;
}
