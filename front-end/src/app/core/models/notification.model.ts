export interface Notification {
    title?: string;
    message?: string;
    read: boolean;
    actions?: any[];
    lastUpdatedDateTime?: any
}
