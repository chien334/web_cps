import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { DefaultFilter } from 'ng2-smart-table';

@Component({
  template: `
    <tri-checkbox
      [value]="state === undefined || state === null ? null : state"
      (stateChange)="onStateChange($event)"
      > </tri-checkbox>
  `,
})
export class CPSCheckBoxFilterComponent extends DefaultFilter implements OnChanges {
  state: boolean;
  constructor() {
    super();
  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  onStateChange(data: boolean): any {
    if (data === undefined || data === null) {
      this.query = '';
      this.state = undefined;
    } else {
      this.state = data;
      this.query = (data).toString();
    }
    this.setFilter()
  }
}