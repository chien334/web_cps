import { Component, OnChanges, OnInit, SimpleChanges, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { DefaultFilter, DefaultEditor, ViewCell } from 'ng2-smart-table';

@Component({
  selector: 'view-checkbox',
  template: `
    <input 
      type="checkbox" disabled
      [checked]="this.value">
  `
})
export class CPSCheckBoxViewComponent implements ViewCell, OnInit {

  @Input() value: any;
  @Input() rowData: any;

  constructor() { }

  ngOnInit(): void{

  }
}