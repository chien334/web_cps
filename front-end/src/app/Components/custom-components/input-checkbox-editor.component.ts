import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { DefaultEditor } from 'ng2-smart-table';

@Component({
  template: `
    <input
      [checked]="this.state"
      [value]="this.state"
      [name]="cell.getId()"
      [disabled]="!cell.isEditable()"
      (click)="updateValue($event)"
      type="checkbox" class="ng-pristine ng-valid ng-touched">
  `,
})
export class CPSCheckBoxEditorComponent extends DefaultEditor implements AfterViewInit {
  state: boolean = false;
  constructor(private changeDetector: ChangeDetectorRef) {
    super();
  }

  updateValue(value): void {
    if (value && this.cell.newValue !== null) {
      this.state = !this.state;
    }
    console.log('cell', this.cell);
    // this.state = value;
    // var strVal = (value === undefined || value === null) ? '' : value.toString();
    this.cell.setValue(this.state);
  }

  ngAfterViewInit(): void{
    if (this.cell.newValue === '') {
      this.state = false;
      this.cell.setValue(this.state);
    }
    this.changeDetector.detectChanges();
  }
}
