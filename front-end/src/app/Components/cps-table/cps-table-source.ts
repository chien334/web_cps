import { LocalDataSource } from 'ng2-smart-table';
import { map } from 'rxjs/operators';
import { deepExtend, getProperty } from 'src/app/utils/helpers';
import { range } from 'rxjs';
import { HttpService } from 'src/app/core/services/http/http.service';

export class CPSDataSource extends LocalDataSource {
    protected totalCount: number;
    protected loaded = false;

    constructor(protected http: HttpService,
                protected searchApi: string,
                protected localMode: boolean = false,
                protected columnsDef: Object = {},
                protected additionalArgs: Object = {}) {
        super();
    }

    count(): number {
        return this.totalCount;
    }

    getElements(): Promise<any> {
        if (this.localMode) {
            if (!this.loaded) {
                return this.getServerData().then((value) => {
                    this.data = value;
                    this.loaded = true;
                    return super.getElements();
                });
            }
            return super.getElements();
        }
        return this.getServerData().then((value) => {
            this.data = value;
            return value;
        });
    }

    getSearchRequest(): Object {
        const pager = this.getPager();
        const requestBase: Object = {
            entity: this.getFilters(),
            sortedColumns: this.getSorts(),
            page: pager.page === 0 ? 1 : pager.page,
            pageSize: this.localMode ? 0 : pager.pageSize === -1 ? 10 : pager.pageSize
        };
        const request = deepExtend({}, requestBase, this.additionalArgs);
        return request;
    }

    protected getServerData(): Promise<any> {
        // this.additionalArgs = {
        //     entity:{
        //         "ID":5
        //     }
        // }

        const request = this.getSearchRequest();

        return this.http.post(this.searchApi, request)
            .pipe(
                map((data: any) => {
                    this.totalCount = data.totalCount;
                    return this.data = data.items;
                })
            ).toPromise();
    }

    protected getFilters(): any {
        const filters = {};
        const columns = this.columnsDef;

        if (this.filterConf.filters) {
            this.filterConf.filters.forEach((fieldConf: any) => {
                if (fieldConf['search'] !== undefined) {
                    const dataType = getProperty(columns, fieldConf['field'] + '.dataType');
                    const convertFunc = getProperty(columns, fieldConf['field'] + '.dataConverter');
                    const filterType = getProperty(columns, fieldConf['field'] + '.filter.filterType');
                    if (filterType === 'multi') {
                        filters[fieldConf['field']] = fieldConf['search'].split(';').map(x => this.convertValue(x, dataType, convertFunc)).filter(x => x !== undefined);
                    }
                    else if (filterType === 'range') {
                        var ranges: any[] = fieldConf['search'].split(';');//.map(x =>(x!='')? this.convertValue(x, dataType, convertFunc):x);
                        if (ranges[0] != '') {
                            filters['from' + this.upperCaseFirstLetter(fieldConf['field'])] = this.convertValue(ranges[0], dataType, convertFunc)
                        }
                        if (ranges[1] != '') {
                            filters['to' + this.upperCaseFirstLetter(fieldConf['field'])] = this.convertValue(ranges[1], dataType, convertFunc)
                        }
                        var ranges: any[] = fieldConf['search'].split(';').map(x => this.convertValue(x, dataType, convertFunc));
                        if (ranges[0] !== undefined) {
                            filters['from' + this.upperCaseFirstLetter(fieldConf['field'])] = ranges[0];
                        }
                        if (ranges[1] !== undefined) {
                            filters['to' + this.upperCaseFirstLetter(fieldConf['field'])] = ranges[1];
                        }
                    }
                    else {
                        const value = this.convertValue(fieldConf['search'], dataType, convertFunc);
                        if (value !== undefined) {
                            filters[fieldConf['field']] = value;
                        }
                    }
                }
            });
        }
        return filters;
    }

    protected convertValue(value: string, dataType: string, convertFunc: (v: string) => any = null): any {

        if (value === null
            || value === undefined) { return undefined; }
        let convertValue: any = null;
        switch (dataType) {
            case 'number':
                convertValue = value === '' ? undefined : Number(JSON.parse(value));
                break;
            case 'boolean':
                convertValue = value === '' ? undefined : Boolean(JSON.parse(value));
                break;
            case 'date':
                convertValue = value === '' ? undefined : new Date(value);
                break;
            case 'custom':
                if (convertFunc != null) {
                    convertValue = convertFunc(value);
                }
                else {
                    convertValue = value;
                }
                break;
            default:
                convertValue = value;
                break;
        }
        return convertValue;
    }

    protected upperCaseFirstLetter(input: string): string {
        if (!input || input === '') {
            return '';
        } else {
            return input[0].toUpperCase() + input.substr(1);
        }
    }

    protected getSorts(): any {
        const sorts = {};
        if (this.sortConf) {
            let count = 1;
            this.sortConf.forEach((fieldConf) => {
                sorts[this.upperCaseFirstLetter(fieldConf.field)] = {
                    'order': count,
                    'direction': fieldConf.direction.toUpperCase()
                };
                count++;
            });
        }

        return sorts;
    }

    protected getPager(): any {
        const pager: any = {
            page: 0,
            pageSize: -1
        };
        if (this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
            pager.page = this.pagingConf['page'];
            pager.pageSize = this.pagingConf['perPage'];
        }

        return pager;
    }
    protected LoadServerData(data): Promise<any> {
        const pager = this.getPager();
        const requestBase: Object = {
            datadefault: data,
            entity: this.getFilters(),
            sortedColumns: this.getSorts(),
            page: pager.page === 0 ? 1 : pager.page,
            pageSize: this.localMode ? 0 : pager.pageSize === -1 ? 10 : pager.pageSize
        };
        const request = deepExtend({}, requestBase, this.additionalArgs);

        return this.http.post(this.searchApi, request)
            .pipe(
                map((data: any) => {
                    this.totalCount = data.totalCount;
                    return data.items;
                })
            ).toPromise();
    }
}