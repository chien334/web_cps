import { Component, OnInit, EventEmitter, Output, ViewChild, Input, Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Ng2SmartTableComponent } from 'ng2-smart-table';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { deepExtend } from 'src/app/utils/helpers';
import { CPSDataSource } from './cps-table-source';
import { HostBinding } from '@angular/core';
import { HttpService } from 'src/app/core/services/http/http.service';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
  selector: 'app-table',
  templateUrl: './cps-table.component.html',
  styleUrls: ['./cps-table.component.less']
})
// @Injectable()
export class CPSTableComponent implements OnInit {
  public LINK_API = 'https://localhost:44399/admin/api/';
  @Input() createApi = '';
  @Input() editApi = '';
  @Input() deleteApi = '';
  @Input() searchApi = '';
  @Input() columns: Object = {};
  @Input() localMode = false;
  @Input() selectMode = 'single'; // single|multi
  @Input() allowEdit = true;
  @Input() allowAdd = true;
  @Input() allowDelete = true;
  @Input() datafilter: Object = {};
  @Input() confirmDeleteMessage: string = 'Are your sure to delete item?';
  @Input() confirmCreateMessage: string = 'Are your sure to create item?';
  @Input() confirmUpdateMessage: string = 'Are your sure to update item?';
  @Input() successfulDeleteMessage = 'Delete successful';
  @Input() successfulCreateMessage = 'Create new successful';
  @Input() successfulUpdateMessage = 'Update successful';
  @Input() noDataMessage = 'No data found';
  @Input() additionalArgs: Object = {};
  // @HostBinding('class')

  @Output() selected = new EventEmitter<any>();
  @Output() deleted = new EventEmitter<any>();
  @Output() edited = new EventEmitter<any>();
  @Output() created = new EventEmitter<any>();
  @Output() exportSuccess = new EventEmitter<any>();
  @Output() successEmitter = new EventEmitter<any>();
  @ViewChild('ng2table') table: Ng2SmartTableComponent;
  dataSelect: any;
  messageerorr: string;
  iNew = '';
  dataSource: any = this.createDataSource();
  defaultSettings = {
    mode: 'inline', // inline|external|click-to-edit
    selectMode: 'single', // single|multi
    hideHeader: false,
    hideSubHeader: false,
    actions: {
      columnTitle: '',
      add: false,
      edit: false,
      delete: false,
      custom: [],
      position: 'left', // left|right
    },
    add: {
      addButtonContent: '<i class="fa fa-plus"></i>',
      createButtonContent: '<i class="fa fa-check"></i>',
      cancelButtonContent: '<i class="fa fa-times"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="fa fa-pen"></i>',
      saveButtonContent: '<i class="fa fa-check"></i>',
      cancelButtonContent: '<i class="fa fa-times"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="fa fa-trash"></i>',
      confirmDelete: true,
    },
    noDataMessage: 'No data found',
    columns: {},
    pager: {
      display: true,
      perPageSelect: [10, 20, 50]
    },
    rowClassFunction: () => ''
  };
  settings: any = this.getSettings();
  constructor(private http: HttpService, private toastrService: NzMessageService) { }
  ngOnInit(): void {
    if (this.datafilter !== undefined) {
      const filters = [];
      const request = deepExtend({}, this.datafilter, this.additionalArgs);
      this.additionalArgs = {
        entity: request
      };
      this.dataSource = this.createDataSource();
    }
  }
  createDataSource(): any {
    const columnsDef = deepExtend({}, this.columns);
    return new CPSDataSource(this.http, this.LINK_API + this.searchApi, this.localMode, columnsDef, this.additionalArgs);
  }
  selectRow(filterfn: (value: any) => unknown): any {
    this.table.grid.getRows()
      .filter(row => filterfn(row.getData()))
      .forEach(row => this.table.grid.multipleSelectRow(row));
  }

  onRowSelect(event: any): any {
    this.dataSelect = event.selected;
    this.selected.emit({
      selected: event.selected,
      changed: {
        item: event.data,
        selected: event.isSelected
      }
    });
  }


  getSettings(): Object {
    return deepExtend({}, this.defaultSettings, {
      columns: this.columns,
      selectMode: this.selectMode,
      noDataMessage: this.noDataMessage,
      actions: {
        add: this.allowAdd,
        edit: this.allowEdit,
        delete: this.allowDelete
      }
    });
  }

  onDeleteConfirm(event: any): any {
    this.iNew = 'Delete';
    const baseRequest: Object = {
      entity: event.data
    };
    const request = deepExtend(baseRequest, this.additionalArgs);

    const success = this.confirmDialog(this.confirmDeleteMessage);
    if (success) {
      this.postRequest(this.LINK_API + this.deleteApi, request)
        .subscribe(
          res => {
            event.confirm.resolve(res);
          },
          err => {
            event.confirm.reject(err);
          },
          () => console.log('HTTP request completed.')
        );
    } else {
      event.confirm.reject();
    }
  }

  onEditConfirm(event: any): any {
    this.iNew = 'Edit';
    const baseRequest: Object = {
      oldEntity: event.data,
      entity: event.newData
    };
    const request = deepExtend(baseRequest, this.additionalArgs);

    const success = this.confirmDialog(this.confirmUpdateMessage);
    if (success) {
      this.postRequest(this.LINK_API + this.editApi, request)
        .subscribe(
          res => {
            event.confirm.resolve(res);
          },
          err => {
            event.confirm.reject(err);
          },
          () => console.log('HTTP request completed.')
        );
    } else {
      event.confirm.reject();
    }
  }

  postRequest(api: string, request: any): Observable<any> {
    // console.log('inside');
    return this.http.post(api, request, { observe: 'body' })
      .pipe(
        map((body: any) => {
          if (body.success) {
            this.ShowSuccessful(this.iNew, null, body.success);
            this.successEmitter.emit(true);
            this.realoadList();
            return body.entity;
          } else {
            throwError(body.message);
            this.ShowSuccessful(null, body.message, body.success);
            this.successEmitter.emit(true);
            this.realoadList();
          }
        })
      );
  }


  onCreateConfirm(event: any): any {
    this.iNew = 'Insert';
    const baseRequest: Object = {
      entity: event.newData
    };
    const request = deepExtend(baseRequest, this.additionalArgs);

    const success = this.confirmDialog(this.confirmCreateMessage);
    if (success) {
      this.postRequest(this.LINK_API + this.createApi, request)
        .subscribe(
          res => {
            event.confirm.resolve(res);
          },
          err => {
            event.confirm.reject(err);
          },
          () => console.log('HTTP request completed.')
        );
    } else {
      event.confirm.reject();
    }
  }
  confirmDialog(message: string): boolean {
    return window.confirm(message);
  }
  ShowSuccessful(value, message, status): void {
    if (value === 'Insert') {
      message = this.successfulCreateMessage;
    } else if (value === 'Edit') {
      message = this.successfulUpdateMessage;
    } else if (value === 'Delete') {
      message = this.successfulDeleteMessage;
    }
    else {
      const st = message.split(';');
      this.messageerorr = '<div><ul>';
      for (let j = 0; j < st.length; j++) {
        let ErrorMessage = st[j];
        this.messageerorr += '<li>' + ErrorMessage + '</li>';
      }
      this.messageerorr += '</ul></div>';
    }
    if (status) {
      this.toastrService.create('success', message);
    }
    else {

      this.toastrService.create('error', this.messageerorr);
    }
  }
  public realoadList(): void {
    // console.log('sdsdsds');
    this.dataSource.loaded = true;
    this.dataSource = this.createDataSource();
  }

  isSuccess(data) {
    if (data) {
      this.realoadList();
    }
  }
  SearchByDate(e): any {
    const filter: object = {};
    filter['year'] = Number(e.target.value.split('-')[0]);
    filter['month'] = Number(e.target.value.split('-')[1]);
    this.datafilter = filter;
    this.additionalArgs = {};
    const request = deepExtend({}, this.datafilter, this.additionalArgs);
    this.additionalArgs = {
      entity: request
    };
    this.dataSource = this.createDataSource();
  }

  SearchByDateFrom(e): any {
    console.log(e.target.value);
    this.datafilter['fromStartDate'] = `${e.target.value}T17:00:00.000`;

    this.additionalArgs = {};
    const request = deepExtend({}, this.datafilter, this.additionalArgs);
    this.additionalArgs = {
      entity: request
    };
    this.dataSource = this.createDataSource();
  }

  SearchByDateTo(e): any {
    console.log(e.target.value);
    this.datafilter['toStartDate'] = `${e.target.value}T17:00:00.000`;
    this.additionalArgs = {};
    const request = deepExtend({}, this.datafilter, this.additionalArgs);
    this.additionalArgs = {
      entity: request
    };
    this.dataSource = this.createDataSource();
  }

}
