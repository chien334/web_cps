import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgZorroAntdModule } from 'src/app/ng-zorro-antd.module';
import { CPSTableComponent } from './cps-table/cps-tablecomponent';
import { CPSCheckBoxEditorComponent } from './custom-components/input-checkbox-editor.component';
import { CPSCheckBoxFilterComponent } from './custom-components/input-checkbox-filter.component';
import { CPSCheckBoxViewComponent } from './custom-components/input-checkbox-view.component';
import { UploadComponent } from './upload/upload.component';


@NgModule({
    imports: [
        CommonModule,
        NgZorroAntdModule,
        Ng2SmartTableModule,
        HttpClientModule,
        FormsModule
    ],
    declarations: [
        UploadComponent,
        CPSTableComponent,
        CPSCheckBoxEditorComponent,
        CPSCheckBoxViewComponent,
        CPSCheckBoxFilterComponent
    ],
    exports: [
        UploadComponent,
        CPSTableComponent,
        CPSCheckBoxEditorComponent,
        CPSCheckBoxViewComponent,
        CPSCheckBoxFilterComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentModule { }
