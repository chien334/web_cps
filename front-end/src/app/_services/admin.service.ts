import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'https://localhost:44399/api/';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getListProduct(query: any): Observable<any> {
    const baseRequest = {
      page: 1,
      PageSize: 20,
      entity: query
    };
    return this.http.post(API_URL + 'product/LoadProducts', baseRequest, { observe: 'body' });
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }

}
