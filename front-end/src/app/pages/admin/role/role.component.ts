import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// import { TokenStorageService } from 'src/app/_services/token-storage.service';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.less']
})
export class RoleComponent implements OnInit {
  createApi = '';
  editApi = '';
  deleteApi = '';
  searchApi = '';
  columns = {
    index: {
      title: 'sr_no',
      type: 'text',
      addable: false,
      editable: false,
      valuePrepareFunction: (value, row, cell) => {
        return cell.row.index + 1;
      }
    },
    firstName: {
      title: 'First Name'
    },
    lastName: {
      title: 'Last Name'
    },
    phone: {
      title: 'phone'
    },
    city: {
      title: 'city'
    },
    province: {
      title: 'province'
    },
    total: {
      title: 'total'
    },
  };
  ngOnInit(): void {
  }

}
