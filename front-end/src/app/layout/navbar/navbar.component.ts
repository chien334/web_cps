import { HostListener } from '@angular/core';
import { Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import * as $ from 'jquery';
// import { TokenStorageService } from 'src/app/_services/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {
  linkNav = [
    {
      link: '/introduction',
      name: 'Giới thiệu',
      son: [
        {
          link: '/introduction',
          name: 'Giới thiệu chung',
          son: []
        },
        {
          link: '#',
          name: 'Chiến lược phát triển',
          son: []
        },
        {
          link: '#',
          name: 'Danh hiệu và giải thưởng',
          son: []
        },
        {
          link: '#',
          name: 'Danh hiệu và giải thưởng',
          son: []
        },
        {
          link: '#',
          name: 'Thông tin doanh nghiệp',
          son: []
        },
        {
          link: '#',
          name: 'Cơ cấu tổ chức',
          son: []
        },
        {
          link: '#',
          name: 'Nhân sự chủ chốt',
          son: []
        },
        {
          link: '#',
          name: 'Công ty thành viên',
          son: []
        },
        {
          link: '#',
          name: 'Văn hóa',
          son: []
        }
      ]
    },
    {
      link: '#',
      name: 'Lĩnh vực kinh doanh',
      son: [
        {
          link: '#',
          name: 'Giống cây trồng',

          son: []
        },
        {
          link: '#',
          name: 'Nông sản an toàn',

          son: [
            {
              link: '#',
              name: 'Chiến',
              lv: 3,
              son: []
            },
            {
              link: '#',
              name: 'Hiếu',
              lv: 3,
              son: []
            },
            {
              link: '#',
              name: 'Nam',
              lv: 3,
              son: []
            }

          ]
        },
        {
          link: '#',
          name: 'Vật tư và dịch vụ nông nghiệp',

          son: []
        },
      ]
    },
    {
      link: '#',
      name: 'Quan hệ nhà đầu tư',
      son: [
        {
          link: '#',
          name: 'Thông tin tài chính',
          son: []
        },
        {
          link: '#',
          name: 'Báo cáo tài chính',
          son: []
        },
        {
          link: '#',
          name: 'Điều lệ công ty',
          son: []
        },
        {
          link: '#',
          name: 'Đại hội cổ đông',
          son: []
        },
        {
          link: '#',
          name: 'Báo cáo thường niên',
          son: []
        },
        {
          link: '#',
          name: 'Công bố thông tin',
          son: []
        }
      ]
    },
    {
      link: '#',
      name: 'Phát triển bền vững',
      son: [
        {
          link: '#',
          name: 'Chiến lược',
          son: []
        },
        {
          link: '#',
          name: 'Cam kết',
          son: []
        },
        {
          link: '#',
          name: 'Báo cáo PT bền vững',
          son: []
        }
      ]
    },
    {
      link: '#',
      name: 'Truyền thông',
      son: [
        {
          link: '#',
          name: 'Tin vinaseed',
          son: []
        },
        {
          link: '#',
          name: 'Sự kiện',
          son: []
        },
        {
          link: '#',
          name: 'Thư viện',
          son: []
        },
        {
          link: '#',
          name: 'Ẩm thực doanh nghiệp',
          son: []
        }
      ]
    },
    {
      link: '#',
      name: 'R&D',
      son: [
        {
          link: '#',
          name: 'Chiến lược R&D',
          son: []
        },
        {
          link: '#',
          name: 'Chiến lược R&D',
          son: []
        }
      ]
    },
    {
      link: '#',
      name: 'Tuyển dụng',
      son: [
        {
          link: '#',
          name: 'Thông tin tuyển dụng',
          son: []
        },
        {
          link: '#',
          name: 'Tại sao chọn vinaseed ?',
          son: []
        },
        {
          link: '#',
          name: 'Quy trình tuyển dụng',
          son: []
        },
        {
          link: '#',
          name: 'Nộp hồ sơ online',
          son: []
        }
      ]
    }
  ];
  constructor() {
  }
  ngOnInit(): void {
  }
  @HostListener("window:resize", [])
  private onResize() {
    this.detectScreenSize();
  }

  ngAfterViewInit() {
    this.detectScreenSize();
  }

  private detectScreenSize() {
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      var $subMenu = $(this).next(".dropdown-menu");
      $subMenu.toggleClass('show');

      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass("show");
      });

      return false;
    });
  }
}
