import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// import { TokenStorageService } from 'src/app/_services/token-storage.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  check = false;
  constructor(private router: Router ) {
    if ( this.router.url === '/home'){
        this.check = true;
    }
    else
    {
        this.check = false;
    }
  }
  listImg = [
    { data: 'https://i.pinimg.com/originals/d4/bc/c4/d4bcc46e371e194b20854acd1ba3a86b.jpg' },
    { data: 'https://i.pinimg.com/originals/3f/61/16/3f61166872e6be98de1cd65f24e41670.jpg' },
    { data: 'https://anhdepfree.com/wp-content/uploads/2018/12/hinh-nen-4k-dep-nhat-2018-20.jpg'}
  ];

  ngOnInit(): void {
  }
}
