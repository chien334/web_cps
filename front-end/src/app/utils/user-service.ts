import { Injectable } from '@angular/core';
import { User } from '../core/models/user.model';
import { AuthService } from '../core/authencation/auth.service';

@Injectable({
    providedIn: 'root',
})
export class UserService {

    public user: User = null;

    constructor(public authService: AuthService) {

    }


    set(key: string, data: any): void {
        try {
            localStorage.setItem(key, JSON.stringify(data));
        } catch (e) {
            console.error('Error saving to localStorage', e);
        }
    }

    get(key: string) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (e) {
            console.error('Error getting data from localStorage', e);
            return null;
        }
    }

    clear(key: string) {
        try {
            localStorage.removeItem(key);
        } catch (e) {
            console.error('Error removing data from localStorage', e);
        }
    }

    enableFunctionByCode(code: string) {
        const permits = this.get('permision');
        if (!permits) {
            return;
        }
        if (permits.includes(code)) {
            return true;
        } else {
            return false;
        }
    }
}
