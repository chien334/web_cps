
import { cloneDeep } from 'lodash';

export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}

export const getProperty = function (obj, desc) {
    var arr = desc.split(".");
    while (arr.length && (obj = obj[arr.shift()]));
    return obj;
}

export const extend = function (...objects: Array<any>): any {
    if (arguments.length < 1 || typeof arguments[0] !== 'object') {
        return false;
    }
    if (arguments.length < 2) {
        return arguments[0];
    }
    const target = arguments[0];
    const args = Array.prototype.slice.call(arguments, 1);
    args.forEach((obj: any) => {
        for (const key in obj) {
            target[key] = obj[key];
        }
    });
}

export const deepExtend = function (...objects: Array<any>): any {
    if (arguments.length < 1 || typeof arguments[0] !== 'object') {
        return false;
    }

    if (arguments.length < 2) {
        return arguments[0];
    }

    const target = arguments[0];

    // convert arguments to array and cut off target object
    const args = Array.prototype.slice.call(arguments, 1);

    let val, src;

    args.forEach((obj: any) => {
        // skip argument if it is array or isn't object
        if (typeof obj !== 'object' || Array.isArray(obj)) {
            return;
        }

        Object.keys(obj).forEach(function (key) {
            src = target[key]; // source value
            val = obj[key]; // new value

            // recursion prevention
            if (val === target) {
                return;

                /**
                 * if new value isn't object then just overwrite by new value
                 * instead of extending.
                 */
            } else if (typeof val !== 'object' || val === null) {
                target[key] = val;
                return;

                // just clone arrays (and recursive clone objects inside)
            } else if (Array.isArray(val)) {
                target[key] = cloneDeep(val);
                return;

                // overwrite by new value if source isn't object or array
            } else if (val instanceof Map) {
                if (target[key] === undefined) target[key] = new Map();
                val.forEach((v, k) => {
                    target[key].set(k, deepExtend({}, target[key][k], v));
                });
                return;

                // overwrite by new value if source isn't object or array
            } else if (typeof src !== 'object' || src === null || Array.isArray(src)) {
                //target[key] = deepExtend({}, val);
                target[key] = val;
                return;

                // source value and new value is objects both, extending...
            } else {
                target[key] = deepExtend(src, val);
                return;
            }
        });
    });

    return target;
};