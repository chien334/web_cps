import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { Notification } from '../core/models/notification.model';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../core/authencation/auth.service';
import { FileSaverService } from 'ngx-filesaver';
import { getBaseUrl } from './helpers';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  public data: any[];
  public countUnread: number = 0;
  private hubConnection: signalR.HubConnection
  private readonly startedConnection: Promise<any>;
  constructor(private http: HttpClient, private authService: AuthService, private _FileSaverService: FileSaverService) {
    console.log(getBaseUrl());
    this.hubConnection = new signalR.HubConnectionBuilder().withUrl(getBaseUrl() + "notificationHub",
      { accessTokenFactory: () => this.authService.token }
    ).build();

    this.startedConnection = this.startConnection();

  }
  private startConnection = () => {
    return this.hubConnection.start().then(() => console.log("Connection Start"))
      .catch(err => console.log("error " + err));
  }
  public addNotificationListener = () => {
    this.startedConnection.then(() => {
      this.hubConnection.on("ReceiveNotification", (data) => {
        this.data = data.map(value => {
          return {
            title: value.title,
            message: value.message,
            read: value.read,
            actions: JSON.parse(value.actions),
            id: value.id
            //actions: JSON.parse('[{"type":"button","name": "OK","requestUri": "uri","requestType":"GET","requestData": "object"},{"type": "Link","name": "hoang.com","requestUri": "uri","requestType":"POST","requestData": "object"}]'),        
          }
        });
        this.countUnread = this.data.filter(value => value.read == false).length;
      })
    });
  }

  public loadNotification = () => {
    this.startedConnection.then(() => {
      this.hubConnection.invoke('SendSpecificUserName')
        .catch(err => console.error(err));
    });
  }

  public addNewNotification = () => {
    this.startedConnection.then(() => {
      this.hubConnection.on('ReceiveNewNotification', (data: any) => {
        var newData = {
          title: data.title,
          message: data.message,
          read: data.read,
          actions: JSON.parse(data.actions),
          id: data.id
        };

        this.data.push(newData);
        this.countUnread = this.data.filter(value => value.read == false).length;
      })
    });
  }
  public readNotification = () => {
    this.startedConnection.then(() => {
      this.hubConnection.invoke('ReadNotificationByUserName')
        .catch(err => console.error(err));
    });
  }

  httpOption = {
    headers: new HttpHeaders({
      "Content-Type": "application/json; chatset=utf-8",
    }),
  };
  public actionNotification(data: any) {

    if (data.requestType == 'Get') {
      if (data.dataType == 'File') {
        this.http
          .get(data.requestUri, { responseType: 'blob', observe: 'response' })
          .subscribe(
            res => {
              let fileName = 'file';
              const contentDisposition = res.headers.get('Content-Disposition');
              if (contentDisposition) {
                const fileNameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                const matches = fileNameRegex.exec(contentDisposition);
                fileName = matches[1].replace(/['"]/g, '');
                if (matches != null && matches[1]) {
                  fileName = matches[1].replace(/['"]/g, '');
                }
              }
              this._FileSaverService.save((<any>res).body, fileName);
            },
            err => {
              console.log(err);
              alert('Download fail !');
            },
            () => console.log('HTTP request completed.'),
          );
      } else {
        this.http
          .get(data.requestUri).subscribe(res => {

          });
      }
    } else {
      this.http
        .post(data.requestUri, data.requestData, this.httpOption)
        .subscribe((data) => {
          console.log(data.toString());
        });
    }
  }
}
