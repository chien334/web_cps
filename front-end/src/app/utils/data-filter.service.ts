import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataFilter } from '../core/models/data-filter.model';

@Injectable({
    providedIn: 'root',
})
export class DataFilterService {

    public datafilter: DataFilter = {
        effectiveFrom: '',
        effectiveTo: '',
        channelCodes: '',
        countryCodes: '',
        regionCodes: '',
        businessTypes: '',
        provinceCodes: '',
        keyAccounts: '',
        customerCodes: '',
        storeCodes: '',
        categoryType: ''
    };

    private filterModel = new BehaviorSubject({});
    dataFilterSharing = this.filterModel.asObservable();

    nextFilter(datafilter: DataFilter): any {
        this.setFilter(datafilter);
        this.filterModel.next(datafilter);
    }

    setFilter(data: DataFilter): any {
        console.log('setFilter', data);
        if (data.channelCodes) {
            this.datafilter.channelCodes = data.channelCodes;
        }
        if (data.keyAccounts) {
            this.datafilter.keyAccounts = data.keyAccounts;
        }
        if (data.businessTypes) {
            this.datafilter.businessTypes = data.businessTypes;
        }
        if (data.customerCodes) {
            this.datafilter.customerCodes = data.customerCodes;
        }
        if (data.storeCodes) {
            this.datafilter.storeCodes = data.storeCodes;
        }
        if (data.countryCodes) {
            this.datafilter.countryCodes = data.countryCodes;
        }
        if (data.regionCodes) {
            this.datafilter.regionCodes = data.regionCodes;
        }
        if (data.provinceCodes) {
            this.datafilter.provinceCodes = data.provinceCodes;
        }
        if (data.categoryType) {
            this.datafilter.categoryType = data.categoryType;
        }
        console.log('setFilterDataFilter', this.datafilter);
    }

    getFilter(): any {
        return this.datafilter;
    }
}
