from django.apps import AppConfig


class UserWebConfig(AppConfig):
    name = 'user_web'
