from django.apps import AppConfig


class AdminManageDataConfig(AppConfig):
    name = 'admin_manage_data'
