from rest_framework import serializers
from admin_manage_view.models import ContentLanguage

class ContentLanguageSe(serializers.ModelSerializer):
    class Meta:
        model = ContentLanguage
        fields ='__all__'