from django.shortcuts import render
from rest_framework import generics
from admin_manage_view.models import ContentLanguage
from admin_manage_view._services.ContentLanguageSe import ContentLanguageSe
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins


class ContentLanguageFilltered(generics.CreateAPIView):
    queryset = ContentLanguage.objects.all()
    serializer_class = ContentLanguageSe
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['l_name', 'l_code']

class SnippetDetail(mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    generics.GenericAPIView):
    queryset = Snippet.objects.all()
    serializer_class = SnippetSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)