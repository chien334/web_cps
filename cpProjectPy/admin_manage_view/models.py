# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ContentLanguage(models.Model):
    l_id = models.AutoField(primary_key=True)
    l_name = models.CharField(max_length=32, blank=True, null=True)
    l_code = models.CharField(max_length=4, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'content_language'


class NavMenu(models.Model):
    nm_id = models.AutoField(primary_key=True)
    nav_nm = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    nm_name = models.CharField(max_length=128, blank=True, null=True)
    nm_link = models.CharField(max_length=10, blank=True, null=True)
    nm_image = models.CharField(max_length=10, blank=True, null=True)
    nm_properties = models.CharField(max_length=256, blank=True, null=True)
    nm_order = models.SmallIntegerField(blank=True, null=True)
    nm_status = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nav_menu'


class NmContent(models.Model):
    l = models.OneToOneField(ContentLanguage, models.DO_NOTHING, primary_key=True)
    nm = models.ForeignKey(NavMenu, models.DO_NOTHING)
    nm_content = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'nm_content'
        unique_together = (('l', 'nm'), ('l', 'nm'),)

class Hello(models.Model):
    CreatedBy = models.CharField(max_length=50,default='admin')
    CreatedTime = models.DateTimeField(auto_now_add=True)
    LastUpdateBy = models.CharField(max_length=50,default='admin')
    LastUpdateTime = models.DateTimeField(auto_now_add=True)
    Deleted = models.BooleanField(default=True)
    RowVersion= timestamp.UnixTimestampField(auto_created=True)
    name_product = models.CharField(max_length=100, default='')
    code = models.TextField()
    color = models.CharField(max_length=100, default='')
    parent= models.ForeignKey("self", null=True,related_name='sub_product', on_delete=models.CASCADE)

    class Meta:
        ordering = ['CreatedTime']