from django.apps import AppConfig


class AdminManageViewConfig(AppConfig):
    name = 'admin_manage_view'
