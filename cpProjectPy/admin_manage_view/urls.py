from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
# from admin_manage_view import views
from . import views

urlpatterns = [
    path('ctlang/', views.ContentLanguageList.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)
