from setting import *

def sheet2viewStr(sheet_data, sheet_name, namespace= 'ADR.CPS.Data.Models'):
    modelname = formatName(sheet_name)
    columnDeclare = '\2tpublic System.Boolean Deleted { get; set;}\n'
    view = '\4tDeleted = this.Deleted;\n'
    for index, data in sheet_data.iterrows():
        dataType = 'long' if data['Data Type'] == 'FK' else DATA_TYPE_CONVERTER.get(data['Data Type'])
        if dataType == 'List':
            continue
        columnName = formatName(data['Field'])        
        isnull = '' if data['Not Null'] == 'x' or data['Data Type'] == 'FK' or dataType == 'String' else '?'
        columnDeclare += f'\2tpublic {dataType}{isnull} {columnName} {{ get; set;}}\n'
        view += f'\4t{columnName} = model.{columnName};\n'
        
#     columnDeclare = replaceIndent(columnDeclare)
#     view = replaceIndent(view)
    codeStr = \
        'using System;\n'\
        'using System.Collections.Generic;\n'\
        'using System.Text;\n'\
        f'namespace ADR.CPS.Data.Models.Views\n'\
        '{\n'\
            f'\tpublic partial class {modelname}View{{\n'\
            f'{columnDeclare}'\
            f'\2tpublic {modelname}View(){{}}\n'\
            f'\2tpublic {modelname}View({modelname} model){{\n'\
            '\3tif (model != null){\n'\
            f'{view}'\
            '\3t}\n'\
            '\2t}\n'\
            '\t}\n'\
        '}'
    codeStr = replaceIndent(codeStr)
    return codeStr


def workbook2Views(workbook,filePath,
                   RepositoriesDir= 'ADR.CPS.Data.Repositories',
                   modelsDir = 'ADR.CPS.Data.Models'):
    
    for sheet_data, sheet_name in workbook:
        try:
            if sheet_name == 'listDataType':
                continue
            RepositoriesDir= 'ADR.CPS.Data.Repositories'
            modelsDir = 'ADR.CPS.Data.Models'

            #sheet to Views
            viewPath = filePath +f'/{formatName(sheet_name)}View.g.cs'
            text = sheet2viewStr(sheet_data=sheet_data,sheet_name=sheet_name,namespace=modelsDir)
            writeFile(filePath=viewPath,text=text)
        except Exception as e:
            print(e)