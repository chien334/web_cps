from setting import *

def sheet2ModelStr(sheet_data,sheet_name,namespace= 'ADR.CPS.Data.Models'):
    modelname = formatName(sheet_name)
    columnDeclare = ''
    base = '\4tRowVersion = this.RowVersion,\n\
\4tDeleted = this.Deleted,\n\
\4tLastUpdatedTime = this.LastUpdatedTime,\n\
\4tLastUpdatedBy = this.LastUpdatedBy,\n\
\4tCreatedTime = this.CreatedTime,\n\
\4tCreatedBy = this.CreatedBy,\n'

    clone = base
    virtual_col = ''
    for index, data in sheet_data.iterrows():
        
        dataType = DATA_TYPE_CONVERTER.get(data['Data Type'])
        isnull = '' if data['Not Null'] == 'x' or  data['Data Type'] == 'FK' or dataType == 'String' else '?'
        
        if data['Data Type'] != 'FK':
            columnName = formatName(data['Field'])
            columnDeclare += f'\2tpublic {dataType}{isnull} {columnName} {{ get; set;}}\n'
        elif data['Reference'] != '-1':
            columnName = formatName(data['Field'])
            table_ref = formatName(data['Reference'])
            dataType = 'virtual ' + table_ref
            virtual_col += f'\2tpublic {dataType}{isnull} {table_ref} {{ get; set;}}\n'
            columnDeclare += f'\2tpublic long {columnName} {{ get; set;}}\n'
        clone += f'\4t{columnName} = this.{columnName},\n'
    columnDeclare +=virtual_col

    codeStr = \
            'using ADR.CPS.SDK.Models;\n'\
            'using System;\n'\
            'using System.Collections.Generic;\n'\
            'using System.Text;\n'\
            f'namespace ADR.CPS.Data.Models\n'\
            '{\n'\
                f'\tpublic partial class {modelname} : BaseEntity, ICloneable\n'\
                '\t{\n'\
                    f'{columnDeclare}'\
                    f'\2tpublic {modelname}():base()\n'\
                    '\2t{\n'\
                    '\2t}\n'\
                    '\2tpublic override object Clone()'\
                    '{\n'\
                        f'\3treturn new {modelname}'\
                        '{\n'\
                        f'{clone}'\
                        '\3t};\n'\
                    '\2t}\n'\
                '\t}\n'\
            '}'
    codeStr = replaceIndent(codeStr)
    return codeStr

def workbook2Models(workbook,filePath,
                    RepositoriesDir= 'ADR.CPS.Data.Repositories',
                    modelsDir = 'ADR.CPS.Data.Models'):
    for sheet_data, sheet_name in workbook:
        if sheet_name == 'listDataType':
            continue
        RepositoriesDir= 'ADR.CPS.Data.Repositories'
        modelsDir = 'ADR.CPS.Data.Models'

        # sheet to models
        modelPath=filePath +f'/{formatName(sheet_name)}.g.cs'
        text = sheet2ModelStr(sheet_data=sheet_data,sheet_name=sheet_name,namespace=modelsDir)
        writeFile(filePath=modelPath,text=text)
