from setting import *

def workbook2IQueryService(workbook,filePath,
    namespace = 'ADR.CPS.DATA.Services',
    fileName = 'IQueryService'):

    IQSModel = ''
    for sd,sheet_name in workbook:
        if sheet_name == 'listDataType':
            continue
        modelName = formatName(sheet_name)
        IQSModel+=\
        f'\2tTask<IPagedList<{modelName}View>> Search(QueryModel<{modelName}Query> query, CancellationToken cancellationToken = default);\n'\
        f'\2tTask<List<{modelName}View>> SearchNoPaging(QueryModel<{modelName}Query> query, CancellationToken cancellationToken = default);\n'\
        f'\2tTask<bool> ExistsAsync({modelName} entity, CancellationToken cancellationToken = default);\n\n'
        IQueryService = \
        'using ADR.CPS.Data.Models.Views;\n'\
        'using ADR.CPS.Data.Models.Queries;\n'\
        'using ADR.CPS.Data.Models;\n'\
        'using ADR.CPS.SDK.Data;\n'\
        'using ADR.CPS.SDK.Models;\n'\
        'using System;\n'\
        'using System.Collections.Generic;\n'\
        'using System.Text;\n'\
        'using System.Threading;\n'\
        'using System.Threading.Tasks;\n'\
        f'namespace {namespace}\n'\
        '{\n'\
            f'\tpublic partial interface {fileName} : IAsyncQueryService\n'\
            '\t{\n'\
                f'{IQSModel}'\
            '\t}\n'\
        '}\n'
    IQueryService = replaceIndent(IQueryService)
    writeFile(f'{filePath}/{fileName}.g.cs',IQueryService)