/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     29/12/2020 7:02:46 CH                        */
/*==============================================================*/


drop index RELATIONSHIP_7_FK;

drop index RELATIONSHIP_4_FK;

drop index FIELD_CONTENT_PK;

drop table FIELD_CONTENT;

drop index RELATIONSHIP_2_FK;

drop index GROUP_PK;

drop table "GROUP";

drop index RELATIONSHIP_5_FK;

drop index IMAGE_PK;

drop table IMAGE;

drop index G_INTRO_FK;

drop index RELATIONSHIP_8_FK;

drop index INTRODUCTION_PK;

drop table INTRODUCTION;

drop index SUB_MENU_FK;

drop index NAV_MENU_PK;

drop table NAV_MENU;

drop index ROLE_FK;

drop index PERSONNEL_PK;

drop table PERSONNEL;

drop index RELATIONSHIP_9_FK;

drop index RELATIONSHIP_6_FK;

drop index PRODUCT_PK;

drop table PRODUCT;

drop index BUSINESS_AREAS_FK;

drop index PRODUCT_TYPE_PK;

drop table PRODUCT_TYPE;

drop index SEO_TABLE_FK;

drop index SEO_LANGUAGE_FK;

drop index SEO_DATA_PK;

drop table SEO_DATA;

drop index SLIDER_TYPE_FK;

drop index SLIDER_PK;

drop table SLIDER;

drop index TABLE_INFOR_PK;

drop table TABLE_INFOR;

drop index TRADEMARK_PK;

drop table TRADEMARK;

/*==============================================================*/
/* Table: FIELD_CONTENT                                         */
/*==============================================================*/
create table FIELD_CONTENT (
   FC_ID                INT4                 not null,
   TB_CODE              VARCHAR(20)          not null,
   FC_TBR_ID            INT4                 not null,
   FC_LANGUAGE          INT4                 not null,
   FC_CONTENT           TEXT                 null,
   constraint PK_FIELD_CONTENT primary key (FC_ID)
);

comment on table FIELD_CONTENT is
'contain all content of all table';

/*==============================================================*/
/* Index: FIELD_CONTENT_PK                                      */
/*==============================================================*/
create unique index FIELD_CONTENT_PK on FIELD_CONTENT (
FC_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on FIELD_CONTENT (
TB_CODE
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on FIELD_CONTENT (
FC_LANGUAGE
);

/*==============================================================*/
/* Table: "GROUP"                                               */
/*==============================================================*/
create table "GROUP" (
   G_ID                 SERIAL               not null,
   TB_CODE              VARCHAR(20)          not null,
   G_CODE               VARCHAR(20)          not null,
   G_NAME               VARCHAR(1024)        null,
   constraint PK_GROUP primary key (G_ID)
);

/*==============================================================*/
/* Index: GROUP_PK                                              */
/*==============================================================*/
create unique index GROUP_PK on "GROUP" (
G_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on "GROUP" (
TB_CODE
);

/*==============================================================*/
/* Table: IMAGE                                                 */
/*==============================================================*/
create table IMAGE (
   IMG_ID               SERIAL               not null,
   TB_CODE              VARCHAR(32)          not null,
   IMG_TBR_ID           INT4                 null,
   IMG_CONTENT          VARCHAR(1024)        null,
   constraint PK_IMAGE primary key (IMG_ID)
);

/*==============================================================*/
/* Index: IMAGE_PK                                              */
/*==============================================================*/
create unique index IMAGE_PK on IMAGE (
IMG_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on IMAGE (
TB_CODE
);

/*==============================================================*/
/* Table: INTRODUCTION                                          */
/*==============================================================*/
create table INTRODUCTION (
   INTR_ID              SERIAL               not null,
   INTR_PARENT          INT4                 null,
   INTR_G_ID            INT4                 not null,
   INTR_LEVEL           INT4                 null,
   INTR_ORDER           INT4                 null,
   INTR_STATUS          INT4                 null,
   constraint PK_INTRODUCTION primary key (INTR_ID)
);

/*==============================================================*/
/* Index: INTRODUCTION_PK                                       */
/*==============================================================*/
create unique index INTRODUCTION_PK on INTRODUCTION (
INTR_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on INTRODUCTION (
INTR_PARENT
);

/*==============================================================*/
/* Index: G_INTRO_FK                                            */
/*==============================================================*/
create  index G_INTRO_FK on INTRODUCTION (
INTR_G_ID
);

/*==============================================================*/
/* Table: NAV_MENU                                              */
/*==============================================================*/
create table NAV_MENU (
   NM_ID                SERIAL               not null,
   NM_ID_PARENT         INT4                 null,
   NM_PROPERTIES        VARCHAR(1024)        null,
   NM_ORDER             INT4                 null,
   NM_LEVEL             INT4                 null,
   NM_STATUS            INT4                 null,
   NM_LOGO              BOOL                 null,
   constraint PK_NAV_MENU primary key (NM_ID)
);

/*==============================================================*/
/* Index: NAV_MENU_PK                                           */
/*==============================================================*/
create unique index NAV_MENU_PK on NAV_MENU (
NM_ID
);

/*==============================================================*/
/* Index: SUB_MENU_FK                                           */
/*==============================================================*/
create  index SUB_MENU_FK on NAV_MENU (
NM_ID_PARENT
);

/*==============================================================*/
/* Table: PERSONNEL                                             */
/*==============================================================*/
create table PERSONNEL (
   PSN_ID               INT4                 not null,
   PSN_G_ID             INT4                 null,
   PSN_NAME             VARCHAR(50)          null,
   PSN_DESCRIPTION      VARCHAR(200)         null,
   constraint PK_PERSONNEL primary key (PSN_ID)
);

/*==============================================================*/
/* Index: PERSONNEL_PK                                          */
/*==============================================================*/
create unique index PERSONNEL_PK on PERSONNEL (
PSN_ID
);

/*==============================================================*/
/* Index: ROLE_FK                                               */
/*==============================================================*/
create  index ROLE_FK on PERSONNEL (
PSN_G_ID
);

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table PRODUCT (
   P_ID                 SERIAL               not null,
   PT_ID                INT4                 not null,
   TM_ID                INT4                 null,
   P_PROPERTIES         VARCHAR(1024)        null,
   ATTRIBUTE_5          BOOL                 null,
   constraint PK_PRODUCT primary key (P_ID)
);

/*==============================================================*/
/* Index: PRODUCT_PK                                            */
/*==============================================================*/
create unique index PRODUCT_PK on PRODUCT (
P_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on PRODUCT (
PT_ID
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on PRODUCT (
TM_ID
);

/*==============================================================*/
/* Table: PRODUCT_TYPE                                          */
/*==============================================================*/
create table PRODUCT_TYPE (
   PT_ID                SERIAL               not null,
   PT_G_ID              INT4                 null,
   PT_NAME              VARCHAR(1024)        null,
   PT_DESCRIPTION       VARCHAR(1024)        null,
   constraint PK_PRODUCT_TYPE primary key (PT_ID)
);

/*==============================================================*/
/* Index: PRODUCT_TYPE_PK                                       */
/*==============================================================*/
create unique index PRODUCT_TYPE_PK on PRODUCT_TYPE (
PT_ID
);

/*==============================================================*/
/* Index: BUSINESS_AREAS_FK                                     */
/*==============================================================*/
create  index BUSINESS_AREAS_FK on PRODUCT_TYPE (
PT_G_ID
);

/*==============================================================*/
/* Table: SEO_DATA                                              */
/*==============================================================*/
create table SEO_DATA (
   SD_ID                SERIAL               not null,
   SD_LANG              INT4                 not null,
   TB_CODE              VARCHAR(20)          not null,
   SD_TBR_ID            INT4                 not null,
   SD_TITLE             VARCHAR(80)          null,
   SD_DESCRIPTION       VARCHAR(200)         null,
   constraint PK_SEO_DATA primary key (SD_ID)
);

/*==============================================================*/
/* Index: SEO_DATA_PK                                           */
/*==============================================================*/
create unique index SEO_DATA_PK on SEO_DATA (
SD_ID
);

/*==============================================================*/
/* Index: SEO_LANGUAGE_FK                                       */
/*==============================================================*/
create  index SEO_LANGUAGE_FK on SEO_DATA (
SD_LANG
);

/*==============================================================*/
/* Index: SEO_TABLE_FK                                          */
/*==============================================================*/
create  index SEO_TABLE_FK on SEO_DATA (
TB_CODE
);

/*==============================================================*/
/* Table: SLIDER                                                */
/*==============================================================*/
create table SLIDER (
   S_ID                 SERIAL               not null,
   S_G_ID               INT4                 null,
   S_TITLE              VARCHAR(1024)        null,
   S_IMAGE              VARCHAR(1024)        null,
   S_ORDER              INT4                 null,
   S_STATUS             INT4                 null,
   constraint PK_SLIDER primary key (S_ID)
);

/*==============================================================*/
/* Index: SLIDER_PK                                             */
/*==============================================================*/
create unique index SLIDER_PK on SLIDER (
S_ID
);

/*==============================================================*/
/* Index: SLIDER_TYPE_FK                                        */
/*==============================================================*/
create  index SLIDER_TYPE_FK on SLIDER (
S_G_ID
);

/*==============================================================*/
/* Table: TABLE_INFOR                                           */
/*==============================================================*/
create table TABLE_INFOR (
   TB_CODE              VARCHAR(32)          not null,
   TB_NAME              VARCHAR(64)          null,
   TB_DESCRIPTION       VARCHAR(512)         null,
   TB_STATUS            INT2                 null,
   constraint PK_TABLE_INFOR primary key (TB_CODE)
);

comment on table TABLE_INFOR is
'table quan ly thong tin, group, FIELD CONTENT cua cac TABLE khac
';

/*==============================================================*/
/* Index: TABLE_INFOR_PK                                        */
/*==============================================================*/
create unique index TABLE_INFOR_PK on TABLE_INFOR (
TB_CODE
);

/*==============================================================*/
/* Table: TRADEMARK                                             */
/*==============================================================*/
create table TRADEMARK (
   TM_ID                SERIAL               not null,
   TM_IMAGE             VARCHAR(1024)        null,
   TM_STATUS            INT4                 null,
   constraint PK_TRADEMARK primary key (TM_ID)
);

/*==============================================================*/
/* Index: TRADEMARK_PK                                          */
/*==============================================================*/
create unique index TRADEMARK_PK on TRADEMARK (
TM_ID
);

alter table FIELD_CONTENT
   add constraint FK_FIELD_CO_LANGUAGE_GROUP foreign key (FC_LANGUAGE)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

alter table FIELD_CONTENT
   add constraint FK_FIELD_CO_TABLE_FIE_TABLE_IN foreign key (TB_CODE)
      references TABLE_INFOR (TB_CODE)
      on delete restrict on update restrict;

alter table "GROUP"
   add constraint FK_GROUP_TABLE_GRO_TABLE_IN foreign key (TB_CODE)
      references TABLE_INFOR (TB_CODE)
      on delete restrict on update restrict;

alter table IMAGE
   add constraint FK_IMAGE_IMAGE_TABLE_IN foreign key (TB_CODE)
      references TABLE_INFOR (TB_CODE)
      on delete restrict on update restrict;

alter table INTRODUCTION
   add constraint FK_INTRODUC_G_INTRO_GROUP foreign key (INTR_G_ID)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

alter table INTRODUCTION
   add constraint FK_INTRODUC_SUB_INTR_INTRODUC foreign key (INTR_PARENT)
      references INTRODUCTION (INTR_ID)
      on delete restrict on update restrict;

alter table NAV_MENU
   add constraint FK_NAV_MENU_SUB_MENU_NAV_MENU foreign key (NM_ID_PARENT)
      references NAV_MENU (NM_ID)
      on delete restrict on update restrict;

alter table PERSONNEL
   add constraint FK_PERSONNE_ROLE_GROUP foreign key (PSN_G_ID)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

alter table PRODUCT
   add constraint FK_PRODUCT_PRODUCT_T_PRODUCT_ foreign key (PT_ID)
      references PRODUCT_TYPE (PT_ID)
      on delete restrict on update restrict;

alter table PRODUCT
   add constraint FK_PRODUCT_TRADEMARK_TRADEMAR foreign key (TM_ID)
      references TRADEMARK (TM_ID)
      on delete restrict on update restrict;

alter table PRODUCT_TYPE
   add constraint FK_PRODUCT__BUSINESS__GROUP foreign key (PT_G_ID)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

alter table SEO_DATA
   add constraint FK_SEO_DATA_SEO_LANGU_GROUP foreign key (SD_LANG)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

alter table SEO_DATA
   add constraint FK_SEO_DATA_SEO_TABLE_TABLE_IN foreign key (TB_CODE)
      references TABLE_INFOR (TB_CODE)
      on delete restrict on update restrict;

alter table SLIDER
   add constraint FK_SLIDER_SLIDER_TY_GROUP foreign key (S_G_ID)
      references "GROUP" (G_ID)
      on delete restrict on update restrict;

